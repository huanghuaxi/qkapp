package com.qk.manager.common;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Properties;

/**
 * 上下文处理类
 * @param <Object>
 */
public class AppContext<Object> {

    /**
     * 配置文件
     */
    private Properties properties;

    /**
     *任务集合
     */
    private List<Object> obj;

    /**
     * 任务名称
     */
    private String taskName;

    /**
     * map集合
     */
    private Map<String,Object> map = null;

    /**
     * 上下文
     */
    private static AppContext cnt = null;

    /**
     * 私有构造函数
     */
    private AppContext(){
    }

    public List<Object> getObj() {
        return obj;
    }

    public void setObj(List<Object> obj) {
        this.obj = obj;
    }

    public String getTaskName() {
        return taskName;
    }

    public void setTaskName(String taskName) {
        this.taskName = taskName;
    }

    public Map<String, Object> getMap() {
        return map;
    }

    public void setMap(Map<String, Object> map) {
        this.map = map;
    }

    public void addObj(Object o){
        if(this.obj == null){
            this.obj = new ArrayList<Object>();
        }
        this.obj.add(o);
    }

    public void removeObj(int index){
        if(this.obj != null && this.obj.size() > index){
            this.obj.remove(index);
        }

    }

    /**
     * 单例模式
     * @return
     */
    public static AppContext getInstance(){
        if(cnt == null) {
            cnt = new AppContext();

        }
        return cnt;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }




}
