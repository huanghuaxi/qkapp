package com.qk.manager.common;

public class GenerateClsUtil {

    /**
     * 根据类名获取实例
     * @param clsName
     * @return
     */
    public static Object getHanlderCls(String clsName) throws ClassNotFoundException, IllegalAccessException, InstantiationException{
        Class<?> cls = Class.forName(clsName);
        return cls.newInstance();
    }
}
