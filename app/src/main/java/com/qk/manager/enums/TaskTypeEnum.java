package com.qk.manager.enums;

public enum TaskTypeEnum {

    ADD_FRIEND("添加好友","ADDFRIEND","com.qk.manager.impl.AddFriendServiceImpl", "com.qk.manager.taskdata.impl.AddFriendData"),
    PULL_FRIEND_TO_GROUP("拉好友进群","PULLFRIEDTOGROUP","com.qk.manager.impl.PullFriendToGroup", "com.qk.manager.taskdata.impl.PullFriendToGroupData"),
    READ_MESSAGE("定时阅读消息","READMESSAGE","com.qk.manager.impl.ReadMessage", "com.qk.manager.taskdata.impl.ReadMessageData"),
    WECHAT_INFO("获取微信信息", "WECHATINFO", "com.qk.manager.impl.WechatInfo", ""),
    FRIEND_LIST("获取好友列表", "FRIENDLIST", "com.qk.manager.impl.FriendList", ""),
    GROUP_LIST("获取群列表", "GROUPLIST", "com.qk.manager.impl.GroupList", ""),
    SEND_MESSAGE("发送消息", "SENDMESSAGE", "com.qk.manager.impl.SendMessage", "com.qk.manager.taskdata.impl.SendMessageData"),
    SEND_VOICE_CALL("发送语音通话", "SENDVOICECALL", "com.qk.manager.impl.SendVoiceCall", "com.qk.manager.taskdata.impl.SendVoiceCallData"),
    SEND_VIDEO_CALL("发送视频通话", "SENDVIDEOCALL", "com.qk.manager.impl.SendVideoCall", "com.qk.manager.taskdata.impl.SendVideoCallData"),
    ACCEPT_VOICE_CALL("接收语音通话", "ACCEPTVOICECALL", "com.qk.manager.impl.AcceptVoiceCall", "com.qk.manager.taskdata.impl.AcceptVoiceCallData"),
    ACCEPT_VIDEO_CALL("接收视频通话", "ACCEPTVIDEOCALL", "com.qk.manager.impl.AcceptVideoCall", "com.qk.manager.taskdata.impl.AcceptVideoCallData"),
    READ_MOMENTS("阅读朋友圈", "FRIENDCYCLE", "com.qk.manager.impl.ReadMoments", "com.qk.manager.taskdata.impl.ReadMomentsData"),
    MOMENTS_SEND_TEXT("朋友圈发送文字", "MOMENTSSENDTEXT", "com.qk.manager.impl.MomentsSendText", "com.qk.manager.taskdata.impl.MomentsSendTextData"),
    MOMENTS_SEND_IMG("朋友圈发送图片", "MOMENTSSENDIMG", "com.qk.manager.impl.MomentsSendImg", "com.qk.manager.taskdata.impl.MomentsSendImgData"),
    MOMENTS_SEND_VIDEO("朋友圈发送视频", "MOMENTSSENDVIDEO", "com.qk.manager.impl.MomentsSendVideo", "com.qk.manager.taskdata.impl.MomentsSendVideoData"),
    QQ_ADD_FRIEND("QQ添加好友", "QQADDFRIEND", "com.qk.manager.impl.QQAddFriend", "com.qk.manager.taskdata.impl.QQAddFriendData"),
    QQ_FRIEND_LIST("获取QQ好友列表", "QQFRIENDLIST", "com.qk.manager.impl.QQFriendList", "com.qk.manager.taskdata.impl.QQFriendListData"),
    QQ_GROUP_LIST("获取QQ群列表", "QQGROUPLIST", "com.qk.manager.impl.QQGroupList", "com.qk.manager.taskdata.impl.QQGroupListData"),
    QQ_PULL_FRIEND_TO_GROUP("QQ拉好友进群", "QQPULLFRIENDTOGROUP", "com.qk.manager.impl.QQPullFriendToGroup", "com.qk.manager.taskdata.impl.QQPullFriendToGroupData"),
    QQ_CHANGE_ACCOUNT("QQ切换账号", "QQCHANGEACCOUNT", "com.qk.manager.impl.QQChangeAccount", "com.qk.manager.taskdata.impl.QQChangeAccountData"),
    QQ_FRIEND_CHECK("QQ好友检查", "QQFRIENDCHECK", "com.qk.manager.impl.QQFriendCheck", "com.qk.manager.taskdata.impl.QQFriendCheckData"),
    WX_FRIEND_CHECK("微信好友检查", "WXFRIENDCHECK", "com.qk.manager.impl.WXFriendCheck", "com.qk.manager.taskdata.impl.WXFriendCheckData"),
    QQ_LOGIN("QQ登录", "QQLOGIN", "com.qk.manager.impl.QQLogin", "com.qk.manager.taskdata.impl.QQLoginData"),
    QQ_LOGIN_CHECK("QQ登录检查", "QQLOGINCHECK", "com.qk.manager.impl.QQLoginCheck", "com.qk.manager.taskdata.impl.QQLoginCheckData");


    /**
     *任务名称
     */
    private String taskName;

    /**
     * 编码
     */
    private String code;

    /**
     *
     */
    private String clsName;

    private String dataName;

    private TaskTypeEnum(String taskName,String code,String clsName, String dataName){
        this.code = code;
        this.taskName = taskName;
        this.clsName = clsName;
        this.dataName = dataName;
    }

    /**
     * 根据code 获取任务名称
     * @param code
     * @return
     */
    public static String getTaskNameByCode(String code){
       String taskName = "";
        switch (code) {
            case "ADDFRIEND":
                taskName = ADD_FRIEND.taskName;
                break;
            case "PULLFRIEDTOGROUP":
                taskName = PULL_FRIEND_TO_GROUP.taskName;
                break;
            case "READMESSAGE":
                taskName = READ_MESSAGE.taskName;
                break;
            case "WECHATINFO":
                taskName = WECHAT_INFO.taskName;
                break;
            case "FRIENDLIST":
                taskName = FRIEND_LIST.taskName;
                break;
            case "GROUPLIST":
                taskName = GROUP_LIST.taskName;
                break;
            case "SENDMESSAGE":
                taskName = SEND_MESSAGE.taskName;
                break;
            case "SENDVOICECALL":
                taskName = SEND_VOICE_CALL.taskName;
                break;
            case "SENDVIDEOCALL":
                taskName = SEND_VIDEO_CALL.taskName;
                break;
            case "ACCEPTVOICECALL":
                taskName = ACCEPT_VOICE_CALL.taskName;
                break;
            case "ACCEPTVIDEOCALL":
                taskName = ACCEPT_VIDEO_CALL.taskName;
                break;
            case "FRIENDCYCLE":
                taskName = READ_MOMENTS.taskName;
                break;
            case "MOMENTSSENDTEXT":
                taskName = MOMENTS_SEND_TEXT.taskName;
                break;
            case "MOMENTSSENDIMG":
                taskName = MOMENTS_SEND_IMG.taskName;
                break;
            case "MOMENTSSENDVIDEO":
                taskName = MOMENTS_SEND_VIDEO.taskName;
                break;
            case "QQADDFRIEND":
                taskName = QQ_ADD_FRIEND.taskName;
                break;
            case "QQFRIENDLIST":
                taskName = QQ_FRIEND_LIST.taskName;
                break;
            case "QQGROUPLIST":
                taskName = QQ_GROUP_LIST.taskName;
                break;
            case "QQPULLFRIENDTOGROUP":
                taskName = QQ_PULL_FRIEND_TO_GROUP.taskName;
                break;
            case "QQCHANGEACCOUNT":
                taskName = QQ_CHANGE_ACCOUNT.taskName;
                break;
            case "QQFRIENDCHECK":
                taskName = QQ_FRIEND_CHECK.taskName;
                break;
            case "WXFRIENDCHECK":
                taskName = WX_FRIEND_CHECK.taskName;
                break;
            case "QQLOGIN":
                taskName = QQ_LOGIN.taskName;
                break;
            case "QQLOGINCHECK":
                taskName = QQ_LOGIN_CHECK.taskName;
                break;
        }
        return taskName;
    }

    /**
     * 根据code 获取任务类
     * @param code
     * @return
     */
    public  static String  getClsName(String code){
        String clsName = "";
        switch (code) {
            case "ADDFRIEND":
                clsName = ADD_FRIEND.clsName;
                break;
            case "PULLFRIEDTOGROUP":
                clsName = PULL_FRIEND_TO_GROUP.clsName;
                break;
            case "READMESSAGE":
                clsName = READ_MESSAGE.clsName;
                break;
            case "WECHATINFO":
                clsName = WECHAT_INFO.clsName;
                break;
            case "FRIENDLIST":
                clsName = FRIEND_LIST.clsName;
                break;
            case "GROUPLIST":
                clsName = GROUP_LIST.clsName;
                break;
            case "SENDMESSAGE":
                clsName = SEND_MESSAGE.clsName;
                break;
            case "SENDVOICECALL":
                clsName = SEND_VOICE_CALL.clsName;
                break;
            case "SENDVIDEOCALL":
                clsName = SEND_VIDEO_CALL.clsName;
                break;
            case "ACCEPTVOICECALL":
                clsName = ACCEPT_VOICE_CALL.clsName;
                break;
            case "ACCEPTVIDEOCALL":
                clsName = ACCEPT_VIDEO_CALL.clsName;
                break;
            case "FRIENDCYCLE":
                clsName = READ_MOMENTS.clsName;
                break;
            case "MOMENTSSENDTEXT":
                clsName = MOMENTS_SEND_TEXT.clsName;
                break;
            case "MOMENTSSENDIMG":
                clsName = MOMENTS_SEND_IMG.clsName;
                break;
            case "MOMENTSSENDVIDEO":
                clsName = MOMENTS_SEND_VIDEO.clsName;
                break;
            case "QQADDFRIEND":
                clsName = QQ_ADD_FRIEND.clsName;
                break;
            case "QQFRIENDLIST":
                clsName = QQ_FRIEND_LIST.clsName;
                break;
            case "QQGROUPLIST":
                clsName = QQ_GROUP_LIST.clsName;
                break;
            case "QQPULLFRIENDTOGROUP":
                clsName = QQ_PULL_FRIEND_TO_GROUP.clsName;
                break;
            case "QQCHANGEACCOUNT":
                clsName = QQ_CHANGE_ACCOUNT.clsName;
                break;
            case "QQFRIENDCHECK":
                clsName = QQ_FRIEND_CHECK.clsName;
                break;
            case "WXFRIENDCHECK":
                clsName = WX_FRIEND_CHECK.clsName;
                break;
            case "QQLOGIN":
                clsName = QQ_LOGIN.clsName;
                break;
            case "QQLOGINCHECK":
                clsName = QQ_LOGIN_CHECK.clsName;
                break;
        }
        return clsName;
    }

    /**
     * 根据code 获取任务数据类
     * @param code
     * @return
     */
    public  static String  getDataName(String code){
        String dataName = "";
        switch (code) {
            case "ADDFRIEND":
                dataName = ADD_FRIEND.dataName;
                break;
            case "PULLFRIEDTOGROUP":
                dataName = PULL_FRIEND_TO_GROUP.dataName;
                break;
            case "READMESSAGE":
                dataName = READ_MESSAGE.dataName;
                break;
            case "WECHATINFO":
                dataName = WECHAT_INFO.dataName;
                break;
            case "FRIENDLIST":
                dataName = FRIEND_LIST.dataName;
                break;
            case "GROUPLIST":
                dataName = GROUP_LIST.dataName;
                break;
            case "SENDMESSAGE":
                dataName = SEND_MESSAGE.dataName;
                break;
            case "SENDVOICECALL":
                dataName = SEND_VOICE_CALL.dataName;
                break;
            case "SENDVIDEOCALL":
                dataName = SEND_VIDEO_CALL.dataName;
                break;
            case "ACCEPTVOICECALL":
                dataName = ACCEPT_VOICE_CALL.dataName;
                break;
            case "ACCEPTVIDEOCALL":
                dataName = ACCEPT_VIDEO_CALL.dataName;
                break;
            case "FRIENDCYCLE":
                dataName = READ_MOMENTS.dataName;
                break;
            case "MOMENTSSENDTEXT":
                dataName = MOMENTS_SEND_TEXT.dataName;
                break;
            case "MOMENTSSENDIMG":
                dataName = MOMENTS_SEND_IMG.dataName;
                break;
            case "MOMENTSSENDVIDEO":
                dataName = MOMENTS_SEND_VIDEO.dataName;
                break;
            case "QQADDFRIEND":
                dataName = QQ_ADD_FRIEND.dataName;
                break;
            case "QQFRIENDLIST":
                dataName = QQ_FRIEND_LIST.dataName;
                break;
            case "QQGROUPLIST":
                dataName = QQ_GROUP_LIST.dataName;
                break;
            case "QQPULLFRIENDTOGROUP":
                dataName = QQ_PULL_FRIEND_TO_GROUP.dataName;
                break;
            case "QQCHANGEACCOUNT":
                dataName = QQ_CHANGE_ACCOUNT.dataName;
                break;
            case "QQFRIENDCHECK":
                dataName = QQ_FRIEND_CHECK.dataName;
                break;
            case "WXFRIENDCHECK":
                dataName = WX_FRIEND_CHECK.dataName;
                break;
            case "QQLOGIN":
                dataName = QQ_LOGIN.dataName;
                break;
            case "QQLOGINCHECK":
                dataName = QQ_LOGIN_CHECK.dataName;
                break;
        }
        return dataName;
    }

    public String getCode(){
        return this.code;
    }
}
