package com.qk.manager.impl;

import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.taskdata.impl.AcceptVoiceCallData;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 接受语音通话操作
 */
public class AcceptVoiceCall implements TaskIntfService{

    private static final String TAG = AcceptVoiceCall.class.getName();

    //最小通话时间
    public static Integer minTime;
    //最大通话时间
    public static Integer maxTime;
    //通话时长
    public static Integer callTime = 0;

    private static String viewName = "";
    //是否挂断标识
    public static int opFlag = 0;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面被点击");
                    break;

                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if("com.tencent.mm.plugin.voip.ui.VideoActivity".equals(viewName) && "android.widget.FrameLayout".equals(className)){
                        //接听页面
                        info = rootNodeInfo;
                        clickHangUpBtn(info, event, myAccessibilityService);
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mm") > -1) {
                        viewName = className;
                    }

                    if("com.tencent.mm.plugin.voip.ui.VideoActivity".equals(className)){
                        //语音通话页面
                        opFlag = 1;
                        clickAnswerBtn(info, event, myAccessibilityService);

                    }else if("com.tencent.mm.ui.LauncherUI".equals(className)){
                        //微信页面，等待通话
                        MyAccessibilityService.timeOut = new Date().getTime() + 1000 * (maxTime + 180);
                    }
                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "接收语音通话失败", e);
        }
    }

    /**
     * 点击接听按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickAnswerBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            List<AccessibilityNodeInfo> answerBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.ANSWER_BTN_NAME);
            Log.i(TAG, "接听按钮数量：" + answerBtnCounts.size());
            if(answerBtnCounts.size() > 0){
                //随机接听时间
                Random random = new Random();
                Integer time = random.nextInt(3) + 3;
                Thread.sleep(time * 1000);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(answerBtnCounts.get(0));
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击接听按钮失败", e);
        }
        return false;
    }

    /**
     * 点击挂断按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickHangUpBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            opFlag = 2;
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> answerBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.HANG_UP_BTN_NAME);
            Log.i(TAG, "挂断按钮数量：" + answerBtnCounts.size());
            if(answerBtnCounts.size() > 0){
                Thread.sleep(1000);
                //随机挂断时间
                /*Random random = new Random();
                Integer time = random.nextInt(maxTime - minTime + 1) + minTime;*/
                Integer time = callTime;
                Log.i(TAG, "通话时长：" + time + "秒");
                //防止通话时间过长系统判定为超时而返回app，使得窗口变成悬浮窗无法关闭通话
                MyAccessibilityService.timeOut = new Date().getTime() + (time * 1000);
                Thread.sleep(time * 1000);
                //获取屏幕宽度和组件顶部位置和高度进行点击，直接用组件点击有可能出现点击位置不正确的情况
                Rect rect1 = new Rect();
                rootNodeInfo.getBoundsInScreen(rect1);
                Rect rect2 = new Rect();
                answerBtnCounts.get(0).getBoundsInScreen(rect2);
                Point position = new Point(rect1.width() / 2, rect2.top + (int)(rect2.height() / 2));
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 200L));
                GestureDescription gesture = builder.build();
                myAccessibilityService.opScreen(gesture);
                Thread.sleep(3000);
                //结束任务
                finish();
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击挂断按钮失败", e);
        }
        return false;
    }

    /**
     * 结束
     */
    public static void finish(){
        try{
            minTime = 0;
            maxTime = 0;
            callTime = 0;
            opFlag = 0;
            viewName = "";
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "接收语音通话结束";
            MainActivity.mHandler.sendMessage(message);
            //停止延时器
            AcceptVoiceCallData.handler.removeCallbacks(AcceptVoiceCallData.mRunnable);
            Log.i(TAG, "返回我的APP");
            Thread.sleep(1000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "结束接受语音通话失败", e);
        }
    }
}
