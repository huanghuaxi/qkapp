package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 *  微信添加好友操作
 */
public class AddFriendServiceImpl implements TaskIntfService {

    private static final String TAG = AddFriendServiceImpl.class.getName();

    //当前页面
    private static String viewName = "";

    private static int addFriend = 0;

    //账号
    private static String account = "";
    //备注
    private static String remarks = "";
    //申请语
    private static String applyText = "";
    //好友列表
    public static List<JSONObject> friendList = null;
    //索引
    public static int index = 0;
    //延迟时间
    public static int sleepTime = 0;

    private static boolean clickBtn = false;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            Log.i(TAG, "执行添加好友");
            /*if(friendList != null){
                Log.i(TAG, "好友数量：" + friendList.size());
                Log.i(TAG, "当前执行序号：" + index);
            }*/

            if(friendList == null || friendList.size() <= index){
                //操作结束
                Log.i(TAG, "操作结束");
                MyAccessibilityService.taskType = "";
                MyAccessibilityService.className = "";
                sleepTime = 0;
                index = 0;
                friendList = null;
                viewName = "";
                addFriend = 0;
                account = "";
                remarks = "";
                applyText = "";
                //向服务器发送任务完成消息
                Log.i(TAG, "发送完成任务消息");
                /*String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "完成任务",
                        DataUtil.setfinishTask(MyAccessibilityService.taskId, MainActivity.MOBILE_IMEI));
                MainActivity.myClient.send(msg);*/
                AppContext cnt = AppContext.getInstance();
                cnt.removeObj(0);
                MyAccessibilityService.taskId = 0;
                MyAccessibilityService.isRun = 0;
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "添加好友操作结束";
                MainActivity.mHandler.sendMessage(message);
                Log.i(TAG, "返回我的APP");
                Thread.sleep(1000);
                MainActivity.openMyApp();
                /*Thread.sleep(1000);
                MainActivity.startTask();*/

                return ;
            }

            //获取好友信息
            if(!account.equals(friendList.get(index).getString("account"))){
                account = friendList.get(index).getString("account");
                remarks = friendList.get(index).getString("remarkName");
                applyText = friendList.get(index).getString("applyText");
                sleepTime = friendList.get(index).getInt("sleepTime");
                Log.d(TAG, "account:" + account);
                Log.d(TAG, "remarks:" + remarks);
                Log.d(TAG, "applyText:" + applyText);
                Log.d(TAG, "sleepTime:" + sleepTime);
            }

            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            AccessibilityNodeInfo info = event.getSource();
            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.WECHAT_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页微信标签被点击，点击更多按钮
                                    clickMainMoreBtn(rootNodeInfo, event);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    Log.d(TAG, "viewName:" + viewName);
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if("android.widget.ListView".equals(className) && "com.tencent.mm.ui.LauncherUI".equals(viewName)){
                        //微信添加好友操作
                        Log.d(TAG, "微信添加好友操作");
                        Log.d(TAG, "event:" + event.toString());

                        //点击微信标签按钮
                        clickWechatBtn(info, event, myAccessibilityService);
                    }else if(("android.widget.EditText".equals(className) || "android.widget.ListView".equals(className))
                            && "com.tencent.mm.plugin.fts.ui.FTSAddFriendUI".equals(viewName)){
                        //账号搜索页面
                        GestureDescription gesture = clickSearchAccount(info, event);
                        if(gesture != null){
                            Log.i(TAG, "----------------点击搜索框----------------");
                            myAccessibilityService.opScreen(gesture);
                            Thread.sleep(2000);
                        }else{
                            //没有搜索按钮，查看是否有个人文本
                            boolean isClick = clickFriendList(info, myAccessibilityService);
                            if(!isClick){
                                boolean isError = isTargetAccountError(info);
                                if(isError){
                                    try{
                                        Log.i(TAG, "发送添加好友状态消息");
                                        String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "添加好友信息", DataUtil.setAddFriendInfo(account, 5, MyAccessibilityService.taskId, 1));
                                        MainActivity.myClient.send(msg);
                                    }catch (Exception e){
                                        Log.e(TAG, "向服务器回调失败", e);
                                    }
                                    Log.i(TAG, "延迟时间:" + sleepTime);
                                    if(sleepTime != 0){
                                        Thread.sleep(sleepTime);
                                    }
                                    index++;
                                    Log.i(TAG, "index2:" + index);
                                }
                            }
                        }
                    }/*else if("com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(viewName) && "android.widget.FrameLayout".equals(className)){
                        index++;
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }*/
                    else if("com.tencent.mm.ui.base.p".equals(viewName) && "android.widget.FrameLayout".equals(className)){
                        info = rootNodeInfo;
                        //搜索用户不存在时跳转的页面
                        GestureDescription gesture = clickNotFindFriend(info, event);
                        if(gesture != null){
                            Thread.sleep(1000);
                            myAccessibilityService.opScreen(gesture);
                        }else{
                            boolean isFinish = isNotOp(info);
                            if(isFinish){
                                //结束
                                index = friendList.size();
                                //返回
                                Thread.sleep(1000);
                                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }
                        }
                    }else if(("com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(viewName) || "com.tencent.mm.ui.chatting.ChattingUI".equals(viewName))
                            && "android.widget.LinearLayout".equals(className)){
                        //聊天页面
                        info = rootNodeInfo;
                        boolean isSend = sendMessage(info, event, applyText);

                        if(isSend){
                            index++;
                            try{
                                //发送消息给客户端
                                Log.i(TAG, "发送添加好友状态消息");
                                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "添加好友信息", DataUtil.setAddFriendInfo(account, 4, MyAccessibilityService.taskId, 1));
                                MainActivity.myClient.send(msg);
                            }catch (Exception e){
                                Log.e(TAG, "向服务器回调失败", e);
                            }
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            Log.i(TAG, "延迟时间:" + sleepTime);
                            if(sleepTime != 0){
                                Thread.sleep(sleepTime);
                            }
                        }

                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mm") > -1 ){
                        viewName = className;
                    }
                    if(MyAccessibilityService.flag == 0) {
                        MyAccessibilityService.flag = 1;
                        if (info == null) {
                            info = rootNodeInfo;
                        }
                        //添加好友
                        wechatAddFriend(info, event, className, myAccessibilityService);
                    }
                    MyAccessibilityService.flag = 0;
                    break;
            }
        }catch (Exception e){
            Log.e(TAG, "添加好友操作失败", e);
        }

    }

    /**
     * 微信添加好友
     * @param info
     * @param event
     * @param className
     */
    public void wechatAddFriend(AccessibilityNodeInfo info, AccessibilityEvent event, String className, MyAccessibilityService myAccessibilityService){
        try{
            if ("com.tencent.mm.ui.LauncherUI".equals(className)) {
                //点击微信标签按钮
                Log.d(TAG, "点击微信标签按钮");
                clickWechatBtn(info, event, myAccessibilityService);
            }else if("com.tencent.mm.plugin.subapp.ui.pluginapp.AddMoreFriendsUI".equals(className)){
                //添加微信好友页面
                GestureDescription gesture = clickSearchBox(info, event);
                if(gesture != null){
                    myAccessibilityService.opScreen(gesture);
                }
            }else if("com.tencent.mm.plugin.fts.ui.FTSAddFriendUI".equals(className)){
                //微信号/手机号搜索页面
                inputFriendAccount(info, event, account);
            }else if("com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(className)){
                //添加好友页面
                if(addFriend == 1){
                    //后退
                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                }else{
                    GestureDescription gesture = clickAddFriendList(info, event);
                    if(gesture != null){
                        myAccessibilityService.opScreen(gesture);
                    }else{
                        //已经加过
                        Log.i(TAG, "已经加过该好友");
                        //点击发送消息按钮
                        clickSendMsgBtn(info, event, myAccessibilityService);
                        /*index++;
                        //发送消息给客户端
                        Log.i(TAG, "发送添加好友状态消息");
                        String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "添加好友信息", DataUtil.setAddFriendInfo(account, 4, MyAccessibilityService.taskId));
                        MainActivity.myClient.send(msg);
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        Log.i(TAG, "延迟时间:" + sleepTime);
                        if(sleepTime != 0){
                            Thread.sleep(sleepTime);
                        }*/
                    }
                }
                addFriend = 0;
            }else if("com.tencent.mm.plugin.profile.ui.SayHiWithSnsPermissionUI".equals(className)){
                //好友验证申请页面
                Log.i(TAG, "好友申请页面");
                addFriend = 1;
                Log.i(TAG, "applyText1:" + applyText);
                Log.i(TAG, "remarks1:" + remarks);
                Log.i(TAG, "index1:" + index);
                sendFriendApply(info, event, applyText, remarks);
                Thread.sleep(4000);
                clickBtn = false;
                //发送消息给客户端
                try{
                    Log.i(TAG, "发送添加好友状态消息");
                    String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "添加好友信息", DataUtil.setAddFriendInfo(account, 3, MyAccessibilityService.taskId, 1));
                    MainActivity.myClient.send(msg);
                }catch (Exception e){
                    Log.e(TAG, "向服务器回调失败", e);
                }
                Log.i(TAG, "延迟时间:" + sleepTime);
                if(sleepTime != 0){
                    Thread.sleep(sleepTime);
                }
                index++;
                Log.i(TAG, "index2:" + index);
            }/*else if("com.tencent.mm.ui.base.p".equals(className)){
                //搜索用户不存在时跳转的页面
                GestureDescription gesture = clickNotFindFriend(info, event);
                if(gesture != null){
                    Thread.sleep(1000);
                    myAccessibilityService.opScreen(gesture);
                }else{
                    //没有搜到
                    index++;
                    Thread.sleep(1000);
                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                }
            }*/
        }catch (Exception e){
            Log.e(TAG, "添加好友失败");
            e.printStackTrace();
        }
    }

    /**
     * 点击首页更多按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickMainMoreBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            //查找是否有更多按钮
            List<AccessibilityNodeInfo> moreBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MORE_BTN_NAME);
            Log.i(TAG, "更多按钮数量：" + moreBtnCounts.size());
            if(moreBtnCounts.size() > 0){
                //点击更多按钮
                Log.i(TAG, "点击更多按钮");
//                Log.i(TAG, "click:" + moreBtnCounts.get(moreBtnCounts.size() - 1).isClickable());
                moreBtnCounts.get(moreBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
            }else{
                return false;
            }
        }catch (Exception e){
            Log.e(TAG, "点击首页更多按钮失败");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 跳转到微信页面
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickWechatBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, Constants.MAIN_PAGE_NAME);
            if(pageCounts.size() > 0) {
                //点击微信标签
                GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 1);
                if(gesture != null){
                    myAccessibilityService.opScreen(gesture);
                }
            }else{
                //判断是否为更多菜单
                List<AccessibilityNodeInfo> addFriendCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.ADD_FRIEND_NAME);
                Log.i(TAG,"添加朋友按钮数量：" + addFriendCounts.size());
                if(addFriendCounts.size() > 0){
                    Log.i(TAG, "点击添加朋友");
                    Thread.sleep(1000);
                    addFriendCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }else{
                    return false;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击微信标签按钮失败");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 点击搜索账号
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickSearchAccount(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> searchCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SHEARCH_ACCOUNT_NAME);
            Log.i(TAG, "搜索标签数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                Log.i(TAG,"点击搜索标签");
                //防止点击无效，使用坐标点击
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchCounts.get(0));
                return gesture;
            }

        }catch(Exception e){
            Log.e(TAG, "点击搜索账号失败", e);
        }
        return null;
    }

    /**
     * 点击添加好友搜索框
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickSearchBox(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> shearchBoxCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SHEARCH_FEIEND_NAME);
            Log.d(TAG, "搜索框数量：" + shearchBoxCounts.size());
            if(shearchBoxCounts.size() > 0){
                Log.i(TAG,"点击搜索框");
                Log.i(TAG,"TEXT:" + shearchBoxCounts.get(0).getText());
                //防止点击无效，使用坐标点击
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(shearchBoxCounts.get(0));
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击添加朋友搜索框失败");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 输入账号搜索
     * @param rootNodeInfo
     * @param event
     * @param account
     * @return
     */
    public static boolean inputFriendAccount(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String account){
        try{
            Log.i(TAG, "输入账号搜索");
            //获取输入框
            List<AccessibilityNodeInfo> inputCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(inputCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + inputCounts.size());
            if(inputCounts.size() > 0){
                //输入账号
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, account);
                inputCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                inputCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(1000);
                return true;
            }
        }catch(Exception e){
            Log.e(TAG, "搜索好友失败", e);
        }
        return false;
    }

    /**
     * 点击添加到通讯录
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickAddFriendList(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> addBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("添加到通讯录");
            Log.i(TAG, "添加到通讯录按钮数量：" + addBtnCounts.size());
            if(addBtnCounts.size() > 0){
                Log.i(TAG, "点击添加到通讯按钮");
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(addBtnCounts.get(0));
                clickBtn = true;
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击添加到通讯录按钮失败");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 发送好友申请
     * @param rootNodeInfo
     * @param event
     * @param info
     * @param remarks
     * @return
     */
    public static boolean sendFriendApply(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String info, String remarks){
        try{
            List<AccessibilityNodeInfo> applyCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(applyCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + applyCounts.size());
            if(applyCounts.size() > 1){
                //输入申请信息
                Log.i(TAG, "输入申请信息");
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, info);
                applyCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                applyCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                //判断备注名是否为空，为空不做修改
                if(remarks != null && !"".equals(remarks)){
                    //点击备注
                    Log.i(TAG, "输入备注");
                    Thread.sleep(1000);
                    applyCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, remarks);
                    applyCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    applyCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                }
                //点击发送
                Log.i(TAG, "点击发送");
                Thread.sleep(1000);
                clickSendFriendApply(rootNodeInfo, event);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "填写好友申请验证信息失败");
            e.printStackTrace();

        }
        return false;
    }

    /**
     * 点击发送好友验证申请
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickSendFriendApply(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("发送");
            Log.i(TAG, "发送按钮数量：" + sendBtnCounts.size());
            if(sendBtnCounts.size() > 0){
                Log.i(TAG, "点击发送好友验证申请");
                sendBtnCounts.get(sendBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击发送好友验证申请失败", e);
        }
        return false;
    }

    /**
     * 点击没有找到该用户
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickNotFindFriend(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "没有找到该用户");
            List<AccessibilityNodeInfo> textCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.NOT_FIND_FRIEND_TEXT_NAME);
//            List<AccessibilityNodeInfo> textCounts = new ArrayList<AccessibilityNodeInfo>();
//            AccessibilityWechatUtil.recycleFindView(textCounts, rootNodeInfo, "android.widget.TextView");
            Log.i(TAG, "文本框数量：" + textCounts.size());
            if(textCounts.size() > 0){
                Log.i(TAG, "点击该用户不存在文本框，返回上个页面");
                index++;
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(textCounts.get(0));
                try{
                    //发送修改好友状态给服务器
                    Log.i(TAG, "发送添加好友状态消息");
                    String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "添加好友信息", DataUtil.setAddFriendInfo(account, 5, MyAccessibilityService.taskId, 1));
                    MainActivity.myClient.send(msg);
                }catch (Exception e){
                    Log.e(TAG, "向服务器回调失败", e);
                }
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击该用户不存在文本框失败", e);
        }
        return null;
    }

    /**
     * 判断是否操作过于频繁
     * @param rootNodeInfo
     * @return
     */
    public static boolean isNotOp(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否操作过于频繁");
            List<AccessibilityNodeInfo> textCounts = rootNodeInfo.findAccessibilityNodeInfosByText("操作过于频繁");
            Log.i(TAG, "文本框数量：" + textCounts.size());
            if(textCounts.size() > 0){
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否操作过于频繁不让搜索失败", e);
        }
        return false;
    }

    /**
     * 点击发送消息按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSendMsgBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            List<AccessibilityNodeInfo> sendMsgBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SEND_MSG_BTN_NAME);
            Log.i(TAG, "发消息按钮数量：" + sendMsgBtnCounts.size());
            if(sendMsgBtnCounts.size() > 0){
                Log.i(TAG, "点击发送消息按钮");
                Thread.sleep(1000);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(sendMsgBtnCounts.get(0).getParent());
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击发送消息按钮失败", e);
        }
        return false;
    }

    /**
     * 发送消息
     * @param rootNodeInfo
     * @param event
     * @param msg
     * @return
     */
    public static boolean sendMessage(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String msg){
        try{
            Log.i(TAG, "发送消息");
            //判断当前是文字输入框还是语音
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());
            if(editCounts.size() > 0){
                //输入消息
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, msg);
                editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                //点击发送，原先的更多按钮位置
                List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("发送");
                Log.i(TAG, "发送按钮数量：" + sendBtnCounts.size());
                if(sendBtnCounts.size() > 0){
                    Thread.sleep(1000);
                    sendBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
                return true;
            }else{
                //切换到文本输入框
                List<AccessibilityNodeInfo> changeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("切换");
                Log.i(TAG, "文本切换按钮数量：" + changeBtnCounts.size());
                if(changeBtnCounts.size() > 0){
                    Log.i(TAG, "点击切换文本框");
                    Thread.sleep(1000);
                    changeBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }
        }catch (Exception e){
            Log.i(TAG, "发送消息失败", e);
        }
        return false;
    }

    /**
     * 点击好友列表
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickFriendList(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击好友列表");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> personalCounts = rootNodeInfo.findAccessibilityNodeInfosByText("个人");
            Log.i(TAG, "个人文本数量：" + personalCounts.size());
            if(personalCounts.size() > 0){
                Rect rect = new Rect();
                personalCounts.get(0).getBoundsInScreen(rect);
                Point position = new Point(rect.left + (int)(rect.width() / 2), rect.bottom + 50);
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p1 = new Path();
                p1.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p1, 0L, 100L));
                GestureDescription gesture = builder.build();
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
            }
        }catch (Exception e){
            Log.e(TAG, "点击好友列表失败", e);
        }
        return false;
    }

    /**
     * 判断被搜账号是否异常
     * @param rootNodeInfo
     * @return
     */
    public static boolean isTargetAccountError(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断被搜账号是否异常");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> errorTextCounts = rootNodeInfo.findAccessibilityNodeInfosByText("被搜帐号状态异常");
            Log.i(TAG, "被搜帐号状态异常文本数量：" + errorTextCounts.size());
            if(errorTextCounts.size() > 0){
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断被搜账号是否异常失败", e);
        }
        return false;
    }

}
