package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Rect;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取好友列表
 */
public class FriendList implements TaskIntfService {

    private static final String TAG = MyAccessibilityService.class.getName();

    private static int index = 0;

    private static String last = "";

    private static String viewName = "";

    private static boolean isRoll = false;

    private static int bottomY = 0;

    private static boolean isRun = true;

    private static boolean isFinish = false;

    private static boolean isFirst = false;
    //好友列表
    public static List<Map<String, String>> friendList = new ArrayList<Map<String, String>>();

    private static Map<String, String> friendMap= new HashMap<String, String>();

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            Log.d(TAG, "获取好友列表");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            AccessibilityNodeInfo info = event.getSource();
            switch (eventType){
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        AccessibilityWechatUtil.recycleCheck(info);
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.ADDR_LIST_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页通讯录标签被点击，开始获取好友
                                    if(isRun){
                                        info = rootNodeInfo;
                                        getInfo(info, event, myAccessibilityService);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    Log.d(TAG, "viewName:" + viewName);
                    if(("com.tencent.mm.ui.LauncherUI".equals(viewName) || "com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(viewName)) && "android.widget.ListView".equals(className)){
                        if(isRun){
                            info = rootNodeInfo;
                            getInfo(info, event, myAccessibilityService);
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mm") > -1 ){
                        viewName = className;
                    }
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if("com.tencent.mm.ui.LauncherUI".equals(className)){
                        //判断是否滚动过
                        Log.i(TAG, "是否滚动：" + isRoll);
                        if(isRoll){
                            getInfo(info, event, myAccessibilityService);
                        }else{
                            //双击通讯录标签按钮
                            doubleClickAddrListBtn(info, event, myAccessibilityService);
                        }
                    }else if("com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(className)){
                        //获取好友信息
                        getFriendInfo(info, event, myAccessibilityService);
                    /*if(isRoll){
                        GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(200, 400, 200, 200);
                        myAccessibilityService.opScreen(gesture);
                    }*/
                    }else if("android.support.design.widget.c".equals(className)){
                        //点到其他按钮，返回
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }
                    break;
            }
        }catch (Exception e){
            Log.e(TAG, "获取好友列表失败", e);
        }

    }

    /**
     * 获取好友信息
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     */
    public static void getInfo(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            //获取好友
            Log.i(TAG, "获取好友");
            Thread.sleep(1000);
            //判断是否未列表页面
            GestureDescription gestureBack = isBackBtn(rootNodeInfo, event, myAccessibilityService);
            if(gestureBack != null){
                //返回
                myAccessibilityService.opScreen(gestureBack);
            }
            int flag = getFriend(rootNodeInfo, event, myAccessibilityService);
            Log.i(TAG, "flag3:" + flag);
            if(flag == 2){
                Log.d(TAG, "下拉列表");
                Thread.sleep(1000);
                MyAccessibilityService.flag = 1;
                GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(200, 400, 200, 300);
                myAccessibilityService.opScreen(gesture);
                isRoll = true;
//                isRun = false;
            }else if(flag == 3) {
                Log.i(TAG, "获取好友结束");
                isFinish = true;
            }else if(flag == 0){
                //重新点击通讯录按钮
                clickAddrListBtn(rootNodeInfo, event, myAccessibilityService);
            }else{
                //防止重复执行
                isRun = false;
            }
        }catch (Exception e){
            Log.e(TAG, "获取好友信息失败", e);
        }
    }

    /**
     * 双击通讯录标签按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean doubleClickAddrListBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            GestureDescription gesture = AccessibilityWechatUtil.doubleClickBottomMenuBtn(rootNodeInfo, 2);
            if(gesture != null){
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, Constants.MAIN_PAGE_NAME);
                Rect rect = new Rect();
                pageCounts.get(0).getParent().getChild(1).getBoundsInScreen(rect);
                bottomY = rect.top - 50;
                isFirst = true;
                Thread.sleep(1000);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "双击通讯录按钮失败", e);
        }
        return false;
    }

    /**
     * 点击通讯录按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickAddrListBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 2);
            if(gesture != null){
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击通讯录按钮失败", e);
        }
        return false;
    }

    /**
     * 获取好友
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static int getFriend(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "isFirst:" + isFirst);
            if(isFirst){
                isFirst = false;
                isRoll = true;
                return 0;
            }
            if(rootNodeInfo == null){
                Log.i(TAG, "组件为空");
                return 1;
            }
            List<AccessibilityNodeInfo> textCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(textCounts, rootNodeInfo, "android.view.View");

            List<AccessibilityNodeInfo> endCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.LINKMAN_TEXT_NAME);
            Log.d(TAG, "联系人统计文本框数量：" + endCounts.size());
            Log.d(TAG, "文本框数量：" + textCounts.size());
            Log.d(TAG, "index：" + index);
//            Log.d(TAG, "id:" + textCounts.get(0).getViewIdResourceName());
            if(textCounts.size() <= index + 2 && endCounts != null && endCounts.size() > 0){
                //已经到达底部，点击最后一条
                if(index < textCounts.size()){
                    friendMap.put("name", textCounts.get(index).getText() + "");
                }
                Thread.sleep(1000);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(textCounts.get(textCounts.size() - 2));
                myAccessibilityService.opScreen(gesture);
                Thread.sleep(2000);
                Log.i(TAG, "==============结束===============");
                return 3;
            }
            Log.i(TAG, "friendList_size:" + friendList.size());
            if(friendList.size() > 0){
                String text2 = friendList.get(friendList.size() - 1).get("name");
                Log.i(TAG, "test2:" + text2);
                /*text2 = new String(text2.getBytes("ISO-8859-1"), "UTF-8");
                Log.i(TAG, "test2:" + text2);*/
                for(int i = 0;i < textCounts.size() - 1;i++){
                    if(textCounts.get(i).getText() == null){
                        continue;
                    }
                    String text1 = textCounts.get(i).getText().toString();
                    Log.i(TAG, "name:" + text1);
                    String text3 = new String(text1.getBytes("ISO-8859-1"), "UTF-8");
                    if(text2.equals(text1) || text2.equals(text3)){
                        //判断名称是否有相同的，名称相同查看list中存在数量
                        int num = 0;
                        for(Map<String, String> map : friendList){
                            if(text2.equals(map.get("name"))){
                                num++;
                            }
                        }
                        Log.i(TAG, "名称相同数量：" + num);
                        index = i + num;
                        if(index >= textCounts.size()){
                            index = textCounts.size() - 1;
                        }
                        break;
                    }
                }
            }

            Log.i(TAG, "index:" + index);
            Log.i(TAG, "list_size:" + textCounts.size());
            Log.i(TAG, "last:" + last);
//            Log.i(TAG, "text:" + textCounts.get(index).getText());

            if(textCounts.size() <= index + 2){
                if(index + 1 >= textCounts.size() || textCounts.get(index).getText() == null){
                    return 2;
                }
                //最后一个
                if(textCounts.get(index).getText() != null && last.equals(textCounts.get(index).getText().toString())){
                    //需要下拉
                    return 2;
                }else{
                    //点击查看好友信息
                    Log.d(TAG, "点击好友列表1");
                    Thread.sleep(1000);
                    Rect rect = new Rect();
                    textCounts.get(index).getBoundsInScreen(rect);
                    int viewY = rect.top + rect.height();
                    if(rect.height() > 500){
                        index++;
                        return 2;
                    }
                    Log.i(TAG, "组件坐标:" + viewY);
                    Log.i(TAG, "底部坐标:" + bottomY);
                    if(viewY > bottomY){
                        return 2;
                    }
                    Thread.sleep(1000);
                    MyAccessibilityService.flag = 1;
                    GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(textCounts.get(index));
                    myAccessibilityService.opScreen(gesture);
                    last = textCounts.get(index).getText().toString();
                    Log.i(TAG, "====last=====:" + last);
                    friendMap.put("name", last);
                    return 1;
                }
            }else{
                Log.i(TAG, "last:" + last);
                //点击查看好友信息
                Log.d(TAG, "点击好友列表2");
                Thread.sleep(1000);
                AccessibilityWechatUtil.recycleCheck(textCounts.get(index));
                Rect rect = new Rect();
                textCounts.get(index).getBoundsInScreen(rect);
                if(rect.height() > 500){
                    index++;
                    return 2;
                }
                int viewY = rect.top + rect.height();
                Log.i(TAG, "组件坐标:" + viewY);
                Log.i(TAG, "底部坐标:" + bottomY);
                if(viewY >= bottomY){
                    return 2;
                }
                Thread.sleep(1000);
                MyAccessibilityService.flag = 1;
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(textCounts.get(index));
                myAccessibilityService.opScreen(gesture);
                last = textCounts.get(index).getText().toString();
                Log.i(TAG, "====last=====:" + last);
                friendMap.put("name", last);
                index++;
                return 1;
            }
            /*for(int i = 0;i < textCounts.size();i++){
                Log.d(TAG, "text:" + textCounts.get(i).getText());
            }*/


        }catch (Exception e){
            Log.e(TAG, "获取好友列表失败", e);
            return 2;
        }
    }

    /**
     * 获取微信信息
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean getFriendInfo(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            List<AccessibilityNodeInfo> accountCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.WECHAT_NUM_TEXT_NAME);
            Log.i(TAG, "微信号文本框数量：" + accountCounts.size());
            List<AccessibilityNodeInfo> mobileCounts = rootNodeInfo.findAccessibilityNodeInfosByText("电话号码");
            Log.i(TAG, "微信号文本框数量：" + accountCounts.size());
            Log.i(TAG, "电话号码文本框数量：" + mobileCounts.size());
            if(mobileCounts != null && mobileCounts.size() > 0){
                Log.i(TAG, "mobile:" + mobileCounts.get(0).getParent().getChild(1).getText());
                String mobile = mobileCounts.get(0).getParent().getChild(1).getText().toString();
                friendMap.put("mobile", mobile);
            }
            if(accountCounts != null && accountCounts.size() > 0){
                String account = accountCounts.get(0).getText().toString();
                account = account.substring(6);
                Log.i(TAG, "account:" + account);
                friendMap.put("account", account);
            }
            friendList.add(friendMap);
            friendMap = new HashMap<String, String>();
            //后退
            Log.i(TAG, "---------后退----------");
            Thread.sleep(1000);
            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
            isRun = true;
            if(isFinish){
                //最后一条
                Log.i(TAG, "获取好友列表结束");
                if(friendList.size() == 0){
                    friendList = null;
                }/*else{
                    for(Map<String, String> map : friendList){
                        Log.i(TAG, "name:" + map.get("name"));
                        Log.i(TAG, "account:" + map.get("account"));
                        Log.i(TAG, "mobile:" + map.get("mobile"));
                    }
                    Thread.sleep(5000);
                }*/
                MyAccessibilityService.isRun = 0;
                MyAccessibilityService.taskType = "";
                MyAccessibilityService.className = "";
                index = 0;
                last = "";
                viewName = "";
                isRoll = false;
                bottomY = 0;
                isRun = true;
                isFinish = false;
                isFirst = false;
                Log.i(TAG, "返回APP");
                Thread.sleep(1000);
                MainActivity.openMyApp();
               /* Thread.sleep(1000);
                MainActivity.startTask();*/
            }

        }catch (Exception e){
            Log.e(TAG, "获取好友信息失败", e);
        }
        return false;
    }

    /**
     * 判断是否有后退按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static GestureDescription isBackBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "判断是否有后退按钮");
            Thread.sleep(1000);
            return myAccessibilityService.clickBackBtn(event, rootNodeInfo);
        }catch (Exception e){
            Log.e(TAG, "判断是否有后退按钮失败", e);
        }
        return null;
    }

}
