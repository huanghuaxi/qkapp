package com.qk.manager.impl;

import android.accessibilityservice.GestureDescription;
import android.graphics.Rect;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取群列表
 */
public class GroupList implements TaskIntfService {

    private static final String TAG = MyAccessibilityService.class.getName();

    private static int index = 3;

    private static String last = "";

    private static String viewName = "";

    private static boolean isRoll = false;

    private static int bottomY = 0;

    private static boolean isRun = true;

    private static boolean isFinish = false;
    //群列表
    public static List<Map<String, String>> groupList = new ArrayList<Map<String, String>>();

    private static Map<String, String> groupMap= new HashMap<String, String>();

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            Log.d(TAG, "获取群列表");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            AccessibilityNodeInfo info = event.getSource();
            switch (eventType){
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        AccessibilityWechatUtil.recycleCheck(info);
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.ADDR_LIST_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页通讯录标签被点击，点击群聊按钮
                                    info = rootNodeInfo;
                                    MyAccessibilityService.flag = 1;
                                    GestureDescription gesture = clickGroupChatBtn(info, event);
                                    if(gesture != null){
                                        myAccessibilityService.opScreen(gesture);
                                    }else{
                                        //点击通讯录标签按钮
                                        clickAddrListBtn(info, event, myAccessibilityService);
                                    }
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    Log.d(TAG, "viewName:" + viewName);
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if("com.tencent.mm.ui.contact.ChatroomContactUI".equals(viewName) &&
                            ("android.widget.ListView".equals(className) || "android.widget.FrameLayout".equals(className))){
                        Log.i(TAG, "查看群列表信息");
                        info = rootNodeInfo;
                        Thread.sleep(1000);
                        int flag = getGroupList(info, event);
                        if(flag == 2){
                            if(MyAccessibilityService.flag == 0){
                                //下拉
                                Log.d(TAG, "下拉列表");
                                Thread.sleep(1000);
                                MyAccessibilityService.flag = 1;
                                GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(200, 400, 200, 300);
                                myAccessibilityService.opScreen(gesture);
                                isRoll = true;
                            }
                        }else if(flag == 3){
                            //结束
                            Log.i(TAG, "获取群列表结束");
                            if(groupList.size() == 0){
                                groupList = null;
                            }/*else{
                                for(Map<String, String> map : groupList){
                                    Log.i(TAG, "groupName:" + map.get("name"));
                                }
                                Thread.sleep(10000);
                            }*/
                            MyAccessibilityService.isRun = 0;
                            MyAccessibilityService.taskType = "";
                            MyAccessibilityService.className = "";
                            index = 3;
                            last = "";
                            viewName = "";
                            isRoll = false;
                            bottomY = 0;
                            isRun = true;
                            isFinish = false;
                            Log.i(TAG, "返回APP");
                            Thread.sleep(1000);
                            MainActivity.openMyApp();
                            /*Thread.sleep(1000);
                            MainActivity.startTask();*/
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mm") > -1 ){
                        viewName = className;
                    }
                    if("com.tencent.mm.ui.LauncherUI".equals(className)){
                        if(isRoll){
                            /*int flag = getFriend(info, event, myAccessibilityService);
                            if(flag == 2){
                                Log.d(TAG, "下拉列表");
                                Thread.sleep(1000);
                                GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(200, 400, 200, 300);
                                myAccessibilityService.opScreen(gesture);
                                isRoll = true;
                            }else if(flag == 3){
                                isFinish = true;
                            }*/
                        }else{
                            //点击通讯录标签按钮
                            clickAddrListBtn(info, event, myAccessibilityService);
                        }
                    }
                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "获取群列表失败", e);
        }
    }

    /**
     * 点击通讯录标签按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickAddrListBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击通讯录标签按钮");
            GestureDescription gesture = AccessibilityWechatUtil.doubleClickBottomMenuBtn(rootNodeInfo, 2);
            if(gesture != null){
                List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, Constants.MAIN_PAGE_NAME);
                Rect rect = new Rect();
                pageCounts.get(0).getParent().getChild(1).getBoundsInScreen(rect);
                bottomY = rect.top;
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);

                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击通讯录按钮失败", e);
        }
        return false;
    }

    /**
     * 点击群聊按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickGroupChatBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> groupChatBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.GROUP_CHAT_BTN_NAME);
            Log.i(TAG, "群聊按钮数量：" + groupChatBtnCounts.size());
            if(groupChatBtnCounts.size() > 0 && groupChatBtnCounts.size() < 3){
                Log.i(TAG, "点击群聊按钮");
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(groupChatBtnCounts.get(groupChatBtnCounts.size() - 1));
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击群聊按钮失败");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 获取群聊列表
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static int getGroupList(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> textCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(textCounts, rootNodeInfo, "android.widget.TextView");
            Log.i(TAG, "文本框数量:" + textCounts.size());
            /*if(textCounts.size() <= 4){
                //没有群聊房间
                groupList = null;
                return 3;
            }*/

            if(groupList.size() > 0){
                //获取下一条
                String text1 = groupList.get(groupList.size() - 1).get("name");
                for(int i = 3;i < textCounts.size();i++){
                    String text2 = textCounts.get(i).getText().toString();
                    String text3 = new String(text2.getBytes("ISO-8859-1"), "UTF-8");
                    if(text1.equals(text2) || text1.equals(text3)){
                        index = i + 1;
                        if(index >= textCounts.size()){
                            index = textCounts.size() - 1;
                        }
                    }
                }
            }
            if(textCounts.get(textCounts.size() - 1).getText() != null){
                if(textCounts.get(textCounts.size() - 1).getText().toString().indexOf("保存到通讯录") > -1){
                    return 3;
                }
            }
            if(textCounts.get(index).getText() != null){
                if(textCounts.get(index).getText().toString().indexOf("个群聊") > -1){
                    //最后一条
                    return 3;
                }
            }

            for(int i = index;i < textCounts.size();i++){
                Rect rect = new Rect();
                textCounts.get(i).getBoundsInScreen(rect);
                int viewY = rect.top + rect.height();
                Log.i(TAG, "组件坐标:" + viewY);
                Log.i(TAG, "底部坐标:" + bottomY);
                Log.i(TAG, "标题:" +  textCounts.get(i).getText());
                if(viewY > bottomY){
                    return 2;
                }else{
                    //保存到列表
                    String text1 = textCounts.get(i).getText().toString();
//                    String text2 = new String(text1.getBytes("ISO-8859-1"), "UTF-8");
                    if(text1.indexOf("个群聊") > -1){
                        //最后一条
                        return 3;
                    }
                    groupMap = new HashMap<String, String>();
                    groupMap.put("name", textCounts.get(i).getText().toString());
                    groupList.add(groupMap);
                }
            }


            /*for(int i = 0;i < textCounts.size();i++){
                Log.d(TAG, "text:" + textCounts.get(i).getText());
            }
            List<AccessibilityNodeInfo> groupTextCounts = rootNodeInfo.findAccessibilityNodeInfosByText("群聊");
            Log.i(TAG, "群聊文本框数量：" + groupTextCounts.size());
            if(groupTextCounts != null){
                for (int i = 0;i < groupTextCounts.size();i++){
                    AccessibilityWechatUtil.clickCoordinate(groupTextCounts.get(i));
                }
            }
            List<AccessibilityNodeInfo> frameCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(frameCounts, rootNodeInfo, "android.widget.FrameLayout");
            Log.i(TAG, "frame数量:" + frameCounts.size());
            for(int i = 0;i < frameCounts.size();i++){
                AccessibilityWechatUtil.clickCoordinate(frameCounts.get(i));
            }*/
        }catch (Exception e){
            Log.e(TAG, "获取群列表失败", e);

        }
        return 2;
    }
}
