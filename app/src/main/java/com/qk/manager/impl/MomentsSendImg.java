package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.os.Bundle;
import android.os.Environment;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * 朋友圈发送图片
 */
public class MomentsSendImg implements TaskIntfService {

    private static final String TAG = MomentsSendImg.class.getName();
    //发送消息内容
    public static String msg = "";
    //图片名称
    public static String fileName = "";

    private static String viewName = "";

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面被点击");

                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if (info == null) {
                        info = rootNodeInfo;
                    }

                    if("android.widget.FrameLayout".equals(className) && "com.tencent.mm.ui.LauncherUI".equals(viewName)){
                        //微信主页，点击朋友圈按钮
                        boolean isFindPage = clickMomentsBtn(info, event, myAccessibilityService);
                        //如果没有找到朋友圈按钮，则点击发现
                        if(!isFindPage){
                            clickFindBtn(info, event, myAccessibilityService);
                        }
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mm") > -1) {
                        viewName = className;
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(className)) {
                        //主页，点击发现按钮
                        boolean isClick = clickFindBtn(info, event, myAccessibilityService);
                        if(isClick){
                            //点击朋友圈按钮
                            Thread.sleep(1000);
                            clickMomentsBtn(info, event, myAccessibilityService);
                        }
                    }else if("com.tencent.mm.plugin.sns.ui.SnsTimeLineUI".equals(className)){
                        //朋友圈页面，点击拍照分享按钮
                        clickShareBtn(rootNodeInfo, event);
                    }else if("com.tencent.mm.ui.widget.a.c".equals(className)) {
                        //第一次发送提示页面，点击我知道了按钮
                        clickISeeBtn(info, event);
                    }else if("com.tencent.mm.ui.base.k".equals(className)) {
                        //选择从相册选择
                        Boolean isClick = clickPhotoAlbumBtn(info, event, myAccessibilityService);
                        if(!isClick){
                            //没有点击从相册选择，点击返回按钮
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }
                    }else if("com.tencent.mm.plugin.gallery.ui.AlbumPreviewUI".equals(className)){
                        //相册页面，选择第一张图片
                        Thread.sleep(2000);
                        List<AccessibilityWindowInfo> infoList = myAccessibilityService.getWindows();
                        if(infoList != null){
                            info = infoList.get(infoList.size() - 1).getRoot();
                        }
                        boolean isChecked = checkedImg(info, event);
                        if(!isChecked){
                            //没有图片选中，结束任务
                            Thread.sleep(2000);
                            finish();
                        }
                    }else if("com.tencent.mm.plugin.sns.ui.SnsUploadUI".equals(className)){
                        //发送文字消息页面
                        boolean isSend = sendMoments(info, event, msg);
                        if(!isSend){
                            //发送失败返回朋友圈页面，点击返回
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }else{
                            //结束任务
                            Thread.sleep(2000);
                            finish();
                        }
                    }else if("com.tencent.mm.ui.widget.b.c".equals(className)){
                        //退出提示页面，点击退出
                        backMomentsPage(info, event);
                        //结束任务
                        Thread.sleep(2000);
                        finish();
                    }else if("com.tencent.mm.plugin.sns.ui.SnsOnlineVideoActivity".equals(className) || "com.tencent.mm.plugin.sns.ui.SnsBrowseUI".equals(className)){
                        //跳转到了内容详情页面，点击返回
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }else if(!"com.tencent.mm.ui.base.p".equals(className)){
                        //其他页面，点击返回
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }
                    //退出提示框页面

                    break;
            }
        }catch (Exception e){
            Log.e(TAG, "朋友圈发送图片失败", e);
        }

    }


    /**
     * 点击发现标签按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickFindBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            Log.i(TAG, "点击发现标签按钮");
            GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 3);
            if(gesture != null){
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击发现标签按钮失败", e);
        }
        return false;
    }

    /**
     * 点击朋友圈按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickMomentsBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try {
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> momentsBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MOMENTS_BTN_NAME);
            Log.i(TAG, "朋友圈按钮数量：" + momentsBtnCounts.size());
            if(momentsBtnCounts.size() > 0 && momentsBtnCounts.size() < 3){
                Thread.sleep(1000);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(momentsBtnCounts.get(0));
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击朋友圈按钮失败", e);
        }
        return false;
    }

    /**
     * 点击拍照分享按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickShareBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "点击拍照分享按钮");
            Thread.sleep(3000);
            List<AccessibilityNodeInfo> shareBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.PHOTO_SHARE_BTN_NAME);
            Log.i(TAG, "分享按钮数量：" + shareBtnCounts.size());
            if(shareBtnCounts.size() > 0){
                //点击分享按钮
                shareBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击拍照分享按钮失败", e);
        }
        return false;
    }

    /**
     * 点击我知道了按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickISeeBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "点击我知道了按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> iSeeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.I_SEE_BTN_NAME);
            Log.i(TAG, "我知道了按钮数量：" + iSeeBtnCounts.size());
            if(iSeeBtnCounts.size() > 0){
                //点击我知道了按钮
                iSeeBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击我知道了按钮", e);
        }
        return false;
    }

    /**
     * 点击从相册选择
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickPhotoAlbumBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击从相册选择");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> photoBtncounts = rootNodeInfo.findAccessibilityNodeInfosByText("从相册选择");
            Log.i(TAG, "从相册选择按钮数量：" + photoBtncounts.size());
            if(photoBtncounts.size() > 0){
                GestureDescription gestrue = AccessibilityWechatUtil.clickCoordinate(photoBtncounts.get(0).getParent());
                myAccessibilityService.opScreen(gestrue);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击从相册选择失败", e);
        }
        return false;
    }

    /**
     * 选择图片
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean checkedImg(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "选择图片");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> checkBoxCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(checkBoxCounts, rootNodeInfo, "android.widget.CheckBox");
            Log.i(TAG, "复选框数量：" + checkBoxCounts.size());
            if(checkBoxCounts.size() > 0){
                //选择第一张图片
                List<AccessibilityNodeInfo> viewCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(viewCounts, checkBoxCounts.get(0).getParent(), "android.view.View");
                Log.i(TAG, "view数量：" + viewCounts.size());
                if(viewCounts.size() > 0){
                    viewCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
//                checkBoxCounts.get(0).getParent().getChild(2).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                //点击完成按钮
                List<AccessibilityNodeInfo> finishBtnName = rootNodeInfo.findAccessibilityNodeInfosByText("完成");
                Log.i(TAG, "完成按钮数量：" + finishBtnName.size());
                if(finishBtnName.size() > 0){
                    Thread.sleep(1000);
                    if(finishBtnName.get(0).isClickable()){
                        finishBtnName.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG, "选择图片失败", e);
        }
        return false;
    }

    /**
     * 发送朋友圈
     * @param rootNodeInfo
     * @param event
     * @param info
     * @return
     */
    public static boolean sendMoments(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String info){
        try{
            Log.i(TAG, "发送朋友圈");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editTextCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editTextCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "文本框数量：" + editTextCounts.size());
            if(editTextCounts.size() > 0){
                //输入内容
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, info);
                editTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                editTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                //点击发送
                Thread.sleep(2000);
                List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.PUBLISH_BTN_NAME);
                Log.i(TAG, "发表按钮数量:" + sendBtnCounts.size());
                if(sendBtnCounts.size() > 0){
                    sendBtnCounts.get(sendBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "发送朋友圈失败", e);
        }
        return false;
    }

    /**
     * 返回朋友圈页面
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean backMomentsPage(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "返回朋友圈主页");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> quitBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.QUIT_BTN_NAME);
            Log.i(TAG, "退出按钮数量：" + quitBtnCounts.size());
            if(quitBtnCounts.size() > 0){
                quitBtnCounts.get(quitBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }else{
                List<AccessibilityNodeInfo> notRetainBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.NOT_RETAIN_BTN_NAME);
                Log.i(TAG, "不保留按钮数量：" + notRetainBtnCounts.size());
                if(notRetainBtnCounts.size() > 0){
                    notRetainBtnCounts.get(notRetainBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "返回朋友圈主页", e);
        }
        return false;
    }

    /**
     * 结束发送朋友圈任务
     */
    public static void finish(){
        try {
            Log.i(TAG, "结束发送朋友圈任务");
            //删除图片
            deleteImg(fileName);
            msg = "";
            fileName = "";
            viewName = "";
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "朋友圈发送图片结束";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "返回我的APP");
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "结束发送朋友圈任务失败", e);
        }
    }

    /**
     * 删除图片
     */
    public static void deleteImg(String fileName){
        try{
            String url = Environment.getExternalStorageDirectory() + Constants.IMAGE_URL + fileName;
            File file = new File(url);
            file.delete();
            //通知系统库更新
            String where = MediaStore.Images.Media.DATA + " like ?";
            String params[] = new String[] { url };
            MainActivity.mActivity.getContentResolver().delete(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, where, params);
        }catch (Exception e){
            Log.e(TAG, "删除图片失败", e);
        }
    }

}
