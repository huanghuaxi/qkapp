package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;

/**
 * 朋友圈发送文字
 */
public class MomentsSendText implements TaskIntfService {

    private static final String TAG = MomentsSendText.class.getName();
    //发送消息内容
    public static String msg = "";

    private static String viewName = "";

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{

            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面被点击");

                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if (info == null) {
                        info = rootNodeInfo;
                    }

                    if("android.widget.FrameLayout".equals(className) && "com.tencent.mm.ui.LauncherUI".equals(viewName)){
                        //微信主页，点击朋友圈按钮
                        boolean isFindPage = clickMomentsBtn(info, event, myAccessibilityService);
                        //如果没有找到朋友圈按钮，则点击发现
                        if(!isFindPage){
                            clickFindBtn(info, event, myAccessibilityService);
                        }
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mm") > -1) {
                        viewName = className;
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(className)) {
                        //主页，点击发现按钮
                        boolean isClick = clickFindBtn(info, event, myAccessibilityService);
                        if(isClick){
                            //点击朋友圈按钮
                            Thread.sleep(1000);
                            clickMomentsBtn(info, event, myAccessibilityService);
                        }
                    }else if("com.tencent.mm.plugin.sns.ui.SnsTimeLineUI".equals(className)){
                        //朋友圈页面，长点击拍照分享按钮
                        longClickShareBtn(rootNodeInfo, event);
                    }else if("com.tencent.mm.plugin.sns.ui.SnsLongMsgUI".equals(className)) {
                        //第一次文字发送提示页面，点击我知道了按钮
                        clickISeeBtn(info, event);
                    }else if("com.tencent.mm.plugin.sns.ui.SnsUploadUI".equals(className)){
                        //发送文字消息页面
                        boolean isSend = sendMoments(info, event, msg);
                        if(!isSend){
                            //发送失败返回朋友圈页面，点击返回
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }else{
                            //结束任务
                            Thread.sleep(2000);
                            finish();
                        }
                    }else if("com.tencent.mm.ui.widget.b.c".equals(className)){
                        //退出提示页面，点击退出
                        backMomentsPage(info, event);
                        //结束任务
                        Thread.sleep(2000);
                        finish();
                    }else if("com.tencent.mm.plugin.sns.ui.SnsOnlineVideoActivity".equals(className) || "com.tencent.mm.plugin.sns.ui.SnsBrowseUI".equals(className)){
                        //跳转到了内容详情页面，点击返回
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }else if(!"com.tencent.mm.ui.base.p".equals(className)){
                        //其他页面，点击返回
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }
                //退出提示框页面

                    break;
            }


        }catch (Exception e){
            Log.e(TAG, "朋友圈发送文字失败", e);
        }
    }

    /**
     * 点击发现标签按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickFindBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            Log.i(TAG, "点击发现标签按钮");
            GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 3);
            if(gesture != null){
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击发现标签按钮失败", e);
        }
        return false;
    }

    /**
     * 点击朋友圈按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickMomentsBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try {
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> momentsBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MOMENTS_BTN_NAME);
            Log.i(TAG, "朋友圈按钮数量：" + momentsBtnCounts.size());
            if(momentsBtnCounts.size() > 0 && momentsBtnCounts.size() < 3){
                Thread.sleep(1000);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(momentsBtnCounts.get(0));
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击朋友圈按钮失败", e);
        }
        return false;
    }

    /**
     * 双击朋友圈顶部
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean doubleClickTop(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> backBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.BACK_BTN_NAME);
            Log.i(TAG, "返回按钮数量：" + backBtnCounts.size());
            if(backBtnCounts.size() > 0){
                //双击顶部
                backBtnCounts.get(0).getParent().getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                Thread.sleep(200);
                backBtnCounts.get(0).getParent().getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "双击朋友圈顶部失败", e);
        }
        return false;
    }

    /**
     * 长点击拍照分享按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean longClickShareBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "长点击拍照分享按钮");
            Thread.sleep(3000);
            List<AccessibilityNodeInfo> shareBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.PHOTO_SHARE_BTN_NAME);
            Log.i(TAG, "分享按钮数量：" + shareBtnCounts.size());
            if(shareBtnCounts.size() > 0){
                //长点击分享按钮
                shareBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_LONG_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "长点击拍照分享按钮失败", e);
        }
        return false;
    }

    /**
     * 点击我知道了按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickISeeBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "点击我知道了按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> iSeeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.I_SEE_BTN_NAME);
            Log.i(TAG, "我知道了按钮数量：" + iSeeBtnCounts.size());
            if(iSeeBtnCounts.size() > 0){
                //点击我知道了按钮
                iSeeBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击我知道了按钮", e);
        }
        return false;
    }

    /**
     * 发送朋友圈
     * @param rootNodeInfo
     * @param event
     * @param info
     * @return
     */
    public static boolean sendMoments(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String info){
        try{
            Log.i(TAG, "发送朋友圈");
            if(info == null || "".equals(info)){
                return false;
            }
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editTextCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editTextCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "文本框数量：" + editTextCounts.size());
            if(editTextCounts.size() > 0){
                //输入内容
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, info);
                editTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                editTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                //点击发送
                Thread.sleep(2000);
                List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.PUBLISH_BTN_NAME);
                Log.i(TAG, "发表按钮数量:" + sendBtnCounts.size());
                if(sendBtnCounts.size() > 0){
                    sendBtnCounts.get(sendBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "发送朋友圈失败", e);
        }
        return false;
    }

    /**
     * 返回朋友圈页面
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean backMomentsPage(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "返回朋友圈主页");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> quitBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.QUIT_BTN_NAME);
            Log.i(TAG, "退出按钮数量：" + quitBtnCounts.size());
            if(quitBtnCounts.size() > 0){
                quitBtnCounts.get(quitBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }else{
                List<AccessibilityNodeInfo> notRetainBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.NOT_RETAIN_BTN_NAME);
                Log.i(TAG, "不保留按钮数量：" + notRetainBtnCounts.size());
                if(notRetainBtnCounts.size() > 0){
                    notRetainBtnCounts.get(notRetainBtnCounts.size() - 1).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "返回朋友圈主页", e);
        }
        return false;
    }

    /**
     * 结束发送朋友圈任务
     */
    public static void finish(){
        try {
            Log.i(TAG, "结束发送朋友圈任务");
            msg = "";
            viewName = "";
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "朋友圈发送文字结束";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "返回我的APP");
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "结束发送朋友圈任务失败", e);
        }
    }

}
