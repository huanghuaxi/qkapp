package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class PullFriendToGroup implements TaskIntfService {

    private static final String TAG = PullFriendToGroup.class.getName();

    //当前页面
    private static String viewName = "";
    //群名
    public static List<String> groupList = null;
    //成员
    public static List<String> memberList = null;
    //每个群成员数量
    public static int memNum = 0;
    //群索引
    public static int index = 0;
    //成员索引
    public static int memIndex = 0;
    //上一个成员索引
    public static int nextMemIndex = 0;

    private static boolean isChange = false;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try {
            if(groupList != null){
                Log.d(TAG, "群数量：" + groupList.size());
                Log.d(TAG, "群聊当前索引：" + index);
            }
            if(memberList != null){
                Log.d(TAG, "成员数量：" + memberList.size());
                Log.d(TAG, "成员当前索引：" + memIndex);
            }

            //判断结束
            isFinish(rootNodeInfo, event, myAccessibilityService);
//            Thread.sleep(500);
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            AccessibilityNodeInfo info = event.getSource();
            switch (eventType){
                case AccessibilityEvent.TYPE_VIEW_SCROLLED:
                    Log.i(TAG, "View滚动");
                    if("android.widget.ListView".equals(className) && "com.tencent.mm.ui.contact.SelectContactUI".equals(viewName)){
                        Thread.sleep(1000);
                        info = rootNodeInfo;
                        if(isChange){
                            //点击确定按钮
                            clickFix(info, event, myAccessibilityService);
                        }else{
                            //选择好友
                            int checked = searchMember(info, event, memberList.get(memIndex));
                            Log.i(TAG, "checked:" + checked);
                            if(checked != 0 && checked != 1){
                                Log.i(TAG, "下一个好友");
                                memIndex++;
                            }
                            if(memIndex != nextMemIndex && memIndex % memNum == 0){
                                Log.i(TAG, "下一个群聊");
                                index++;
                                nextMemIndex = memIndex;
                                isChange = true;
                            }
                        }
                        info.recycle();
                    }
                    break;
                /*case AccessibilityEvent.TYPE_VIEW_TEXT_CHANGED:
                    Log.d(TAG, "文本发生改变");
                    Log.d(TAG, "viewName:" + viewName);
                    if("com.tencent.mm.ui.contact.SelectContactUI".equals(viewName) && "android.widget.EditText".equals(className)){
                        info = rootNodeInfo;
                        isFindMember(info, event);
                    }
                    break;*/
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        AccessibilityWechatUtil.recycleCheck(info);
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.ADDR_LIST_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页通讯录标签被点击，点击群聊按钮
                                    Thread.sleep(1000);
                                    info = rootNodeInfo;
                                    MyAccessibilityService.flag = 1;
                                    GestureDescription gesture = clickGroupChatBtn(info, event);
                                    myAccessibilityService.opScreen(gesture);
                                    break;
                                }
                            }
                        }else{
                            if(Constants.SEARCH_MEMBER_NAME.equals(info.getText())){
                                Log.i(TAG, "搜索按钮被点击");
                                //选择好友
                                Thread.sleep(1000);
                                info = rootNodeInfo;
                                if(isChange) {
                                    //点击确定按钮
                                    clickFix(info, event, myAccessibilityService);

                                }else{
                                    int checked = searchMember(info, event, memberList.get(memIndex));
                                }
                                info.recycle();
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    Log.d(TAG, "viewName:" + viewName);
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if("android.widget.ListView".equals(className) && "com.tencent.mm.ui.contact.ChatroomContactUI".equals(viewName)) {
                        //查询群聊房间
                        info = rootNodeInfo;
                        searchChatRoom(info, event, groupList.get(index));
                    }else if("android.widget.EditText".equals(className) && "com.tencent.mm.ui.contact.ChatroomContactUI".equals(viewName)){
                        //点击群聊房间
                        info = rootNodeInfo;
                        GestureDescription gesture = clickChatRoom(info, event, groupList.get(index));
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }else if("android.widget.LinearLayout".equals(className) && "com.tencent.mm.ui.contact.ChatroomContactUI".equals(viewName)){
                        //群聊房间页面，点击聊天信息按钮
                        info = rootNodeInfo;
                        clickChatInfoBtn(info, event, myAccessibilityService);
                    }else if("android.widget.EditText".equals(className) && "com.tencent.mm.ui.contact.SelectContactUI".equals(viewName)){
                        Thread.sleep(1000);
                        info = rootNodeInfo;
                        if(isChange){
                            //点击确定按钮
                            clickFix(info, event, myAccessibilityService);
                        }else{
                            //选择好友
                            int checked = searchMember(info, event, memberList.get(memIndex));
                            Log.i(TAG, "checked:" + checked);
                            if(checked != 0 && checked != 1){
                                Log.i(TAG, "下一个好友");
                                memIndex++;
                            }
                            if(memIndex != nextMemIndex && memIndex % memNum == 0){
                                Log.i(TAG, "下一个群聊");
                                index++;
                                nextMemIndex = memIndex;
                                isChange = true;
                            }
                        }
                        info.recycle();
                    }else if("android.widget.FrameLayout".equals(className) && "com.tencent.mm.ui.contact.SelectContactUI".equals(viewName)){
//                        int checked = checkFirend(info, event);
                        int checked = searchMember(info, event, memberList.get(memIndex));
                        Log.i(TAG, "checked:" + checked);
                        if(checked != 0 && checked != 1){
                            Log.i(TAG, "下一个好友");
                            memIndex++;
                        }
                        if(memIndex != nextMemIndex && memIndex % memNum == 0){
                            Log.i(TAG, "下一个群聊");
                            index++;
                            nextMemIndex = memIndex;
                            isChange = true;
                        }
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mm") > -1 ){
                        viewName = className;
                    }
                    if(MyAccessibilityService.flag == 0) {
                        MyAccessibilityService.flag = 1;
                        if (info == null) {
                            info = rootNodeInfo;
                        }
                        //拉人进群
                        toGroup(info, event, className, myAccessibilityService);
                    }
                    MyAccessibilityService.flag = 0;
                    break;
            }
        }catch (Exception e){
            Log.e(TAG, "拉人进群操作失败", e);
        }
    }

    /**
     * 点击确定按钮
     * @param info
     * @param event
     * @param myAccessibilityService
     */
    public static void clickFix(AccessibilityNodeInfo info, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            boolean isClick = clickAddMemberSureBtn(info, event);
            //判断确定按钮是否可用
            MyAccessibilityService.isRun = 1;
            /*if (!isClick) {
                Thread.sleep(1000);
                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                Thread.sleep(2000);
                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
            }
            Thread.sleep(1000);
            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
            Thread.sleep(2000);
            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);*/
            isChange = false;
        }catch (Exception e){
            Log.e(TAG, "点击确定按钮失败", e);
        }
    }

    /**
     * 拉人进群
     * @param info
     * @param event
     * @param className
     */
    public static void toGroup(AccessibilityNodeInfo info, AccessibilityEvent event, String className, MyAccessibilityService myAccessibilityService) throws Exception{
        //拉人进群
        if("com.tencent.mm.ui.LauncherUI".equals(className)){
            //点击通讯录标签按钮
//            clickAddrListBtn(info, event);
            Log.i(TAG, "点击通讯录标签按钮");
            GestureDescription gesture = AccessibilityWechatUtil.doubleClickBottomMenuBtn(info, 2);
            if(gesture != null){
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
            }
        }else if("com.tencent.mm.ui.contact.ChatroomContactUI".equals(className)){
            //群聊房间列表页面，选中群聊房间
            Thread.sleep(1000);
            if(groupList != null && groupList.size() > 0){
                searchChatRoom(info, event, groupList.get(index));
            }
        }else if("com.tencent.mm.ui.chatting.ChattingUI".equals(className)){
            //群聊房间页面，点击聊天信息按钮
            clickChatInfoBtn(info, event, myAccessibilityService);
        }else if("com.tencent.mm.chatroom.ui.ChatroomInfoUI".equals(className)){
            //群聊房间信息页面，点击添加成员
            clickAddMember(info, event);
        }else if("com.tencent.mm.ui.contact.SelectContactUI".equals(className)){
            //添加群聊成员页面，添加成员操作
            clickSearchView(info, event);
//            int checked = searchMember(info, event, memberList.get(memIndex));
            /*if(checked == 0 || checked == 1){
                memIndex++;
            }
            if(memIndex != 0 && memIndex % memNum == 0){
                index++;
            }*/
        }
    }

    /**
     * 点击群聊按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickGroupChatBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            //判断是否在微信主页
            GestureDescription gesture1 = AccessibilityWechatUtil.doubleClickBottomMenuBtn(rootNodeInfo, 2);
            if(gesture1 == null){
                return null;
            }
            List<AccessibilityNodeInfo> groupChatBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.GROUP_CHAT_BTN_NAME);
            Log.i(TAG, "群聊按钮数量：" + groupChatBtnCounts.size());
            if(groupChatBtnCounts.size() > 0 && groupChatBtnCounts.size() < 3){
                Log.i(TAG, "点击群聊按钮");
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(groupChatBtnCounts.get(groupChatBtnCounts.size() - 1));
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击群聊按钮失败");
            e.printStackTrace();
        }
        return null;
    }

    /**
     * 查询并选择好友
     * @param rootNodeInfo
     * @param event
     * @param name
     * @return
     */
    public static int searchMember(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String name){
        try{
            Log.i(TAG, "----------------------------");
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                if(searchCounts.get(0).getText() == null){
                    /*Log.i(TAG, "没有查询到该成员1");
                    Bundle arguments  = new Bundle();
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "");
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);*/
                    return 1;
                }
                String text1= "搜索";
                Log.i(TAG, "taxt1:" + text1);
                String text2 = searchCounts.get(0).getText().toString();
                Log.i(TAG, "taxt2:" + text2);
//                text2 = new String(text2.getBytes("ISO-8859-1"), "UTF-8");
                Log.i(TAG, "taxt2:" + text2);
                if(text1.equals(text2) || "".equals(text2)){
                    //搜索成员
                    Log.i(TAG, "name:" + name);
                    if(name == null || "".equals(name)){
                        memIndex++;
                    }
                    Bundle arguments  = new Bundle();
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    Thread.sleep(1000);
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, name);
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    return 1;
                }else{
                    //判断当前页面
                    /*String[] texts = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
                    Log.i(TAG, "组件信息");
                    AccessibilityWechatUtil.recycleCheck(rootNodeInfo);
                    boolean b = false;
                    for(String text : texts){
                        Log.i(TAG, "字母：" + text);
                        List<AccessibilityNodeInfo> textViewCounts = rootNodeInfo.findAccessibilityNodeInfosByText(text);
                        Log.i(TAG, "组件数：" + textViewCounts.size());
                        if(textViewCounts.size() > 0){
                            b = true;
                            break;
                        }
                    }
                    if(b){
                        Log.i(TAG, "没有查询到成员");
                        Bundle arguments  = new Bundle();
                        searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "");
                        searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                        searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        return 4;
                    }*/
                    //选中成员
                    List<AccessibilityNodeInfo> memberCounts = new ArrayList<AccessibilityNodeInfo>();
                    AccessibilityWechatUtil.recycleFindView(memberCounts, rootNodeInfo, "android.widget.CheckBox");
                    Log.i(TAG, "成员数量：" + memberCounts.size());
                    Thread.sleep(1000);
                    if(memberCounts.size() > 0){
                        if(!memberCounts.get(0).isChecked()){
                            Log.i(TAG, "添加成员");
                            memberCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            return 2;
                        }else{
                            Log.i(TAG, "已添加重新查询新的成员");
                            Bundle arguments  = new Bundle();
                            searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                            arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "");
                            searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                            searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                            return 3;
                        }
                    }/*else{
                        Log.i(TAG, "没有查询到该成员2");
                        Bundle arguments  = new Bundle();
                        searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "");
                        searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                        searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        return 4;
                    }*/
                }
            }
        }catch (Exception e){
            Log.e(TAG, "搜索成员失败", e);
        }
        return 0;
    }

    public static boolean clickSearchView(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "点击搜索框");
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                Thread.sleep(1000);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
            }
        }catch (Exception e){
            Log.i(TAG, "点击搜索框失败", e);
        }
        return false;
    }

    /**
     * 选中好友
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static int checkFirend(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String name){
        try{
            //弃用
            //判断当前页面
            String[] texts = new String[]{"A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z"};
            boolean b = false;
            for(String text : texts){
                List<AccessibilityNodeInfo> textViewCounts = rootNodeInfo.findAccessibilityNodeInfosByText(text);
                if(textViewCounts.size() > 0){
                    b = true;
                    break;
                }
            }
            if(b){
                return 0;
            }
            //选中成员
            List<AccessibilityNodeInfo> memberCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(memberCounts, rootNodeInfo, "android.widget.CheckBox");
            Log.i(TAG, "成员数量：" + memberCounts.size());
            Thread.sleep(1000);
            if(memberCounts.size() > 0){
                if(!memberCounts.get(0).isChecked()){
                    Log.i(TAG, "添加成员");
                    memberCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return 2;
                }else{
                    Log.i(TAG, "已添加重新查询新的成员");
                    List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
                    AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
                    Log.i(TAG, "搜索框数量：" + searchCounts.size());
                    Bundle arguments  = new Bundle();
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "");
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    return 3;
                }
            }

        }catch (Exception e){
            Log.e(TAG, "选中好友失败", e);
        }
        return 0;
    }

    /**
     * 点击通讯录标签按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickAddrListBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, Constants.MAIN_PAGE_NAME);
            if(pageCounts.size() > 0) {
                AccessibilityNodeInfo relativeCounts = pageCounts.get(0).getParent();
                if(relativeCounts != null){
                    //点击微信标签按钮
                    Log.i(TAG, "点击通讯录标签按钮");
                    Thread.sleep(1000);

//                    relativeCounts.getChild(2).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击通讯录按钮失败", e);
        }
        return false;
    }

    /**
     * 查询群聊房间
     * @param rootNodeInfo
     * @param event
     * @param roomName
     * @return
     */
    public static GestureDescription searchChatRoom(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String roomName){
        try{
            List<AccessibilityNodeInfo> chatInfoBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CHAT_INFO_BTN_NAME);
            Log.i(TAG, "聊天信息按钮数量：" + chatInfoBtnCounts.size());
            if(chatInfoBtnCounts.size() > 0){
                return null;
            }
            List<AccessibilityNodeInfo> searchTextCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchTextCounts, rootNodeInfo, "android.widget.EditText");
            Log.d(TAG, "输入框数量：" + searchTextCounts.size());
            if(searchTextCounts.size() > 0){
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, roomName);
                searchTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
            }else{
                //点击搜索按钮
                List<AccessibilityNodeInfo> searchBtnCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(searchBtnCounts, rootNodeInfo, "android.widget.TextView");
                Log.d(TAG, "搜索按钮数量：" + searchBtnCounts.size());
                if(searchBtnCounts.size() > 0){
                    Log.d(TAG, "点击搜索框");
                    for(AccessibilityNodeInfo info : searchBtnCounts){
                        Log.d(TAG, "text:" + info.getText());
                        if(info.getText() == null){
                            info.performAction(AccessibilityNodeInfo.ACTION_CLICK);
                        }
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG, "查询群聊房间失败", e);
        }
        return null;
    }

    /**
     * 点击群聊房间
     * @param rootNodeInfo
     * @param event
     * @param roomName
     * @return
     */
    public static GestureDescription clickChatRoom(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String roomName){
        try{
            Thread.sleep(1000);
            Log.i(TAG, "点击群聊房间");
            List<AccessibilityNodeInfo> chatRoomCounts = rootNodeInfo.findAccessibilityNodeInfosByText(roomName);
            Log.i(TAG, "群聊房间数量：" + chatRoomCounts.size());
            if(chatRoomCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(chatRoomCounts.get(0));
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击群聊房间失败", e);
        }
        return null;
    }

    /**
     * 点击聊天信息按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickChatInfoBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            List<AccessibilityNodeInfo> chatInfoBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CHAT_INFO_BTN_NAME);
            Log.i(TAG, "聊天信息按钮数量：" + chatInfoBtnCounts.size());
            if(chatInfoBtnCounts.size() > 0){
                Thread.sleep(1000);
                MyAccessibilityService.flag = 1;
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(chatInfoBtnCounts.get(0));
                myAccessibilityService.opScreen(gesture);
//                chatInfoBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击聊天信息按钮失败", e);
        }
        return false;
    }

    /**
     * 点击添加成员按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickAddMember(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> addMemberBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.ADD_MEMBER_BTN_NAME);
            Log.i(TAG, "添加成员按钮数量:" + addMemberBtnCounts.size());
            if(addMemberBtnCounts.size() > 0){
                Thread.sleep(1000);
                addMemberBtnCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
            }
        }catch (Exception e){
            Log.e(TAG, "群聊添加成员失败", e);
        }
        return false;
    }

    /**
     * 点击确定按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickAddMemberSureBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            //选择最后一个好友
            searchMember(rootNodeInfo, event, "");
            Thread.sleep(1000);
            Log.i(TAG, "点击确定按钮");
            List<AccessibilityNodeInfo> sureBtnCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(sureBtnCounts, rootNodeInfo, "android.widget.Button");
            Log.i(TAG, "确定按钮数量：" + sureBtnCounts.size());
            if(sureBtnCounts.size() > 0){
                Log.i(TAG, "点击确定按钮");
                Thread.sleep(1000);
                if(sureBtnCounts.get(0).isEnabled()){
                    AccessibilityWechatUtil.clickCoordinate(sureBtnCounts.get(0));
                    sureBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击群添加成员确定按钮失败", e);
        }
        return false;
    }

    /**
     * 判断是否查询到好友失败
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isFindMember(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> notFindTextCounts = rootNodeInfo.findAccessibilityNodeInfosByText("没有找到");
            Log.i(TAG, "没有找到文本数量：" + notFindTextCounts.size());
            if(notFindTextCounts.size() > 0){
                //没有找到该好友跳转到下一个
                List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
                Log.i(TAG, "搜索框数量：" + searchCounts.size());
                Log.i(TAG, "没有查询到该成员");
                //清空输入框
                Bundle arguments  = new Bundle();
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "");
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                memIndex++;
                Thread.sleep(1000);
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否查询到好友失败", e);
        }
        return false;
    }

    public static void isFinish(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            if(memberList == null || groupList == null || memberList.size() <= memIndex || groupList.size() <= index){
                //操作结束
                clickAddMemberSureBtn(rootNodeInfo, event);
                Log.i(TAG, "拉人进群操作结束");
                MyAccessibilityService.taskType = "";
                MyAccessibilityService.className = "";
                MyAccessibilityService.index = 0;
                MyAccessibilityService.taskList = null;
                viewName = "";
                groupList = new ArrayList<String>();
                memberList = new ArrayList<String>();
                memNum = 0;
                index = 0;
                memIndex = 0;
                GroupList.groupList = new ArrayList<Map<String, String>>();
                FriendList.friendList = new ArrayList<Map<String, String>>();
                MyAccessibilityService.isRun = 0;
                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "完成任务",
                        DataUtil.setfinishTask(MyAccessibilityService.taskId, MainActivity.MOBILE_IMEI));
                MainActivity.myClient.send(msg);
                AppContext cnt = AppContext.getInstance();
                cnt.removeObj(0);
                MyAccessibilityService.taskId = 0;
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "拉人进群操作结束";
                MainActivity.mHandler.sendMessage(message);
                Log.i(TAG, "后退到主页面");
                Thread.sleep(1000);
                MainActivity.openMyApp();
                /*AccessibilityNodeInfo info = rootNodeInfo;
                boolean isClick = clickAddMemberSureBtn(info, event);
                if (!isClick) {
                    Thread.sleep(1000);
                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    Thread.sleep(1000);
                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                }
                Thread.sleep(3000);
                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                Thread.sleep(1000);
                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);*/
                return ;
            }
        }catch (Exception e){
            Log.e(TAG, "结束操作失败", e);
        }
    }

}