package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * QQ添加好友操作
 */
public class QQAddFriend implements TaskIntfService {

    private static final String TAG = QQAddFriend.class.getName();

    //当前页面
    private static String viewName = "";

    //账号
    private static String account = "";
    //备注
    private static String remarks = "";
    //申请语
    private static String applyText = "";
    //好友列表
    public static List<JSONObject> friendList = null;
    //索引
    public static int index = 0;
    //延迟时间
    public static int sleepTime = 0;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            Log.i(TAG, "执行QQ添加好友");

            //切换账号
            if(index >= QQChangeAccount.accountIndex && QQChangeAccount.accountNum != -1){
                viewName = "com.tencent.mobileqq.activity.SplashActivity";
                MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_CHANGE_ACCOUNT.getCode());
                return ;
            }

            //是否结束任务
            if(finish()){
                return ;
            }
            //获取数据
            getData();

            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr == null ? "" : classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName);
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType){
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.LINKMAN_TEXT_NAME.equals(info.getChild(i).getText())) {
                                    //联系人页面，点击添加好友按钮
                                    clickAddBtn(rootNodeInfo, myAccessibilityService);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");

                    if(info == null){
                        info = rootNodeInfo;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(viewName)){
                        //主页，点击联系人按钮
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }

                    if("android.widget.LinearLayout".equals(className) || "com.tencent.mobileqq.activity.contact.addcontact.SearchContactsActivity".equals(viewName)){
                        //搜索账号页面，点击找人
                        clickSearchBtn(rootNodeInfo, myAccessibilityService);
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mobileqq") > -1 ){
                        viewName = className;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        //主页，点击联系人按钮
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }

                    if("com.tencent.mobileqq.activity.contact.addcontact.AddContactsActivity".equals(className)){
                        //选择搜索好友页面，点击输入框
                        clickSearchView(info);
                    }
                    if("com.tencent.mobileqq.activity.contact.addcontact.SearchContactsActivity".equals(className)){
                        //搜索好友页面，输入账号
                        int result = isSearch(info);
                        if(result == 1){
                            Log.i(TAG, "没有搜索到好友");
                            index++;
                            MyAccessibilityService.isRun = 1;
                            //点击后退
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            //向服务器发送修改好友状态消息
                            try{
                                //向服务器发送修改好友状态消息
                                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ好友状态",
                                        DataUtil.setQQAddFriendInfo(account, 5, QQChangeAccount.accountList.get(QQChangeAccount.accountIndex - 1), MyAccessibilityService.taskId, 1));
                                MainActivity.myClient.send(msg);
                            }catch (Exception e){
                                Log.i(TAG, "向服务器回调失败", e);
                            }
                        }else{
                            inputAccount(info, account);
                        }
                    }
                    if("com.tencent.mobileqq.activity.FriendProfileCardActivity".equals(className)){
                        //好友信息页面，点击加好友按钮
                        boolean isAdd = clickAddFriendBtn(info);
                        if(!isAdd){
                            //没有添加好友按钮，已添加过该好友跳过
                            index++;
                            MyAccessibilityService.isRun = 1;
                            //点击后退
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            try{
                                //向服务器发送修改好友状态消息
                                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ好友状态",
                                        DataUtil.setQQAddFriendInfo(account, 4, QQChangeAccount.accountList.get(QQChangeAccount.accountIndex - 1), MyAccessibilityService.taskId, 1));
                                MainActivity.myClient.send(msg);
                            }catch (Exception e){
                                Log.i(TAG, "向服务器回调失败", e);
                            }
                        }
                    }
                    if("com.tencent.mobileqq.activity.contact.addcontact.ClassificationSearchActivity".equals(className)){
                        //没有搜索到好友页面，向服务器发送账号状态
                        Log.i(TAG, "没有搜索到好友");
                        index++;
                        MyAccessibilityService.isRun = 1;
                        //点击后退
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        //向服务器发送修改好友状态消息
                        try{
                            //向服务器发送修改好友状态消息
                            String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ好友状态",
                                    DataUtil.setQQAddFriendInfo(account, 5, QQChangeAccount.accountList.get(QQChangeAccount.accountIndex - 1), MyAccessibilityService.taskId, 1));
                            MainActivity.myClient.send(msg);
                        }catch (Exception e){
                            Log.i(TAG, "向服务器回调失败", e);
                        }
                    }
                    if("com.tencent.mobileqq.activity.AddFriendVerifyActivity".equals(className) || "com.tencent.mobileqq.activity.AddFriendLogicActivity".equals(className)){
                        //添加好友页面，发送验证信息
                        boolean isAdd = sendVerify(info);
                        if(isAdd){
                            index++;
                            MyAccessibilityService.isRun = 1;
                            try{
                                //向服务器发送修改好友状态消息
                                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ好友状态",
                                        DataUtil.setQQAddFriendInfo(account, 3, QQChangeAccount.accountList.get(QQChangeAccount.accountIndex - 1), MyAccessibilityService.taskId, 1));
                                MainActivity.myClient.send(msg);
                            }catch (Exception e){
                                Log.i(TAG, "向服务器回调失败", e);
                            }
                            //延迟
                            Log.i(TAG, "延迟时间：" + sleepTime);
                            if(sleepTime != 0){
                                Thread.sleep(sleepTime);
                            }
                        }
                    }

                    if("com.tencent.mobileqq.activity.NotificationActivity".equals(className)) {
                        //qq账号冻结提示页面，点击取消，结束任务
                        clickCancelBtn(rootNodeInfo, myAccessibilityService);
                        QQChangeAccount.accountNum = -1;
                        finish();
                    }

                    break;
            }
        }catch (Exception e){
            Log.e(TAG, "QQ添加好友操作失败", e);
        }
    }

    /**
     * 点击添加好友按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickAddBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击添加好友按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> addBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.ADD_BTN_NAME);
            Log.i(TAG, "添加按钮数量：" + addBtnCounts.size());
            if(addBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(addBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击添加好友按钮失败", e);
        }
        return false;
    }

    /**
     * 点击搜索框
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickSearchView(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "点击搜索框");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchViewCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchViewCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchViewCounts.size());
            if(searchViewCounts.size() > 0){
                searchViewCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击搜索框");
        }
        return false;
    }

    /**
     * 输入好友账号
     * @param rootNodeInfo
     * @param account
     * @return
     */
    public static boolean inputAccount(AccessibilityNodeInfo rootNodeInfo, String account){
        try{
            Log.i(TAG, "输入好友账号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> inputCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(inputCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + inputCounts.size());
            if(inputCounts.size() > 0){
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, account);
                inputCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                inputCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(2000);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "输入好友账号失败", e);
        }
        return false;
    }

    /**
     * 点击搜索按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickSearchBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击搜索按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("找人:");
            Log.i(TAG, "搜索按钮数量：" + searchBtnCounts.size());
            if(searchBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchBtnCounts.get(searchBtnCounts.size() - 1).getParent());
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击搜索按钮失败", e);
        }
        return false;
    }

    /**
     * 点击加好友按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickAddFriendBtn(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "点击加好友按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> addFriendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("加好友");
            Log.i(TAG, "加好友按钮数量：" + addFriendBtnCounts.size());
            if(addFriendBtnCounts.size() > 0){
                addFriendBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击加好友按钮失败", e);
        }
        return false;
    }

    /**
     * 发送验证消息
     * @param rootNodeInfo
     * @return
     */
    public static boolean sendVerify(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "发送验证消息");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editTextCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editTextCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editTextCounts.size());
            if(editTextCounts.size() > 1){
                if(applyText != null && !"".equals(applyText)){
                    //输入验证信息
                    Bundle arguments  = new Bundle();
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, applyText);
                    editTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    editTextCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    Thread.sleep(1000);
                }
                if(remarks != null && !"".equals(remarks)){
                    //输入备注
                    Bundle arguments  = new Bundle();
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, remarks);
                    editTextCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    editTextCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    Thread.sleep(1000);
                }
                //点击发送
                List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("发送");
                Log.i(TAG, "发送按钮数量：" + sendBtnCounts.size());
                if(sendBtnCounts.size() > 0){
                    Thread.sleep(1000);
                    sendBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "发送验证消息失败", e);
        }
        return false;
    }

    /**
     * 判断是否搜索到好友
     * @param rootNodeInfo
     * @return
     */
    public static int isSearch(AccessibilityNodeInfo rootNodeInfo){
        try {
            Log.i(TAG, "判断是否搜索到好友");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> resultCounts = rootNodeInfo.findAccessibilityNodeInfosByText("没有找到相关结果");
            Log.i(TAG, "没有找到相关结果数量：" + resultCounts.size());
            if(resultCounts.size() > 0){
                return 1;
            }
            return 2;
        }catch (Exception e){
            Log.e(TAG, "判断是否搜索到好友失败", e);
        }
        return 0;
    }

    /**
     * 获取数据
     */
    public static void getData(){
        try{
            //获取好友信息
            if(!account.equals(friendList.get(index).getString("account"))){
                account = friendList.get(index).getString("account");
                remarks = friendList.get(index).getString("remarkName");
                applyText = friendList.get(index).getString("applyText");
                sleepTime = friendList.get(index).getInt("sleepTime");
                Log.d(TAG, "account:" + account);
                Log.d(TAG, "remarks:" + remarks);
                Log.d(TAG, "applyText:" + applyText);
                Log.d(TAG, "sleepTime:" + sleepTime);
            }
        }catch (Exception e){
            Log.e(TAG, "获取好友数据失败", e);
        }
    }

    /**
     * 点击取消按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickCancelBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击取消按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> cancelBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CANCEL_BTN_NAME);
            Log.i(TAG, "取消按钮数量：" + cancelBtnCounts.size());
            if(cancelBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(cancelBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击取消按钮失败", e);
        }
        return false;
    }

    /**
     * 结束任务
     */
    public static boolean finish(){
        //结束添加好友
        try{
            if(friendList == null || friendList.size() <= index || QQChangeAccount.accountNum == -1){
                Log.i(TAG, "操作结束");
                QQChangeAccount.resetParam();
                sleepTime = 0;
                index = 0;
                friendList = null;
                viewName = "";
                account = "";
                remarks = "";
                applyText = "";
                MyAccessibilityService.resetParam();
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "添加QQ好友操作结束";
                MainActivity.mHandler.sendMessage(message);
                Log.i(TAG, "返回我的APP");
                Thread.sleep(1000);
                MainActivity.openMyApp();
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "QQ添加好友结束失败", e);
        }
        return false;
    }

}
