package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;

/**
 * QQ切换账号
 */
public class QQChangeAccount implements TaskIntfService {

    private static final String TAG = QQChangeAccount.class.getName();

    //当前页面
    public static String viewName = "";

    //账号数量
    public static Integer accountNum = 0;
    //账号列表
    public static List<String> accountList = new ArrayList<String>();
    //账号索引
    public static Integer accountIndex = 0;
    //切换QQ号
    public static String qqAccount = "";
    //切换是否成功
    public static boolean isSuccess = true;
    //是否为已登录的号
    public static boolean isLogin = false;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{

            Log.d(TAG, "QQ切换账号");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr == null ? "" : classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            if("".equals(viewName)){
                viewName = "com.tencent.mobileqq.activity.SplashActivity";
            }

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.LINKMAN_TEXT_NAME.equals(info.getChild(i).getText())) {
                                    //联系人页面，点击账号及设置按钮
                                    if(!isSetBtn(rootNodeInfo)){
                                        clickAccountAndSet(rootNodeInfo, myAccessibilityService);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }

                    if(!"android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.activity.SplashActivity".equals(viewName)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }else{
                            //账户及设置页面，点击设置
                            clickSetBtn(rootNodeInfo, myAccessibilityService);
                        }
                    }

                    if(!"android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.activity.QQSettingSettingActivity".equals(viewName)){
                        //设置页面，点击账号管理
                        clickAccManangeBtn(rootNodeInfo, myAccessibilityService);
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mobileqq") > -1 ){
                        viewName = className;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }else if("com.tencent.mobileqq.activity.QQSettingSettingActivity".equals(className)){
                        //设置页面，点击账号管理
                        clickAccManangeBtn(rootNodeInfo, myAccessibilityService);
                    }else if("com.tencent.mobileqq.activity.AccountManageActivity".equals(className)){
                        //账号管理页面，切换账号
                        boolean isChange = changeAccount(info, myAccessibilityService);
                        if(isLogin){
                            //不需要切换
                            accountIndex++;
                            isSuccess = true;
                            MyAccessibilityService.isRun = 1;
                            MyAccessibilityService.className = TaskTypeEnum.getClsName(MyAccessibilityService.taskType);
                        }
                        if("".equals(qqAccount) && accountNum == -1){
                            MyAccessibilityService.isRun = 1;
                            MyAccessibilityService.className = TaskTypeEnum.getClsName(MyAccessibilityService.taskType);
                        }
                    }else if("com.tencent.mobileqq.activity.LoginActivity".equals(className)){
                        //QQ登录页面，切换账号失败
                        isSuccess = false;
                        accountIndex++;
                        //后退，切换下一个账号
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        if(!"".equals(qqAccount)){
                            MyAccessibilityService.isRun = 1;
                            MyAccessibilityService.className = TaskTypeEnum.getClsName(MyAccessibilityService.taskType);
                        }
                    }else if(className.indexOf("com.tencent.mobileqq") > -1){
                        //点击到其他页面，后退
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }

                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "QQ切换账号失败", e);
        }
    }


    /**
     * 点击账户及设置按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickAccountAndSet(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击账号及设置按钮");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> accountBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/conversation_head");
            Log.i(TAG, "账户及设置按钮数量：" + accountBtnCounts.size());
            if(accountBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accountBtnCounts.get(0).getParent());
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击账户及设置按钮失败", e);
        }
        return false;
    }

    /**
     * 判断是否有打卡按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean isSetBtn(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否有打卡按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> setBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("打卡");
            Log.i(TAG, "打卡按钮数量：" + setBtnCounts.size());
            if(setBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(setBtnCounts.get(0).getParent());
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否有打卡按钮失败", e);
        }
        return false;
    }

    /**
     * 点击设置按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickSetBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击设置按钮");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> setBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/settings");
            Log.i(TAG, "设置按钮数量：" + setBtnCounts.size());
            if(setBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(setBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击设置按钮失败", e);
        }
        return false;
    }

    /**
     * 点击账号管理按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickAccManangeBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击账号管理按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> manageBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("帐号管理");
            Log.i(TAG, "账号管理按钮数量：" + manageBtnCounts.size());
            if(manageBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(manageBtnCounts.get(0).getParent());
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击账号管理按钮失败", e);
        }
        return false;
    }

    /**
     * 切换账号
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean changeAccount(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "切换账号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> linerCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/accountLinearlayout");
            Log.i(TAG, "线性布局组件数量：" + linerCounts.size());
            if(linerCounts.size() > 0){
                if("".equals(qqAccount)){
                    if(accountNum == 0){
                        linerCounts.get(0).findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/account");
                        for(int i = 0;i < linerCounts.get(0).getChildCount();i++){
                            List<AccessibilityNodeInfo> accountCounts = linerCounts.get(0).getChild(i).findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/account");
                            if(accountCounts.size() > 0){
                                if(accountCounts.get(0).getText() != null){
                                    accountList.add(accountCounts.get(0).getText().toString());
                                }
                            }
                        }
                        accountNum = accountList.size();
                    }

                    Log.i(TAG, "账号数量：" + accountNum);

                    //判断是否为最后一个
                    if(accountIndex >= accountNum){
                        accountIndex = 0;
                        accountNum = -1;
                        viewName = "";
                        //后退
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        return true;
                    }

                    //判断当前账号是否已经切换
                    List<AccessibilityNodeInfo> accCounts = new ArrayList<AccessibilityNodeInfo>();

                    for(int i = 0;i < linerCounts.get(0).getChildCount();i++){
                        List<AccessibilityNodeInfo> accountCounts = linerCounts.get(0).getChild(i).findAccessibilityNodeInfosByText(accountList.get(accountIndex));
                        if(accountCounts.size() > 0){
                            accCounts = new ArrayList<AccessibilityNodeInfo>(accountCounts);
                            break;
                        }
                    }
                    Log.i(TAG, accountList.get(accountIndex) + "账号数量：" + accCounts.size());
                    if(accCounts.size() > 0){
                        AccessibilityWechatUtil.clickCoordinate(accCounts.get(0).getParent());
                        List<AccessibilityNodeInfo> checkCounts = accCounts.get(0).getParent().findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/check");
                        Log.i(TAG, "check组件数量：" + checkCounts.size());
                        if(checkCounts != null && checkCounts.size() > 0){
                            //被选中，不切换,后退
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            isLogin = true;
                        }else{
                            //切换账号
                            GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accCounts.get(0));
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                            isLogin = false;
                        }

                        return true;
                    }
                }else{
                    //判断当前账号是否已经切换
                    List<AccessibilityNodeInfo> accCounts = new ArrayList<AccessibilityNodeInfo>();
                    for(int i = 0;i < linerCounts.get(0).getChildCount();i++){
                        List<AccessibilityNodeInfo> accountCounts = linerCounts.get(0).getChild(i).findAccessibilityNodeInfosByText(qqAccount);
                        if(accountCounts.size() > 0){
                            accCounts = new ArrayList<AccessibilityNodeInfo>(accountCounts);
                            break;
                        }
                    }
                    if(accCounts.size() > 0){
                        AccessibilityWechatUtil.clickCoordinate(accCounts.get(0).getParent());
                        List<AccessibilityNodeInfo> checkCounts = accCounts.get(0).getParent().findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/check");
                        Log.i(TAG, "check组件数量：" + checkCounts.size());
                        if(checkCounts != null && checkCounts.size() > 0){
                            //被选中，不切换,后退
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            isLogin = true;
                        }else{
                            //切换账号
                            GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accCounts.get(0));
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                            isLogin = false;
                        }

                        return true;
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG, "切换账号失败", e);
        }
        return true;
    }

    /**
     * QQ切换账号参数重置
     */
    public static void resetParam(){
        try{
            Log.i(TAG, "QQ切换账号参数重置");
            viewName = "";
            accountIndex = 0;
            accountList = new ArrayList<String>();
            qqAccount = "";
            accountNum = 0;
            isSuccess = true;
            isLogin = false;
        }catch (Exception e){
            Log.e(TAG, "QQ切换账号参数重置失败", e);
        }
    }

}
