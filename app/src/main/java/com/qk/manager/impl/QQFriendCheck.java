package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * QQ好友检查
 */
public class QQFriendCheck implements TaskIntfService {

    private static final String TAG = QQFriendCheck.class.getName();

    private static String viewName = "";
    //每个qq账号好友集合
    public static Map<String, JSONArray> accountMap;
    //QQ账号
    public static List<String> qqAccount;
    //QQ号索引
    public static Integer qqIndex = 0;
    //好友索引
    public static Integer friendIndex = 0;
    //消息内容
    public static List<String> msgList = new ArrayList<String>();
    //是否发送消息
    public static boolean isSendMsg = false;
    //关联添加好友任务ID
    public static int addFriendId = 0;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            Log.d(TAG, "QQ好友检查");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr == null ? "" : classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            //判断是否结束
            if(isFinish(rootNodeInfo, myAccessibilityService)){
                return ;
            }

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.LINKMAN_TEXT_NAME.equals(info.getChild(i).getText())) {
                                    //联系人页面，点击搜索框
                                    boolean isClick = clickSearchBox(rootNodeInfo, myAccessibilityService);
                                    if(!isClick){
                                        //没有搜索框再次点击联系人返回顶部
                                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                                        if(gesture != null){
                                            MyAccessibilityService.flag = 1;
                                            myAccessibilityService.opScreen(gesture);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }

                    if(!"android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.activity.SplashActivity".equals(viewName)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }

                    if("com.tencent.mobileqq.search.activity.UniteSearchActivity".equals(viewName)){
                        //搜索好友页面，判断是输入账号还是查看是否添加成功
                        info = rootNodeInfo;
                        int checkState = isCheck(info);
                        if(checkState == 1){
                            //输入好友账号
                            JSONArray jsonArray = accountMap.get(qqAccount.get(qqIndex));
                            JSONObject json = new JSONObject(jsonArray.get(friendIndex).toString());
                            String account = json.getString("account");
                            inputFriendAccount(info, myAccessibilityService, account);
                        }else if(checkState == 2){
                            //查看是否添加成功
                            JSONArray jsonArray = accountMap.get(qqAccount.get(qqIndex));
                            JSONObject json = new JSONObject(jsonArray.get(friendIndex).toString());
                            String account = json.getString("account");
                            boolean isAdd = isAdd(info, myAccessibilityService, json);
                            if(isAdd){
                                //已经添加修改好友状态
                                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ好友状态",
                                        DataUtil.setQQAddFriendInfo(account, 4, qqAccount.get(qqIndex), addFriendId, 0));
                                MainActivity.myClient.send(msg);
                            }else{
                                //添加失败，查询下一个
                                friendIndex++;
                            }

                        }

                    }

                    if("android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.activity.ChatActivity".equals(viewName)){
                        //聊天页面，检查是否有回复消息
                        JSONArray jsonArray = accountMap.get(qqAccount.get(qqIndex));
                        JSONObject json = new JSONObject(jsonArray.get(friendIndex).toString());
                        if(json.getInt("state") == 3 && !isSendMsg){
                            //还没发送消息
                            boolean isSend = sendMsg(info, myAccessibilityService);
                            if(isSend){
                                //延迟，等待回复
                                isSendMsg = true;
                                Thread.sleep(1000 * 10);
                            }
                        }else{
                            //已经发送过消息，查看是否回复
                            boolean isReply = isReply(rootNodeInfo);
                            if(!isReply){
                                if(json.getInt("state") == 4){
                                    //如果没有回复再发一条消息
                                    sendMsg(info, myAccessibilityService);
                                    Thread.sleep(3000);
                                }
                            }else{
                                //已回复，修改好友状态
                                try{
                                    String account = json.getString("account");
                                    String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ好友状态",
                                            DataUtil.setQQAddFriendInfo(account, 6, qqAccount.get(qqIndex), addFriendId, 0));
                                    MainActivity.myClient.send(msg);
                                }catch (Exception e){
                                    Log.e(TAG, "向服务器回调失败", e);
                                }
                            }
                            //后退
                            if(!clickBackBtn(rootNodeInfo, myAccessibilityService)){
                                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }
                            viewName = "com.tencent.mobileqq.activity.SplashActivity";
                            isSendMsg = false;
                            friendIndex++;
                        }
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mobileqq") > -1 ){
                        viewName = className;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }

                    if("com.tencent.mobileqq.search.activity.UniteSearchActivity".equals(viewName)){
                        //搜索好友页面，判断是输入账号还是查看是否添加成功
                        info = rootNodeInfo;
                        int checkState = isCheck(info);
                        if(checkState == 1){
                            //输入好友账号
                            JSONArray jsonArray = accountMap.get(qqAccount.get(qqIndex));
                            JSONObject json = new JSONObject(jsonArray.get(friendIndex).toString());
                            String account = json.getString("account");
                            inputFriendAccount(info, myAccessibilityService, account);
                        }else if(checkState == 2){
                            //查看是否添加成功
                            JSONArray jsonArray = accountMap.get(qqAccount.get(qqIndex));
                            JSONObject json = new JSONObject(jsonArray.get(friendIndex).toString());
                            String account = json.getString("account");
                            boolean isAdd = isAdd(info, myAccessibilityService, json);
                            if(isAdd){
                                //已经添加修改好友状态
                                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ好友状态",
                                        DataUtil.setQQAddFriendInfo(account, 4, qqAccount.get(qqIndex), addFriendId, 0));
                                MainActivity.myClient.send(msg);
                            }else{
                                //添加失败，查询下一个
                                friendIndex++;
                            }

                        }

                    }

                    if(!"com.tencent.mobileqq.activity.SplashActivity".equals(className) && !"com.tencent.mobileqq.search.activity.UniteSearchActivity".equals(className)
                        && !"com.tencent.mobileqq.activity.ChatActivity".equals(className)){
                        //跳转到了其他页面，后退
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }

                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "QQ好友检查操作失败", e);
        }
    }

    /**
     * 切换QQ账号
     * @return
     */
    public static boolean changeAccount(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            //要更换的QQ号
            QQChangeAccount.resetParam();
            QQChangeAccount.qqAccount = qqAccount.get(qqIndex);
            MyAccessibilityService.isRun = 1;
            if("com.tencent.mobileqq.activity.SplashActivity".equals(viewName) || "".equals(viewName)){
                GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 1);
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                Thread.sleep(1000);
                gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
            }else{
                Thread.sleep(1000);
                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                Thread.sleep(1000);
                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
            }
            viewName = "com.tencent.mobileqq.activity.SplashActivity";
            MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_CHANGE_ACCOUNT.getCode());
            return true;
        }catch (Exception e){
            Log.e(TAG, "切换QQ号失败", e);
        }
        return false;
    }

    /**
     * 点击搜索框
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSearchBox(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击搜索框");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = rootNodeInfo.findAccessibilityNodeInfosByText("搜索");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 1){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击搜索框失败", e);
        }
        return false;
    }

    /**
     * 判断是否输入好友账号
     * @param rootNodeInfo
     * @return
     */
    public static int isCheck(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否输入好友账号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/et_search_keyword");
            Log.i(TAG, "搜索框数量：" +  searchCounts.size());
            if(searchCounts.size() > 0){
                String text = searchCounts.get(0).getText() == null ? "" : searchCounts.get(0).getText().toString();
                //如果相等则为查看好友是否存在，不相等为输入好友账号
                JSONArray jsonArray = accountMap.get(qqAccount.get(qqIndex));
                JSONObject json = new JSONObject(jsonArray.get(friendIndex).toString());
                if(text.equals(json.getString("account"))){
                    return 2;
                }else{
                    return 1;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否输入好友账号失败", e);
        }
        return 0;
    }

    /**
     * 输入好友账号
     * @param rootNodeInfo
     * @param account
     * @return
     */
    public static boolean inputFriendAccount(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService, String account){
        try{
            Log.i(TAG, "输入好友账号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/et_search_keyword");
            Log.i(TAG, "搜索框数量：" +  searchCounts.size());
            if(searchCounts.size() > 0){
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, account);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(2000);
            }
        }catch (Exception e){
            Log.e(TAG, "输入好友账号失败", e);
        }
        return false;
    }

    /**
     * 查看是否添加
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean isAdd(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService, JSONObject json){
        boolean isAdd = false;
        try{
            Log.i(TAG, "查看是否添加");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> textCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.LINKMAN_TEXT_NAME);
            List<AccessibilityNodeInfo> text2Counts = rootNodeInfo.findAccessibilityNodeInfosByText("最近常用");
            Log.i(TAG, "联系人文本框数量：" + textCounts.size());
            if(textCounts.size() < 2 ){
                textCounts = rootNodeInfo.findAccessibilityNodeInfosByText("最近常用");
                Log.i(TAG, "最近常用文本框数量：" + text2Counts.size());
            }
            if(textCounts.size() > 0){
                isAdd = true;
                Rect rect = new Rect();
                textCounts.get(0).getBoundsInScreen(rect);
                Point position = new Point(rect.left + (int)(rect.width() / 2), rect.bottom + 100);
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                GestureDescription gesture = builder.build();
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
            }else{
                Log.i(TAG, "点击清空搜索框");
                List<AccessibilityNodeInfo> clearBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/ib_clear_text");
                Log.i(TAG, "清空按钮数量：" + clearBtnCounts.size());
                if(clearBtnCounts.size() > 0){
                    GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(clearBtnCounts.get(0));
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
            }

        }catch (Exception e){
            Log.i(TAG, "查看是否添加失败", e);
        }
        return isAdd;
    }

    /**
     * 发送消息
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean sendMsg(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "发送消息");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> inputCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/input");
            Log.i(TAG, "输入框数量：" + inputCounts.size());
            if(inputCounts.size() > 0){
                //发送按钮
                List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SEND_BTN_NAME);
                Log.i(TAG, "发送按钮数量：" + sendBtnCounts.size());
                if(sendBtnCounts.size() > 0){
                    //循环发送消息
                    for(String msg : msgList){
                        //输入消息
                        Thread.sleep(1000 * 5);
                        Bundle arguments  = new Bundle();
                        arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, msg);
                        inputCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                        inputCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                        Thread.sleep(1000);
                        //点击发送
                        GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(sendBtnCounts.get(0));
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                    }
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "发送消息失败", e);
        }
        return false;
    }

    /**
     * 查看是否回复
     * @param rootNodeInfo
     * @return
     */
    public static boolean isReply(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "查看是否回复");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> headImgCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/chat_item_head_icon");
            Log.i(TAG, "头像数量：" + headImgCounts.size());
            if(headImgCounts.size() > 0){
                //获取最后一个头像，判断是否为好友回复消息
                AccessibilityNodeInfo lastMsg = headImgCounts.get(headImgCounts.size() - 1).getParent();
                AccessibilityWechatUtil.clickCoordinate(lastMsg);
                AccessibilityNodeInfo lastChild = lastMsg.getChild(lastMsg.getChildCount() - 1);
                if(!"android.widget.ImageView".equals(lastChild.getClassName())){
                    //最后一个组件不是头像则表示为好友回复
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "查看是否回复失败", e);
        }
        return false;
    }

    /**
     * 点击返回按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickBackBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击返回按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> titleBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/rlCommenTitle");
            Log.i(TAG, "标题组件数量：" + titleBtnCounts.size());
            if(titleBtnCounts.size() > 0){
                Rect rect = new Rect();
                titleBtnCounts.get(0).getBoundsInScreen(rect);
                Point position = new Point(rect.left + 50, rect.top + (int)(rect.height() / 2));
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                GestureDescription gesture = builder.build();
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击返回失败", e);
        }
        return false;
    }

    /**
     * 判断是否结束
     * @return
     */
    public static boolean isFinish(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            if(qqAccount == null || qqAccount.size() == 0){
                finish();
                return true;
            }
            JSONArray jsonArray = accountMap.get(qqAccount.get(qqIndex));
            if("".equals(QQChangeAccount.qqAccount)){
                //切换账号
                changeAccount(rootNodeInfo, myAccessibilityService);
            }
            if(jsonArray == null || friendIndex >= jsonArray.length() || !QQChangeAccount.isSuccess){
                //该QQ号添加好友判断结束
                Log.i(TAG, qqAccount.get(qqIndex) + "添加好友判断结束");
                qqIndex++;
                friendIndex = 0;
                if(qqIndex < qqAccount.size()){
                    //切换账号
                    changeAccount(rootNodeInfo, myAccessibilityService);
                }
            }
            if(qqIndex >= qqAccount.size()){
                finish();
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否结束失败", e);
        }
        return false;
    }

    /**
     * 结束任务
     */
    public static void finish(){
        try{
            //全部QQ好友判断结束
            Log.i(TAG, "全部QQ好友判断结束");
            qqIndex = 0;
            friendIndex = 0;
            qqAccount = new ArrayList<String>();
            msgList = new ArrayList<String>();
            accountMap = new HashMap<String, JSONArray>();
            isSendMsg = false;
            viewName = "";
            try{
                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "完成任务",
                        DataUtil.setfinishTask(MyAccessibilityService.taskId, MainActivity.MOBILE_IMEI));
                MainActivity.myClient.send(msg);
            }catch (Exception e){
                Log.i(TAG, "完成任务回调失败", e);
            }
            QQChangeAccount.resetParam();
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "QQ好友检查结束";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "返回我的APP");
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "结束任务失败", e);
        }
    }

}
