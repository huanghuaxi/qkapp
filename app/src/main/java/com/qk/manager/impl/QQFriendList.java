package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;

import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 获取QQ好友列表
 */
public class QQFriendList implements TaskIntfService {

    private static final String TAG = QQFriendList.class.getName();

    private static String viewName = "";
    //好友列表
    public static List<Map<String, String>> friendList = new ArrayList<Map<String, String>>();
    //好友索引
    private static int index = 0;
    //是否滑动到顶部
    private static boolean isTop = true;
    //是否到达底部
    private static int isBottom = 0;
    //底部菜单栏高度
    private static int menuHeight = 0;
    //列表数量
    private static int listCount = 0;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{

            Log.d(TAG, "获取QQ好友列表");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if(info.getChild(i).getText() == null){
                                    continue;
                                }
                                if (Constants.LINKMAN_TEXT_NAME.equals(info.getChild(i).getText())) {
                                    //联系人页面，点击好友按钮
                                    boolean isClick = clickFriendBtn(rootNodeInfo, myAccessibilityService);
                                    if(!isClick){
                                        //没有搜索框再次点击联系人返回顶部
                                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                                        if(gesture != null){
                                            MyAccessibilityService.flag = 1;
                                            myAccessibilityService.opScreen(gesture);
                                        }
                                    }else{
                                        isTop = false;
                                        //向上滑动
                                        upOp(rootNodeInfo, myAccessibilityService);
                                    }
                                    break;
                                }

                                /*if ("好友".equals(info.getChild(i).getText())) {
                                    if(isMenuTop(rootNodeInfo)){
                                        //联系人页面点击好友按钮后，查看有没字母列
                                        clickLetterList(rootNodeInfo, myAccessibilityService, 1);
                                    }else{
                                        clickLetterList(rootNodeInfo, myAccessibilityService, 0);
                                    }

                                    break;
                                }*/
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(viewName)){
                        //判断是否到达顶部
                        if(!isTop){
                            isTop = isTop(info);
                            if(!isTop){
                                //向上滑动
                                upOp(rootNodeInfo, myAccessibilityService);
                            }else{
                                //开始向下滑动
                                isBottom = 1;
                            }
                        }
                        if(isBottom != 0){
                            //查看是否为好友页面
                            boolean isFriend = isFriendList(rootNodeInfo);
                            if(!isFriend){
                                //不是好友列表页面，点击好友按钮
                                clickFriendBtn(rootNodeInfo, myAccessibilityService);
                                break;
                            }
                        }
                        if(isBottom == 1){
                            //点击列表获取qq号
                            clickFriendList(rootNodeInfo, myAccessibilityService);

                        }
                        if(isBottom == 3){
                            // 判断是否到达底部
                            if(isBottom(info)){
                                //滑动到底部结束
                                Log.i(TAG, "滑动到底部");
                                //点击列表获取qq号
                                clickFriendList(rootNodeInfo, myAccessibilityService);
                                isBottom = 1;
                            }else{
                                //向下滑动
                                downOp(rootNodeInfo, myAccessibilityService);
                                isBottom = 1;
                            }
                        }
                        if(isBottom == 4){
                            //结束
                            finish();
                        }

                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mobileqq") > -1){
                        viewName = className;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        if(isBottom == 0){
                            //主页，点击联系人
                            GestureDescription gesture = AccessibilityWechatUtil.doubleClickQQTab(rootNodeInfo, 2);
                            if(gesture != null){
                                MyAccessibilityService.flag = 1;
                                myAccessibilityService.opScreen(gesture);
                            }
                            //获取底部菜单栏高度
                            getBottomMenu(rootNodeInfo);
                        }
                    }else if("com.tencent.mobileqq.activity.FriendProfileCardActivity".equals(className)){
                        //好友资料页面，获取好友名称与QQ号
                        getFriendInfo(info);
                        //后退
                        boolean isBack = clickBackBtn(rootNodeInfo, myAccessibilityService);
                        if(!isBack){
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }
                    }else{
                        //跳转到其他页面后退
                        boolean isBack = clickBackBtn(rootNodeInfo, myAccessibilityService);
                        if(!isBack){
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }
                    }

                    break;
            }
            /*if(info != null){
                info.recycle();
                info = null;
            }
            if(rootNodeInfo != null){
                rootNodeInfo.recycle();
            }*/
        }catch (Exception e){
            Log.e(TAG, "获取QQ好友列表操作失败", e);
        }

    }

    /**
     * 点击好友按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickFriendBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击好友按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> friendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("好友");
            Log.i(TAG, "好友按钮数量:" + friendBtnCounts.size());
            if(friendBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(friendBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                friendBtnCounts.get(0).recycle();
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击好友按钮失败", e);
        }
        return false;
    }

    /**
     * 点击字母列
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickLetterList(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService, int type){
        try{
            Log.i(TAG, "点击字母列");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> listCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(listCounts, rootNodeInfo, "android.view.View");
            Log.i(TAG, "view数量：" + listCounts.size());
            if(listCounts.size() > 0){
                AccessibilityNodeInfo viewInfo = null;
                for(int i = 0;i < listCounts.size();i++){
                    Rect rect = new Rect();
                    listCounts.get(i).getBoundsInScreen(rect);
                    if(rect.width() < 100 && rect.width() > 0){
                        viewInfo = listCounts.get(i);
                        break;
                    }
                }
                /*List<AccessibilityNodeInfo> absListCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(absListCounts, listCounts.get(0), "android.widget.AbsListView");*/
                /*for(int i = 0;i < absListCounts.size();i++){
                    AccessibilityWechatUtil.clickCoordinate(absListCounts.get(i));
                }*/
//                Log.i(TAG, "abs组件数量：" + absListCounts.size());
                /*AccessibilityNodeInfo viewCounts = listCounts.get(0);
                for(int i = 0;i < viewCounts.getChildCount();i++){
                    AccessibilityWechatUtil.clickCoordinate(viewCounts.getChild(i));
                }
                Log.i(TAG, "view子元素数量：" + viewCounts.getChildCount());*/
                if(viewInfo != null){
                    if(type == 0){
                        //点击最后一个字母
                        Rect rect = new Rect();
                        viewInfo.getBoundsInScreen(rect);
                        Point position1 = new Point(rect.left + (int)(rect.width() / 2), rect.bottom - 30);
                        GestureDescription.Builder builder = new GestureDescription.Builder();
                        Path p1 = new Path();
                        p1.moveTo(position1.x, position1.y);
                        Path p2 = new Path();
                        p1.moveTo(50, 50);
                        builder.addStroke(new GestureDescription.StrokeDescription(p1, 0L, 100L));
                        builder.addStroke(new GestureDescription.StrokeDescription(p2, 1000L, 100L));
                        GestureDescription gesture = builder.build();
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                    }else{
                        //点击最后一个字母再点击第一个字母
                        Rect rect = new Rect();
                        viewInfo.getBoundsInScreen(rect);
                        Point position1 = new Point(rect.left + (int)(rect.width() / 2), rect.bottom - 30);
                        Point position2 = new Point(rect.left + (int)(rect.width() / 2), rect.top + 30);
                        GestureDescription.Builder builder = new GestureDescription.Builder();
                        Path p1 = new Path();
                        p1.moveTo(position1.x, position1.y);
                        Path p2 = new Path();
                        p2.moveTo(position2.x, position2.y);
                        builder.addStroke(new GestureDescription.StrokeDescription(p1, 0L, 100L));
                        builder.addStroke(new GestureDescription.StrokeDescription(p2, 1000L, 100L));
                        GestureDescription gesture = builder.build();
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                    }

                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击字母列失败", e);
        }
        return false;
    }

    /**
     * 判断是否到达顶部
     * @param rootNodeInfo
     * @return
     */
    public static boolean isTop(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否到达顶部");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchViewCounts = rootNodeInfo.findAccessibilityNodeInfosByText("下拉刷新");
            Log.i(TAG, "下拉刷新数量：" + searchViewCounts.size());
            if(searchViewCounts.size() > 0){
                MyAccessibilityService.recycleAll(searchViewCounts);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否到达顶部", e);
        }
        return false;
    }

    /**
     * 判断是否到达底部
     * @param rootNodeInfo
     * @return
     */
    public static boolean isBottom(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否到达底部");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> bottomCounts = rootNodeInfo.findAccessibilityNodeInfosByText("位联系人");
            Log.i(TAG, "底部文本数量：" + bottomCounts.size());
            if(bottomCounts.size() > 0){
                AccessibilityWechatUtil.clickCoordinate(bottomCounts.get(0));
                Rect rect = new Rect();
                Rect rect1 = new Rect();
                bottomCounts.get(0).getBoundsInScreen(rect);
                rootNodeInfo.getBoundsInScreen(rect1);
                bottomCounts.get(0).recycle();
                if(rect.bottom <= rect1.height() - menuHeight + 10){
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否到达底部失败", e);
        }
        return false;
    }

    /**
     * 向上滑动
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean upOp(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "向上滑动");
            Thread.sleep(1000);
            //获取好友列位置
            List<AccessibilityNodeInfo> tabCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.GROUP_CHAT_BTN_NAME);
            Log.i(TAG, "tab数量：" + tabCounts.size());
            if(tabCounts.size() > 0){
                Rect rect = new Rect();
                tabCounts.get(0).getBoundsInScreen(rect);
                GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(rect.left + rect.width() / 2, rect.bottom + 50,
                        rect.left + rect.width() / 2, rect.bottom + 250);
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
            }

        }catch (Exception e){
            Log.e(TAG, "向上滑动失败", e);
        }
        return false;
    }

    /**
     * 向下滑动
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean downOp(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "向下滑动");
            Thread.sleep(1000);
            GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(400, 600, 400, 400);
            MyAccessibilityService.flag = 1;
            myAccessibilityService.opScreen(gesture);
        }catch (Exception e){
            Log.e(TAG, "向下滑动失败", e);
        }
        return false;
    }

    /**
     * 判断菜单栏是否在顶部
     * @param rootNodeInfo
     * @return
     */
    public static boolean isMenuTop(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否菜单在顶部");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> viewCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(viewCounts, rootNodeInfo, "android.widget.HorizontalScrollView");
            Log.i(TAG, "菜单栏数量：" + viewCounts.size());
            if(viewCounts.size() > 0){
                Rect rect = new Rect();
                viewCounts.get(0).getBoundsInScreen(rect);
                viewCounts.get(0).recycle();
                if(rect.top == 0){
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否菜单在顶部失败", e);
        }
        return false;
    }

    /**
     * 点击好友列表
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickFriendList(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try {
            Log.i(TAG, "点击好友列表");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> listCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/text1");

            Log.i(TAG, "列表数量：" + listCounts.size());
            if(listCounts.size() > 0){
                if(listCounts.size() > listCount){
                    listCount = listCounts.size();
                }
                GestureDescription gesture = null;
                int num = index - (listCount - listCounts.size());
                if(num < 0){
                    num = 0;
                }
                if(num >= listCounts.size() && listCounts.size() != 0){
                    //遍历完所有好友，结束
                    isBottom = 4;
                    return false;
                }
                Rect rect1 = new Rect();
                Rect rect2 = new Rect();
                listCounts.get(num).getParent().getBoundsInScreen(rect1);
                rootNodeInfo.getBoundsInScreen(rect2);
                Log.i(TAG, "top:" + rect1.top + "   height2:" + rect2.height() +  "   height1：" + rect1.height() + "   x:" + (rect2.height() - rect1.top) + " width:" + rect1.width());
                if(rect1.width() < 0){
                    //遍历完所有好友，结束
                    isBottom = 4;
                    return false;
                }
                if(rect2.height() - rect1.top < menuHeight + 200){
                    //超出页面范围下拉
                    isBottom = 3;
                    return false;
                }
                gesture = AccessibilityWechatUtil.clickCoordinate(listCounts.get(num).getParent());
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                listCounts.get(index).recycle();
            }
        }catch (Exception e){
            Log.e(TAG, "点击好友列表失败", e);
        }
        return false;
    }

    /**
     * 获取好友信息
     * @param rootNodeInfo
     * @return
     */
    public static boolean getFriendInfo(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "获取好友资料");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> nameCounts = rootNodeInfo.findAccessibilityNodeInfosByText("昵称");
            List<AccessibilityNodeInfo> accountCounts = rootNodeInfo.findAccessibilityNodeInfosByText("QQ：");
            List<AccessibilityNodeInfo> buttonCounts = rootNodeInfo.findAccessibilityNodeInfosByText("音视频通话");
            Log.i(TAG, "昵称文本数量：" + nameCounts.size());
            Log.i(TAG, "qq号文本数量：" + accountCounts.size());
            Log.i(TAG, "音视频通话按钮数量：" + buttonCounts.size());
            if(buttonCounts.size() == 0){
                //非好友资料页面，不加入好友列表
                index++;
                return true;
            }
            if(accountCounts.size() > 0){
                Map<String, String> friendMap = new HashMap<String, String>();
                String qq = accountCounts.get(0).getText() + "";
                accountCounts.get(0).recycle();
                qq = qq.replace("QQ：", "");
                if(qq.indexOf("(") > -1){
                    qq = qq.substring(0, qq.indexOf("(")).trim();
                }
                friendMap.put("qq", qq);
                String name = nameCounts.get(0).getChild(0).getText() + "";
                nameCounts.get(0).recycle();
                friendMap.put("name", name);
                friendList.add(friendMap);
                index++;
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "获取好友资料失败", e);
        }
        return false;
    }

    /**
     * 获取底部菜单栏高度
     * @param rootNodeInfo
     * @return
     */
    public static boolean getBottomMenu(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "获取底部菜单栏高度");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> menuCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("android:id/tabs");
            Log.i(TAG, "底部菜单栏数量：" + menuCounts.size());
            if(menuCounts.size() > 0){
                Rect rect = new Rect();
                menuCounts.get(0).getBoundsInScreen(rect);
                menuCounts.get(0).recycle();
                menuHeight = rect.height();
                return true;
            }
        }catch (Exception e){
            Log.i(TAG, "获取底部菜单栏高度失败", e);
        }
        return false;
    }

    /**
     * 判断是否为好友列表页面
     * @param rootNodeInfo
     * @return
     */
    public static boolean isFriendList(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否为好友列表页面");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> listCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/group_item_layout");
            Log.i(TAG, "列表类型数量：" + listCounts.size());
            if(listCounts.size() > 0){
                for(AccessibilityNodeInfo info : listCounts){
                    Rect rect = new Rect();
                    info.getBoundsInScreen(rect);
                    info.recycle();
                    if(rect.width() > 0){
                        return false;
                    }
                }
            }
            return true;
        }catch (Exception e){
            Log.e(TAG, "判断是否为好友列表页面失败", e);
        }
        return false;
    }

    /**
     * 点击后退按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickBackBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击后退按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> backBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/ivTitleBtnLeft");
            Log.i(TAG, "后退按钮数量：" + backBtnCounts.size());
            if(backBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(backBtnCounts.get(0));
                backBtnCounts.get(0).recycle();
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击后退按钮失败", e);
        }
        return false;
    }

    /**
     * 结束任务
     */
    public static void finish(){
        try{
            index = 0;
            isTop = false;
            isBottom = 0;
            menuHeight = 0;
            viewName = "";
            if(friendList.size() == 0){
                friendList = null;
            }
            MyAccessibilityService.isRun = 0;
            MyAccessibilityService.taskType = "";
            MyAccessibilityService.className = "";
            Log.i(TAG, "返回APP");
            Thread.sleep(1000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "结束获取QQ好友列表失败", e);
        }
    }

}
