package com.qk.manager.impl;

import android.accessibilityservice.GestureDescription;
import android.graphics.Rect;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;

/**
 * QQ获取群聊列表
 */
public class QQGroupList implements TaskIntfService {

    private static final String TAG = QQGroupList.class.getName();

    private static String viewName = "";
    //群聊列表
    public static List<String> groupList = new ArrayList<String>();
    //分类索引
    private static int typeIndex = 1;
    //向下滑动
    private static int bottomType = 0;
    //是否切换账号
    private static boolean isChange = true;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{

            if(isChange && QQChangeAccount.accountNum != -1){
                //切换账号
                viewName = "com.tencent.mobileqq.activity.SplashActivity";
                MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_CHANGE_ACCOUNT.getCode());
                isChange = false;
                return ;
            }
            if(QQChangeAccount.accountNum == -1){
                //全部切换完成结束拉人进群任务
                finishTask();
                return ;
            }

            Log.d(TAG, "获取QQ群聊列表");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if(info.getChild(i).getText() == null){
                                    continue;
                                }
                                if (Constants.LINKMAN_TEXT_NAME.equals(info.getChild(i).getText()) && bottomType == 0) {
                                    //联系人页面，点击群聊按钮
                                    boolean isClick = clickGroupBtn(rootNodeInfo, myAccessibilityService);
                                    if(!isClick){
                                        //没有搜索框再次点击联系人返回顶部
                                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                                        if(gesture != null){
                                            MyAccessibilityService.flag = 1;
                                            myAccessibilityService.opScreen(gesture);
                                        }
                                    }else{
                                        GestureDescription gesture = AccessibilityWechatUtil.doubleClickQQTab(rootNodeInfo, 2);
                                        MyAccessibilityService.flag = 1;
                                        myAccessibilityService.opScreen(gesture);
                                    }
                                    break;
                                }

                                if (Constants.GROUP_CHAT_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //群聊按钮被点击，获取列表
                                    int type = clickTblList(rootNodeInfo, myAccessibilityService);
                                    if(type == 1){
                                        //结束
                                        finish();
                                    }
                                    if(type == 2){
                                        //没有分类列表，下拉完结束
                                        bottomType = 1;
//                                        downOp(rootNodeInfo, myAccessibilityService);
                                    }
                                    if(type == 3){
                                        //点击分类列表，开始下拉
                                        bottomType = 1;
//                                        downOp(rootNodeInfo, myAccessibilityService);
                                    }
                                    if(bottomType == 1){
                                        boolean isSuccess = getGroupInfo(rootNodeInfo);
                                        if(isSuccess){
                                            //获取成功
                                            if(typeIndex == 1){
                                                //没有群分类结束任务
                                                finish();
                                            }else{
                                                //点击联系人按钮获取下一个群分类列表
                                                bottomType = 0;
                                            }

                                        }
                                    }
                                    break;
                                }

                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(viewName)){

                        if(bottomType == 0){
                            //主页，点击联系人
                            GestureDescription gesture = AccessibilityWechatUtil.doubleClickQQTab(rootNodeInfo, 2);
                            if(gesture != null){
                                MyAccessibilityService.flag = 1;
                                myAccessibilityService.opScreen(gesture);
                            }
                        }
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mobileqq") > -1){
                        viewName = className;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.doubleClickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                        //获取底部菜单栏高度
//                        getBottomMenu(rootNodeInfo);
                    }

                    break;
            }


        }catch (Exception e){
            Log.e(TAG, "QQ获取群聊列表失败", e);
        }
    }

    /**
     * 点击群聊按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickGroupBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击群聊按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> friendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.GROUP_CHAT_BTN_NAME);
            Log.i(TAG, "群聊按钮数量:" + friendBtnCounts.size());
            if(friendBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(friendBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击群聊按钮失败", e);
        }
        return false;
    }

    /**
     * 点击分类列表
     * @param rootNodeInfo
     * @return
     */
    public static int clickTblList(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击分类列表");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> listCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/group_item_layout");
            Log.i(TAG, "分类列表数量：" + listCounts.size());
            //type 1:有分类列表已经都点完 2：没有分类列表 3：有分类列表并且点击
            int type = 2;
            for(int i = listCounts.size() - 1;i >= 0;i--){
                Rect rect = new Rect();
                listCounts.get(i).getBoundsInScreen(rect);
                if(rect.width() > 10 && rect.height() > 10){
                    type = 1;
                    if(listCounts.size() - i == typeIndex){
                        Thread.sleep(1000);
                        GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(listCounts.get(listCounts.size() - typeIndex));
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                        typeIndex++;
                        return 3;
                    }
                }
            }
            return type;
        }catch (Exception e){
            Log.e(TAG, "点击分类列表失败", e);
        }
        return 0;
    }

    /**
     * 获取群信息
     * @param rootNodeInfo
     * @return
     */
    public static boolean getGroupInfo(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "获取群信息");
            Thread.sleep(5000);
            List<AccessibilityNodeInfo> listCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/text1");
            Log.i(TAG, "文本数量：" + listCounts.size());
            for(AccessibilityNodeInfo text : listCounts){
                Log.i(TAG, "text:" + text.getText());
                AccessibilityWechatUtil.clickCoordinate(text);
                if(text.getText() != null){
                    groupList.add(text.getText().toString());
                }
            }
            return true;
        }catch (Exception e){
            Log.e(TAG, "获取群信息失败", e);
        }
        return false;
    }

    /**
     * 向下滑动
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean downOp(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "向下滑动");
            Thread.sleep(1000);
            GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(400, 600, 400, 400);
            MyAccessibilityService.flag = 1;
            myAccessibilityService.opScreen(gesture);
        }catch (Exception e){
            Log.e(TAG, "向下滑动失败", e);
        }
        return false;
    }

    /**
     * 任务结束
     */
    public static void finish(){
        try{
            viewName = "";
            typeIndex = 1;
            bottomType = 0;
            isChange = true;
            if(groupList.size() == 0){
                groupList = null;
            }
            MyAccessibilityService.isRun = 0;
            MyAccessibilityService.taskType = "";
            MyAccessibilityService.className = "";
            Log.i(TAG, "返回APP");
            Thread.sleep(1000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "QQ获取群列表结束失败", e);
        }
    }

    /**
     * 结束拉人进群任务
     */
    public static void finishTask(){
        try{
            //QQ账号已全部切换完成，拉人进群任务结束
            viewName = "";
            isChange = true;
            try{
                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "完成任务",
                        DataUtil.setfinishTask(MyAccessibilityService.taskId, MainActivity.MOBILE_IMEI));
                MainActivity.myClient.send(msg);
            }catch (Exception e){
                Log.e(TAG, "完成任务回调失败", e);
            }
            QQChangeAccount.resetParam();
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "拉人进群操作结束";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "后退到主页面");
            Thread.sleep(1000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "结束拉人进群任务失败", e);
        }
    }

}
