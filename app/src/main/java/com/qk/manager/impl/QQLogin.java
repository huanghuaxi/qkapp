package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * QQ登录
 */
public class QQLogin implements TaskIntfService {

    private static final String TAG = QQLogin.class.getName();

    //当前页面
    public static String viewName = "";
    //QQ账号列表
    public static JSONObject accountMap = new JSONObject();
    //当前账号列表
    private static List<String> accountList = new ArrayList<String>();
    //登录失败的账号
    private static List<String> failList = new ArrayList<String>();
    //标识 0：切换账号，1：删除账号
    private static int flag = 0;
    //是否切换账号
    private static boolean isChange = false;
    //当前账号
    private static String qqAccount = "";

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{

            Log.d(TAG, "QQ切换账号");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr == null ? "" : classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");

                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if (info == null) {
                        info = rootNodeInfo;
                    }

                    if("com.tencent.mobileqq.activity.LoginActivity".equals(viewName) && "android.widget.RelativeLayout".equals(className)){
                        //登录页面
                        if(!isChange){
                            loginPageOp(rootNodeInfo, info, myAccessibilityService);
                        }
                    }

                    if("com.tencent.mobileqq.activity.LoginActivity".equals(viewName) && "android.widget.ListView".equals(className)){
                        //账号列表页面
                        if(flag == 0){
                            //切换账号
                            isChange = false;
                            boolean isChangeQQ = changeAccount(info, myAccessibilityService);
                            if(!isChangeQQ){
                                //切换失败，登录结束
                                finish(false);
                            }
                        }else{
                            //删除账号
                            String account = getSelectAccount(rootNodeInfo);
                            if(account != null){
                                boolean isClick = removeAccount(info, myAccessibilityService, account);
                                if(isClick){
                                    isChange = false;
                                    flag = 0;
                                }
                            }
                        }

                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mobileqq") > -1) {
                        viewName = className;
                    }

                    if (info == null) {
                        info = rootNodeInfo;
                    }
                    if("com.tencent.mobileqq.activity.LoginActivity".equals(className)){
                        //登录页面，判断是否为登录失败弹窗
                        loginPageOp(rootNodeInfo, info, myAccessibilityService);
                    }

                    if("com.tencent.mobileqq.activity.NotificationActivity".equals(className)){
                        //qq账号冻结提示页面，删除账号
                        flag = 1;
                        isChange = true;
                        clickCancelBtn(rootNodeInfo, myAccessibilityService);
                        try{
                            //向服务器发送修改QQ账号状态消息
                            String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ账号状态",
                                    DataUtil.setQQTargetStateInfo(qqAccount, 0));
                            MainActivity.myClient.send(msg);
                        }catch (Exception e){
                            Log.i(TAG, "向服务器回调失败", e);
                        }
                    }

                    if("android.app.Dialog".equals(className)){
                        //删除账号弹窗，点击删除按钮
                        isChange = true;
                        clickRemoveBtn(info, myAccessibilityService);
                    }

                    if("com.tencent.mobileqq.activity.QQBrowserActivity".equals(className)){
                        //QQ登录验证页面，后退切换账号
                        isChange = true;
                        boolean isClick = clickBackBtn(info, myAccessibilityService);
                        if(!isClick){
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }
                        try{
                            //向服务器发送修改QQ账号备注消息
                            String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ账号备注",
                                    DataUtil.setQQTargetRemarkInfo(qqAccount, "登录需要手动验证"));
                            MainActivity.myClient.send(msg);
                        }catch (Exception e){
                            Log.i(TAG, "向服务器回调失败", e);
                        }
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        //主页，登录成功
                        finish(true);
                    }

                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "QQ登录操作失败", e);
        }
    }

    /**
     * 登录页面操作
     * @param rootNodeInfo
     * @param info
     * @param myAccessibilityService
     */
    private static void loginPageOp(AccessibilityNodeInfo rootNodeInfo, AccessibilityNodeInfo info, MyAccessibilityService myAccessibilityService){
        boolean isWrong = isPwdWrong(rootNodeInfo, myAccessibilityService);
        if(isWrong){
            isChange = true;
            try{
                //向服务器发送修改QQ账号备注消息
                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ账号备注",
                        DataUtil.setQQTargetRemarkInfo(qqAccount, "密码错误"));
                MainActivity.myClient.send(msg);
            }catch (Exception e){
                Log.i(TAG, "向服务器回调失败", e);
            }
            return ;
        }
        if(isChange){
            //是，切换下一个账号
            String account = getSelectAccount(info);
            failList.add(account);
            clickAccountListBtn(info, myAccessibilityService);
        }else{
            //不是，输入密码登录
            boolean isLogin = login(info, myAccessibilityService);
            if(!isLogin){
                //点击登录失败，切换账号
                isChange = true;
                flag = 0;
                String account = getSelectAccount(info);
                failList.add(account);
                clickAccountListBtn(info, myAccessibilityService);
            }
        }
    }

    /**
     * 判断是否为密码错误弹窗
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean isPwdWrong(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "判断是否为密码错误弹窗");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> loginFailCounts = rootNodeInfo.findAccessibilityNodeInfosByText("登录失败");
            Log.i(TAG, "登录失败数量：" + loginFailCounts.size());
            if(loginFailCounts.size() > 0){
                //点击确定按钮
                List<AccessibilityNodeInfo> confirmBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CONFIRM_BTN_NAME);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(confirmBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否为密码错误弹窗失败", e);
        }
        return false;
    }

    /**
     * 点击账号列表按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickAccountListBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击账号列表按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> accountListBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("帐号列表");
            Log.i(TAG, "账号列表按钮数量:" + accountListBtnCounts.size());
            if(accountListBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accountListBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
            }
        }catch (Exception e){
            Log.e(TAG, "点击账号列表按钮失败", e);
        }
        return false;
    }

    /**
     * 切换账号
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean changeAccount(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "切换账号");
            Thread.sleep(1000);
            List<AccessibilityWindowInfo> list = myAccessibilityService.getWindows();
            if(list.size() >= 3){
                rootNodeInfo = list.get(1).getRoot();
                List<AccessibilityNodeInfo> accountCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(accountCounts, rootNodeInfo,  "android.widget.LinearLayout");
                Log.i(TAG, "账号数量：" + accountCounts.size());
                if(accountCounts.size() > 0){
                    //获取账号列表
                    accountList = new ArrayList<String>();
                    for(AccessibilityNodeInfo nodeInfo : accountCounts){
                        if(nodeInfo.getChild(0).getText() != null){
                            accountList.add(nodeInfo.getChild(0).getText() + "");
                        }
                        Log.i(TAG, "text:" + nodeInfo.getChild(0).getText());
                        AccessibilityWechatUtil.clickCoordinate(nodeInfo.getChild(1));
                        Log.i(TAG, "=========================");
                    }
                    //切换没有登录失败的qq号
                    boolean b = true;
                    String qq = "";
                    for(String account : accountList){
                        b = true;
                        for(String fail : failList){
                            if(fail.equals(account)){
                                b = false;
                                break;
                            }
                        }
                        if(b){
                            qq = account;
                            break;
                        }
                    }
                    //点击列表
                    List<AccessibilityNodeInfo> qqCounts = rootNodeInfo.findAccessibilityNodeInfosByText(qq);
                    Log.i(TAG, "qq账号数量：" + qqCounts.size());
                    if(qqCounts.size() > 0){
                        GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(qqCounts.get(0));
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG, "切换账号失败", e);
        }
        return false;
    }

    /**
     * 删除账号
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @param account
     * @return
     */
    public static boolean removeAccount(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService, String account){
        try{
            Log.i(TAG, "删除账号");
            Thread.sleep(1000);
            List<AccessibilityWindowInfo> list = myAccessibilityService.getWindows();
            if(list.size() >= 3){
                rootNodeInfo = list.get(1).getRoot();
                List<AccessibilityNodeInfo> accountCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(accountCounts, rootNodeInfo,  "android.widget.LinearLayout");
                Log.i(TAG, "账号数量：" + accountCounts.size());
                if(accountCounts.size() > 0){
                    //删除被冻结的QQ号
                    for(AccessibilityNodeInfo nodeInfo : accountCounts){
                        if(nodeInfo.getChild(0).getText() != null && account.equals(nodeInfo.getChild(0).getText())){
                            AccessibilityWechatUtil.recycleCheck(nodeInfo);
                            Rect rect = new Rect();
                            nodeInfo.getChild(1).getBoundsInScreen(rect);
                            if(rect.height() > 10){
                                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(nodeInfo.getChild(1));
                                MyAccessibilityService.flag = 1;
                                myAccessibilityService.opScreen(gesture);
                                return true;
                            }
                        }
                    }
                    //没有找到账号，向下滑动
                    Rect rect = new Rect();
                    rootNodeInfo.getBoundsInScreen(rect);
                    int x = rect.left + rect.width() / 2;
                    int y = rect.bottom - 50;
                    GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(x, y, x, y - 100);
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "删除账号失败", e);
        }
        return false;
    }

    /**
     * 点击删除按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickRemoveBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击删除按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> removeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.REMOVE_BTN_NAME);
            Log.i(TAG, "删除按钮数量：" + removeBtnCounts.size());
            if(removeBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(removeBtnCounts.get(removeBtnCounts.size() - 1));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击删除按钮");
        }
        return false;
    }

    /**
     * 获取选中账号
     * @param rootNodeInfo
     * @return
     */
    public static String getSelectAccount(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "获取选中账号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());
            if(editCounts.size() > 0){
                return editCounts.get(0).getText() + "";
            }
        }catch (Exception e){
            Log.e(TAG, "获取选中账号失败", e);
        }
        return null;
    }

    /**
     * 登录
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean login(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "登录");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());
            if(editCounts.size() > 1){
                qqAccount = editCounts.get(0).getText() + "";
                String pwd = editCounts.get(1).getText() + "";
                if("输入密码".equals(pwd)){
                    //在账号集合中查询
                    String password = accountMap.getString(qqAccount);
                    if(password == null){
                        //没有找到该账号密码，切换下一个账号
                        return false;
                    }
                    //输入密码
                    Bundle arguments  = new Bundle();
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, password);
                    editCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    editCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    Thread.sleep(1000);
                }
                //点击登录按钮
                List<AccessibilityNodeInfo> loginBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/login");
                Log.i(TAG, "登录按钮数量：" + loginBtnCounts.size());
                if(loginBtnCounts.size() > 0){
                    if(loginBtnCounts.get(0).isClickable()){
                        GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(loginBtnCounts.get(0));
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG, "登录失败", e);
        }
        return false;
    }

    /**
     * 点击返回按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickBackBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击后退按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> backBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/ivTitleBtnLeft");
            Log.i(TAG, "返回按钮数量：" + backBtnCounts.size());
            if(backBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(backBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击后退按钮失败", e);
        }
        return false;
    }

    /**
     * 点击取消按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickCancelBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击取消按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> cancelBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CANCEL_BTN_NAME);
            Log.i(TAG, "取消按钮数量：" + cancelBtnCounts.size());
            if(cancelBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(cancelBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击取消按钮失败", e);
        }
        return false;
    }

    /**
     * 结束任务
     * @param isLogin
     */
    public static void finish(boolean isLogin){
        try{
            //重置参数
            viewName = "";
            accountList = new ArrayList<String>();
            accountMap = new JSONObject();
            flag = 0;
            failList = new ArrayList<String>();
            isChange = false;
            if(isLogin){
                //登录成功
                MyAccessibilityService.isRun = 1;
                MyAccessibilityService.className = TaskTypeEnum.getClsName(MyAccessibilityService.taskType);
            }else{
                //登录失败
                MyAccessibilityService.resetParam();
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "QQ登录失败，任务结束";
                MainActivity.mHandler.sendMessage(message);
                Log.i(TAG, "返回我的APP");
                Thread.sleep(1000);
                MainActivity.openMyApp();
            }
        }catch (Exception e){
            Log.e(TAG, "结束登录失败", e);
        }
    }

}
