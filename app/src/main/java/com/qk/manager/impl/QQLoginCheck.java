package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;

import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * QQ登录检查
 */
public class QQLoginCheck implements TaskIntfService {

    private static final String TAG = QQLoginCheck.class.getName();

    //当前页面
    public static String viewName = "";
    //QQ账号列表
    public static JSONObject accountMap = new JSONObject();
    //是否验证完成
    public static boolean isCheckFinish = false;
    //账号数量
    public static Integer accountNum = 0;
    //账号列表
    public static List<String> accountList = new ArrayList<String>();
    //账号索引
    public static Integer accountIndex = 0;
    //切换是否成功
    public static boolean isSuccess = true;
    //是否为已登录的号
    public static boolean isLogin = false;
    //是否切换账号
    private static boolean isChange = false;
    //是否删除账号
    private static boolean isRemove = false;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            Log.d(TAG, "QQ切换账号");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr == null ? "" : classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            if(isFinish()){
                return ;
            }

            if("".equals(viewName)){
                viewName = "com.tencent.mobileqq.activity.SplashActivity";
            }

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.LINKMAN_TEXT_NAME.equals(info.getChild(i).getText())) {
                                    //联系人页面，点击账号及设置按钮
                                    if(!isSetBtn(rootNodeInfo)){
                                        clickAccountAndSet(rootNodeInfo, myAccessibilityService);
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if (info == null) {
                        info = rootNodeInfo;
                    }

                    if(!"android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.activity.SplashActivity".equals(viewName)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }else{
                            //账户及设置页面，点击设置
                            clickSetBtn(rootNodeInfo, myAccessibilityService);
                        }
                    }

                    if(!"android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.activity.QQSettingSettingActivity".equals(viewName)){
                        //设置页面，点击账号管理
                        clickAccManangeBtn(rootNodeInfo, myAccessibilityService);
                    }

                    if("com.tencent.mobileqq.activity.LoginActivity".equals(viewName) && "android.widget.ListView".equals(className)){
                        //账号列表页面，删除账号
                        info = rootNodeInfo;
                        String account = getSelectAccount(info);
                        if(account != null){
                            boolean isClick = removeAccount(info, myAccessibilityService, account);
                            if(isClick){
                                isRemove = false;
                            }
                        }

                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mobileqq") > -1) {
                        viewName = className;
                    }

                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }else if("com.tencent.mobileqq.activity.QQSettingSettingActivity".equals(className)){
                        //设置页面，点击账号管理
                        clickAccManangeBtn(rootNodeInfo, myAccessibilityService);
                    }else if("com.tencent.mobileqq.activity.AccountManageActivity".equals(className)){
                        //账号管理页面，切换账号
                        accountIndex++;
                        boolean isChange = changeAccount(info, myAccessibilityService);
                    }else if("com.tencent.mobileqq.activity.LoginActivity".equals(className)) {
                        //登录页面
                        //是否要删除该账号
                        if(isRemove){
                            //点击列表按钮
                            clickAccountListBtn(info, myAccessibilityService);
                            return ;
                        }
                        //判断是否为登录失败弹窗
                        boolean isWrong = isPwdWrong(rootNodeInfo, myAccessibilityService);
                        if(isWrong){
                            isChange = true;
                            try{
                                //向服务器发送修改QQ账号备注消息
                                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ账号备注",
                                        DataUtil.setQQTargetRemarkInfo(accountList.get(accountIndex), "密码错误"));
                                MainActivity.myClient.send(msg);
                            }catch (Exception e){
                                Log.i(TAG, "向服务器回调失败", e);
                            }
                            return ;
                        }
                        if(isChange){
                            //是，后退切换下一个账号
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }else{
                            //不是，输入密码登录
                            boolean isLogin = login(info, myAccessibilityService);
                            if(!isLogin){
                                //点击登录失败，切换账号
                                Thread.sleep(1000);
                                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }
                        }
                        isChange = false;
                    }else if("com.tencent.mobileqq.activity.NotificationActivity".equals(className)) {
                        //qq账号冻结提示页面，点击取消，删除账号
                        isRemove = true;
                        clickCancelBtn(rootNodeInfo, myAccessibilityService);
                        try{
                            //向服务器发送修改QQ账号状态消息
                            String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ账号状态",
                                    DataUtil.setQQTargetStateInfo(accountList.get(accountIndex), 0));
                            MainActivity.myClient.send(msg);
                        }catch (Exception e){
                            Log.i(TAG, "向服务器回调失败", e);
                        }
                    }else if("com.tencent.mobileqq.activity.QQBrowserActivity".equals(className)) {
                        //QQ登录验证页面，后退切换账号
                        isChange = true;
                        boolean isClick = clickBackBtn(info, myAccessibilityService);
                        if (!isClick) {
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }
                        try{
                            //向服务器发送修改QQ账号备注消息
                            String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改QQ账号备注",
                                    DataUtil.setQQTargetRemarkInfo(accountList.get(accountIndex), "登录需要手动验证"));
                            MainActivity.myClient.send(msg);
                        }catch (Exception e){
                            Log.i(TAG, "向服务器回调失败", e);
                        }
                    }else if("android.app.Dialog".equals(className)){
                        //删除账号弹窗，点击删除按钮
                        isChange = true;
                        clickRemoveBtn(info, myAccessibilityService);
                    }else if(className.indexOf("com.tencent.mobileqq") > -1){
                        //点击到其他页面，后退
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }

                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "QQ登录检查失败", e);
        }
    }


    /**
     * 点击账户及设置按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickAccountAndSet(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击账号及设置按钮");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> accountBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/conversation_head");
            Log.i(TAG, "账户及设置按钮数量：" + accountBtnCounts.size());
            if(accountBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accountBtnCounts.get(0).getParent());
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击账户及设置按钮失败", e);
        }
        return false;
    }

    /**
     * 判断是否有打卡按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean isSetBtn(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否有打卡按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> setBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("打卡");
            Log.i(TAG, "打卡按钮数量：" + setBtnCounts.size());
            if(setBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(setBtnCounts.get(0).getParent());
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否有打卡按钮失败", e);
        }
        return false;
    }

    /**
     * 点击设置按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickSetBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击设置按钮");
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> setBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/settings");
            Log.i(TAG, "设置按钮数量：" + setBtnCounts.size());
            if(setBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(setBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击设置按钮失败", e);
        }
        return false;
    }

    /**
     * 点击账号管理按钮
     * @param rootNodeInfo
     * @return
     */
    public static boolean clickAccManangeBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击账号管理按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> manageBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("帐号管理");
            Log.i(TAG, "账号管理按钮数量：" + manageBtnCounts.size());
            if(manageBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(manageBtnCounts.get(0).getParent());
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击账号管理按钮失败", e);
        }
        return false;
    }

    /**
     * 切换账号
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean changeAccount(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "切换账号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> linerCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/accountLinearlayout");
            Log.i(TAG, "线性布局组件数量：" + linerCounts.size());
            if(linerCounts.size() > 0){
                if(accountNum == 0){
                    linerCounts.get(0).findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/account");
                    for(int i = 0;i < linerCounts.get(0).getChildCount();i++){
                        List<AccessibilityNodeInfo> accountCounts = linerCounts.get(0).getChild(i).findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/account");
                        if(accountCounts.size() > 0){
                            if(accountCounts.get(0).getText() != null){
                                accountList.add(accountCounts.get(0).getText().toString());
                            }
                        }
                    }
                    accountNum = accountList.size();
                }

                Log.i(TAG, "账号数量：" + accountNum);

                //判断是否为最后一个
                if(accountIndex >= accountNum){
                    //后退
                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    return true;
                }

                //判断当前账号是否已经切换
                List<AccessibilityNodeInfo> accCounts = new ArrayList<AccessibilityNodeInfo>();

                for(int i = 0;i < linerCounts.get(0).getChildCount();i++){
                    List<AccessibilityNodeInfo> accountCounts = linerCounts.get(0).getChild(i).findAccessibilityNodeInfosByText(accountList.get(accountIndex));
                    if(accountCounts.size() > 0){
                        accCounts = new ArrayList<AccessibilityNodeInfo>(accountCounts);
                        break;
                    }
                }
                Log.i(TAG, accountList.get(accountIndex) + "账号数量：" + accCounts.size());
                if(accCounts.size() > 0){
                    AccessibilityWechatUtil.clickCoordinate(accCounts.get(0).getParent());
                    List<AccessibilityNodeInfo> checkCounts = accCounts.get(0).getParent().findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/check");
                    Log.i(TAG, "check组件数量：" + checkCounts.size());
                    if(checkCounts != null && checkCounts.size() > 0){
                        //被选中，不切换,后退
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        isLogin = true;
                    }else{
                        //切换账号
                        GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accCounts.get(0));
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                        isLogin = false;
                    }

                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "切换账号失败", e);
        }
        return true;
    }

    /**
     * 登录
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean login(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "登录");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());
            if(editCounts.size() > 1){
                String account = editCounts.get(0).getText() + "";
                String pwd = editCounts.get(1).getText() + "";
                if("输入密码".equals(pwd)){
                    //在账号集合中查询
                    String password = accountMap.getString(account);
                    if(password == null){
                        //没有找到该账号密码，切换下一个账号
                        return false;
                    }
                    //输入密码
                    Bundle arguments  = new Bundle();
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, password);
                    editCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    editCounts.get(1).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    Thread.sleep(1000);
                }
                //点击登录按钮
                List<AccessibilityNodeInfo> loginBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/login");
                Log.i(TAG, "登录按钮数量：" + loginBtnCounts.size());
                if(loginBtnCounts.size() > 0){
                    if(loginBtnCounts.get(0).isClickable()){
                        GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(loginBtnCounts.get(0));
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gesture);
                        return true;
                    }
                }
            }
        }catch (Exception e){
            Log.e(TAG, "登录失败", e);
        }
        return false;
    }

    /**
     * 判断是否为密码错误弹窗
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean isPwdWrong(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "判断是否为密码错误弹窗");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> loginFailCounts = rootNodeInfo.findAccessibilityNodeInfosByText("登录失败");
            Log.i(TAG, "登录失败数量：" + loginFailCounts.size());
            if(loginFailCounts.size() > 0){
                //点击确定按钮
                List<AccessibilityNodeInfo> confirmBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CONFIRM_BTN_NAME);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(confirmBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否为密码错误弹窗失败", e);
        }
        return false;
    }

    /**
     * 点击账号列表按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickAccountListBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击账号列表按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> accountListBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("帐号列表");
            Log.i(TAG, "账号列表按钮数量:" + accountListBtnCounts.size());
            if(accountListBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accountListBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
            }
        }catch (Exception e){
            Log.e(TAG, "点击账号列表按钮失败", e);
        }
        return false;
    }

    /**
     * 删除账号
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @param account
     * @return
     */
    public static boolean removeAccount(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService, String account){
        try{
            Log.i(TAG, "删除账号");
            Thread.sleep(1000);
            List<AccessibilityWindowInfo> list = myAccessibilityService.getWindows();
            if(list.size() >= 3){
                rootNodeInfo = list.get(1).getRoot();
                List<AccessibilityNodeInfo> accountCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(accountCounts, rootNodeInfo,  "android.widget.LinearLayout");
                Log.i(TAG, "账号数量：" + accountCounts.size());
                if(accountCounts.size() > 0){
                    //删除被冻结的QQ号
                    for(AccessibilityNodeInfo nodeInfo : accountCounts){
                        if(nodeInfo.getChild(0).getText() != null && account.equals(nodeInfo.getChild(0).getText())){
                            AccessibilityWechatUtil.recycleCheck(nodeInfo);
                            Rect rect = new Rect();
                            nodeInfo.getChild(1).getBoundsInScreen(rect);
                            if(rect.height() > 10){
                                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(nodeInfo.getChild(1));
                                MyAccessibilityService.flag = 1;
                                myAccessibilityService.opScreen(gesture);
                                return true;
                            }
                        }
                    }
                    //没有找到账号，向下滑动
                    Rect rect = new Rect();
                    rootNodeInfo.getBoundsInScreen(rect);
                    int x = rect.left + rect.width() / 2;
                    int y = rect.bottom - 50;
                    GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(x, y, x, y - 100);
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "删除账号失败", e);
        }
        return false;
    }

    /**
     * 点击删除按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickRemoveBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击删除按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> removeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.REMOVE_BTN_NAME);
            Log.i(TAG, "删除按钮数量：" + removeBtnCounts.size());
            if(removeBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(removeBtnCounts.get(removeBtnCounts.size() - 1));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击删除按钮");
        }
        return false;
    }

    /**
     * 获取选中账号
     * @param rootNodeInfo
     * @return
     */
    public static String getSelectAccount(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "获取选中账号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());
            if(editCounts.size() > 0){
                return editCounts.get(0).getText() + "";
            }
        }catch (Exception e){
            Log.e(TAG, "获取选中账号失败", e);
        }
        return null;
    }

    /**
     * 点击返回按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickBackBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击后退按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> backBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/ivTitleBtnLeft");
            Log.i(TAG, "返回按钮数量：" + backBtnCounts.size());
            if(backBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(backBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击后退按钮失败", e);
        }
        return false;
    }

    /**
     * 点击取消按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickCancelBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击取消按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> cancelBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CANCEL_BTN_NAME);
            Log.i(TAG, "取消按钮数量：" + cancelBtnCounts.size());
            if(cancelBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(cancelBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击取消按钮失败", e);
        }
        return false;
    }

    /**
     * 是否结束任务
     */
    public static boolean isFinish(){
        try{
            if(accountList.size() > 0 && accountList.size() <= accountIndex){
                //结束
                Log.i(TAG, "QQ登录检查结束");
                viewName = "";
                accountIndex = 0;
                accountList = new ArrayList<String>();
                accountNum = 0;
                isSuccess = true;
                isLogin = false;
                isCheckFinish = true;
                accountMap = new JSONObject();
                MyAccessibilityService.isRun = 0;
                MyAccessibilityService.taskType = "";
                MyAccessibilityService.className = "";
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "QQ登录检查任务结束";
                MainActivity.mHandler.sendMessage(message);
                Log.i(TAG, "返回我的APP");
                Thread.sleep(1000);
                MainActivity.openMyApp();
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "QQ切换账号参数重置失败", e);
        }
        return false;
    }

}
