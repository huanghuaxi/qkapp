package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * QQ拉人进群操作
 */
public class QQPullFriendToGroup implements TaskIntfService {

    private static final String TAG = QQPullFriendToGroup.class.getName();

    private static String viewName = "";
    //群名
    public static List<String> groupList = null;
    //成员
    public static List<String> memberList = null;
    //每个群成员数量
    public static int memNum = 0;
    //群索引
    public static int index = 0;
    //上个群索引
    public static int nextIndex = 0;
    //成员索引
    public static int memIndex = 0;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            //是否结束
            if(isFinish(rootNodeInfo, myAccessibilityService)){
                return ;
            }

            Log.d(TAG, "获取QQ群聊列表");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.LINKMAN_TEXT_NAME.equals(info.getChild(i).getText())) {
                                    //联系人页面，点击搜索框
                                    boolean isClick = clickSearchBox(rootNodeInfo, myAccessibilityService);
                                    if(!isClick){
                                        //没有搜索框再次点击联系人返回顶部
                                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                                        if(gesture != null){
                                            MyAccessibilityService.flag = 1;
                                            myAccessibilityService.opScreen(gesture);
                                        }
                                    }
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if(!"android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.activity.SplashActivity".equals(viewName)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }
                    if("android.widget.FrameLayout".equals(className) && "com.tencent.mobileqq.search.activity.UniteSearchActivity".equals(viewName)){
                        //搜索好友页面，判断是输入账号还是查看是否添加成功
                        info = rootNodeInfo;
                        int checkState = isCheck(info);
                        if(checkState == 1){
                            //输入群名
                            String name = groupList.get(index);
                            inputGroupName(info, name);
                        }else if(checkState == 2){
                            //查看是否有该群，有则点击
                            boolean isClick = clickGroupList(info, myAccessibilityService);
                            if(!isClick){
                                //点击失败下一个群
                                index++;
                            }
                        }
                    }

                    if("com.tencent.mobileqq.activity.selectmember.SelectMemberActivity".equals(viewName)){
                        //群聊邀请页面，判断是否输入好友账号
                        info = rootNodeInfo;
                        if(!isInputQQ(info)){
                            //已经输入QQ号，点击好友
                            boolean isClick = clickFriendList(info, myAccessibilityService);
                            if(!isClick){
                                //没有搜索到可添加好友，清空搜索框
                                inputQQ(info, "");
                            }
                            memIndex++;
                            Log.i(TAG, "好友索引：" + memIndex);
                            nextIndex = index;
                        }else{
                            //输入QQ号
                            inputQQ(info, memberList.get(memIndex));
                        }
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mobileqq") > -1){
                        viewName = className;
                    }
                    if("com.tencent.mobileqq.activity.SplashActivity".equals(className)){
                        //主页，点击联系人
                        GestureDescription gesture = AccessibilityWechatUtil.clickQQTab(rootNodeInfo, 2);
                        if(gesture != null){
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }

                    if("com.tencent.mobileqq.activity.ChatActivity".equals(className)){
                        //群聊房间页面，点击群资料卡
                        clickGroupInfoBtn(info, myAccessibilityService);
                    }

                    if("com.tencent.mobileqq.activity.ChatSettingForTroop".equals(className)){
                        //群资料页面，点击邀请按钮
                        boolean isClick = clickInviteBtn(info, myAccessibilityService);
                        if(!isClick){
                            //如果邀请按钮不存在则是点击到好友资料页，返回并搜索下一个群
                            index++;
                            //返回
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            Thread.sleep(1000);
                        }
                    }

                    if("com.tencent.mobileqq.activity.selectmember.SelectMemberActivity".equals(className)){
                        //群聊邀请页面，判断是否输入好友账号
                        info = rootNodeInfo;
                        if(!isInputQQ(info)){
                            //已经输入QQ号，点击好友
                            boolean isClick = clickFriendList(info, myAccessibilityService);
                            if(!isClick){
                                //没有搜索到可添加好友，清空搜索框
                                inputQQ(info, "");
                            }
                            memIndex++;
                            Log.i(TAG, "好友索引：" + memIndex);
                            nextIndex = index;
                        }else{
                            //输入QQ号
                            inputQQ(info, memberList.get(memIndex));
                        }
                    }


                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "QQ拉人进群操作失败", e);
        }
    }


    /**
     * 点击搜索框
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSearchBox(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击搜索框");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = rootNodeInfo.findAccessibilityNodeInfosByText("搜索");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 1){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击搜索框失败", e);
        }
        return false;
    }

    /**
     * 判断是否输入群名
     * @param rootNodeInfo
     * @return
     */
    public static int isCheck(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否输入群名");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/et_search_keyword");
            Log.i(TAG, "搜索框数量：" +  searchCounts.size());
            if(searchCounts.size() > 0){
                String text = searchCounts.get(0).getText() == null ? "" : searchCounts.get(0).getText().toString();
                //如果相等则为查看好友是否存在，不相等为输入好友账号
                String name = groupList.get(index);
                if(text.equals(name)){
                    return 2;
                }else{
                    return 1;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否输入群名失败", e);
        }
        return 0;
    }

    /**
     * 输入群名
     * @param rootNodeInfo
     * @param name
     * @return
     */
    public static boolean inputGroupName(AccessibilityNodeInfo rootNodeInfo, String name){
        try{
            Log.i(TAG, "输入群名");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/et_search_keyword");
            Log.i(TAG, "搜索框数量：" +  searchCounts.size());
            if(searchCounts.size() > 0){
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, name);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(1000);
            }
        }catch (Exception e){
            Log.e(TAG, "输入群名失败", e);
        }
        return false;
    }

    /**
     * 点击群列表
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickGroupList(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击群列表");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> groupCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.GROUP_CHAT_BTN_NAME);
            if(groupCounts.size() == 0){
                groupCounts = rootNodeInfo.findAccessibilityNodeInfosByText("最近常用");
            }
            Log.i(TAG, "群聊数量：" + groupCounts.size());
            if(groupCounts.size() > 0){
                Rect rect = new Rect();
                groupCounts.get(0).getBoundsInScreen(rect);
                Point position = new Point(rect.left + (int)(rect.width() / 2), rect.bottom + 100);
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));GestureDescription gesture = builder.build();
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击群列表失败", e);
        }
        return false;
    }

    /**
     * 点击群资料卡按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickGroupInfoBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击群资料卡按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> infoBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/ivTitleBtnRightImage");
            Log.i(TAG, "群资料卡按钮数量：" + infoBtnCounts.size());
            if(infoBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(infoBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击群资料卡按钮失败", e);
        }
        return false;
    }

    /**
     * 点击邀请按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickInviteBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击邀请按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> inviteBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("邀请");
            Log.i(TAG, "邀请按钮数量：" + inviteBtnCounts.size());
            if(inviteBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(inviteBtnCounts.get(0).getParent());
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击邀请按钮失败", e);
        }
        return false;
    }

    /**
     * 判断输入QQ号
     * @param rootNodeInfo
     * @return
     */
    public static boolean isInputQQ(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否输入QQ号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + editCounts.size());
            if(editCounts.size() > 0){
                Log.i(TAG, "text:" + editCounts.get(0).getText());
                if(editCounts.get(0).getText() == null || "".equals(editCounts.get(0).getText().toString()) || "搜索".equals(editCounts.get(0).getText().toString())){
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否输入QQ号失败", e);
        }
        return false;
    }

    /**
     * 输入QQ号
     * @param rootNodeInfo
     * @param qq
     * @return
     */
    public static boolean inputQQ(AccessibilityNodeInfo rootNodeInfo, String qq){
        try{
            Log.i(TAG, "输入QQ号");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + editCounts.size());
            if(editCounts.size() > 0){
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, qq);
                editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(2000);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "输入QQ号失败", e);
        }
        return false;
    }

    /**
     * 点击好友列表
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickFriendList(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击好友列表");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> listCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/image");
            Log.i(TAG, "列表数量：" + listCounts.size());
            if(listCounts.size() > 0){
                List<AccessibilityNodeInfo> isAddCounts = listCounts.get(0).getParent().findAccessibilityNodeInfosByText("已加入");
                Log.i(TAG, "已加入文本数量：" + isAddCounts.size());
                if(isAddCounts.size() == 0){
                    //未加入点击列表
                    GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(listCounts.get(0).getParent());
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击好友列表失败", e);
        }
        return false;
    }

    /**
     * 点击立即邀请按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickInvitationBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击立即邀请按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> invitaBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("立即邀请");
            Log.i(TAG, "立即邀请按钮数量：" + invitaBtnCounts.size());
            if(invitaBtnCounts.size() > 0){
                //判断按钮是否禁用
                if(invitaBtnCounts.get(0).isEnabled()){
                    GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(invitaBtnCounts.get(0));
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                    Thread.sleep(3000);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击立即邀请按钮失败", e);
        }
        return false;
    }

    /**
     * 是否结束
     */
    public static boolean isFinish(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            boolean type = false;
            if(memIndex != 0 && memNum != 0 && memIndex % memNum == 0 && index == nextIndex){
                //达到好友添加上限，点击立即邀请，换下一个群
                index++;
                boolean isClick = clickInvitationBtn(rootNodeInfo, myAccessibilityService);
                if(!isClick){
                    //点击失败，后退
                    Thread.sleep(1000);
                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                }
                MyAccessibilityService.isRun = 1;
                viewName = "com.tencent.mobileqq.activity.SplashActivity";
                type = true;
            }

            if(memberList == null || groupList == null || groupList.size() <= index || memberList.size() <= memIndex || memNum == 0){
                //所有群都加完好友，结束
                if(memIndex % memNum != 0){
                    //点击立即邀请
                    clickInvitationBtn(rootNodeInfo, myAccessibilityService);
                    Thread.sleep(2000);
                }
                memberList = null;
                groupList = null;
                index = 0;
                memIndex = 0;
                viewName = "";
                QQFriendList.friendList = new ArrayList<Map<String, String>>();
                QQGroupList.groupList = new ArrayList<String>();
                if(QQChangeAccount.accountNum == -1){
                    //QQ账号已全部切换完成，拉人进群任务结束
                    MyAccessibilityService.resetParam();
                    Message message = MainActivity.mHandler.obtainMessage();
                    message.what = 100;
                    message.obj = "拉人进群操作结束";
                    MainActivity.mHandler.sendMessage(message);

                }else{
                    //切换账号重新开始拉人进群
                    MyAccessibilityService.isRun = 0;
                    MyAccessibilityService.taskType = "";
                    MyAccessibilityService.className = "";
                }
                Log.i(TAG, "后退到主页面");
                Thread.sleep(1000);
                MainActivity.openMyApp();
                type = true;
            }
            return type;
        }catch (Exception e){
            Log.e(TAG, "判断是否任务结束失败", e);
        }
        return false;
    }


}
