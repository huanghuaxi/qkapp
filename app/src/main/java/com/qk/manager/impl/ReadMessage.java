package com.qk.manager.impl;

import android.accessibilityservice.GestureDescription;
import android.graphics.Rect;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

/**
 *  阅读未读消息
 */
public class ReadMessage implements TaskIntfService {

    private static final String TAG = ReadMessage.class.getName();

    //是否为聊天界面
    private static boolean isChat = false;

    //当前页面
    private static String viewName = "";

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            AccessibilityNodeInfo info = event.getSource();
            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面被点击");
                    if(info != null){
                        if(info.getChildCount() > 0){
                            for (int i = 0;i < info.getChildCount();i++){
                                if(Constants.WECHAT_BTN_NAME.equals(info.getChild(i).getText())){
                                    //首页微信标签被点击，阅读消息
                                    boolean isClick = readMsg(rootNodeInfo, event, myAccessibilityService);
                                    boolean isFinish = isFinish(rootNodeInfo, event);
                                    if(isFinish){
                                        finish();
                                        return ;
                                    }else{
                                        //重新双击微信
                                        clickWechatBtn(rootNodeInfo, event, myAccessibilityService);
                                    }
                                    /*if(MyAccessibilityService.isTopFlag == 0){
                                        Log.d(TAG, "下拉页面操作");
                                        GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(500, 400, 500, 700);
                                        MyAccessibilityService.flag = 1;
                                        myAccessibilityService.opScreen(gesture);
                                        MyAccessibilityService.isTopFlag = 1;
                                    }*/
                                }
                            }

                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    Log.d(TAG, "viewName:" + viewName);
                    //阅读消息
                    if (info == null) {
                        info = rootNodeInfo;
                    }

                    //判断是否有后退按钮，有的话点击
                    GestureDescription gestureBack = isBackBtn(info, event, myAccessibilityService);
                    if(gestureBack != null){
                        MyAccessibilityService.flag = 1;
                        myAccessibilityService.opScreen(gestureBack);
                    }else{
                        //双击微信锁定未读消息
                        GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 1);
                        if(gesture != null){
                            boolean isClick = clickWechatBtn(rootNodeInfo, event, myAccessibilityService);
                            if(!isClick){
                                finish();
                                return ;
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mm") > -1 ){
                        viewName = className;
                    }
                    if (info == null) {
                        info = rootNodeInfo;
                    }
                    //阅读消息
                    wechatReadMessages(info, event, className, myAccessibilityService);
                    break;
            }
        }catch (Exception e){
            Log.e(TAG, "阅读未读消息失败", e);
        }
    }

    /**
     * 微信阅读消息
     * @param info
     * @param event
     * @param className
     */
    public void wechatReadMessages(AccessibilityNodeInfo info, AccessibilityEvent event, String className, MyAccessibilityService myAccessibilityService){
        try{
            //阅读消息
            if ("com.tencent.mm.ui.LauncherUI".equals(className)) {
                //点击微信标签按钮
                Log.d(TAG, "点击微信标签按钮");
                boolean isClick = clickWechatBtn(info, event, myAccessibilityService);
                if (!isClick) {
                    finish();
                    return ;
                }
            }else if("com.tencent.mm.ui.base.i".equals(className)){
                clickNoBtn(info, event);
            }else{
                //判断是否有后退按钮，有的话点击
                GestureDescription gestureBack = isBackBtn(info, event, myAccessibilityService);
                if(gestureBack != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gestureBack);
                }
            }



            /*else if(("android.widget.ListView".equals(className) || "android.widget.FrameLayout".equals(className))
                    && "com.tencent.mm.ui.LauncherUI".equals(viewName)){
                //聊天界面
                if(MyAccessibilityService.isTopFlag == 1){
                    boolean isTop = isMainTop(info, event);
                    if(isTop){
                        Log.i(TAG, "已拉到顶部");
                        MyAccessibilityService.isTopFlag = 2;
                    }else {
                        if(MyAccessibilityService.flag == 0){
                            Log.d(TAG, "下拉页面操作");
                            GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(500, 400, 500, 700);
                            MyAccessibilityService.flag = 1;
                            myAccessibilityService.opScreen(gesture);
                        }
                    }
                }else{
                    isBottom(info, event);
                    *//*Log.d(TAG, "阅读消息操作");
                    MyAccessibilityService.isBottomFlag = 2;
                    MyAccessibilityService.flag = 1;
                    int isSlither = readMessages(info, event);
                    Log.i(TAG, "isSlither:" + isSlither);
                    if(isSlither == 0){
                        GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(500, 600, 500, 700);
                        myAccessibilityService.opScreen(gesture);
                        MyAccessibilityService.isBottomFlag = 1;
                    }else if(isSlither == 2){
                        GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(500, 600, 500, 500);
                        myAccessibilityService.opScreen(gesture);
                        MyAccessibilityService.isBottomFlag = 1;
                    }*//*
                }

            }*/
        }catch (Exception e){
            Log.e(TAG, "阅读消息失败");
            e.printStackTrace();
        }
    }

    /**
     * 跳转到微信页面
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickWechatBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "双击微信标签按钮");
            Thread.sleep(1000);
            GestureDescription gesture = AccessibilityWechatUtil.doubleClickBottomMenuBtn(rootNodeInfo, 1);
            if(gesture != null){
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                /*Thread.sleep(500);
                myAccessibilityService.opScreen(gesture);*/
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击微信标签按钮失败", e);
        }
        return false;
    }

    /**
     * 判断是否拉到顶部
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isMainTop(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> listViewCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(listViewCounts, rootNodeInfo, "android.widget.ListView");
            Log.i(TAG, "列表组件数量：" + listViewCounts.size());
            if(listViewCounts.size() > 0){
                List<AccessibilityNodeInfo> textViewCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(textViewCounts, listViewCounts.get(0), "android.widget.TextView");
                Log.i(TAG, "未读消息数量：" + textViewCounts.size());
                if(textViewCounts.size() > 0){
                    String name = "最近使用";
                    String name1 = new String(name.getBytes("ISO-8859-1"), "UTF-8");
                    for(AccessibilityNodeInfo info : textViewCounts){
                        if(info.getText() != null){
                            String text = info.getText().toString();
                            text = new String(text.getBytes("ISO-8859-1"), "UTF-8");
                            if(text.indexOf(name) > -1 || text.indexOf(name1) > -1 ){
                                return true;
                            }
                        }
                    }
                }
            }
        }catch(Exception e){
            Log.e(TAG, "判断是否拉取到顶部失败");
            e.printStackTrace();
        }
        return false;
    }
    /**
     * 阅读消息
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static int readMessages(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            //获取返回按钮
            List<AccessibilityNodeInfo> backBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.BACK_BTN_NAME);
            Log.i(TAG, "返回按钮数量：" + backBtnCounts.size());
            if(backBtnCounts.size() == 0){
                List<AccessibilityNodeInfo> listViewCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(listViewCounts, rootNodeInfo, "android.widget.ListView");
                Log.i(TAG, "列表组件数量：" + listViewCounts.size());
                if(listViewCounts.size() > 0){
                    List<AccessibilityNodeInfo> textViewCounts = new ArrayList<AccessibilityNodeInfo>();
                    AccessibilityWechatUtil.recycleFindView(textViewCounts, listViewCounts.get(0), "android.widget.TextView");
                    Log.i(TAG, "未读消息数量：" + textViewCounts.size());
                    if(textViewCounts.size() > 0){
                        //判断第一条消息序号
                        int index = 0;
                        String pattern = "\\d+\\+*";
                        for(AccessibilityNodeInfo info : textViewCounts){
                            if(info.getText() != null){
                                if(Pattern.matches(pattern, info.getText().toString())){
                                    AccessibilityWechatUtil.clickCoordinate(textViewCounts.get(index).getParent());
                                    textViewCounts.get(index).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                    return 1;
                                }
                            }
                            index++;
                        }
                        return 2;

                    }else{
                        return 2;
                    }
                }
            }else{
                Thread.sleep(1000);
                backBtnCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
            }
        }catch (Exception e){
            Log.e(TAG, "阅读消息失败");
            e.printStackTrace();
        }
        return 1;
    }

    public static boolean readMsg(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> listViewCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(listViewCounts, rootNodeInfo, "android.widget.ListView");
            Log.i(TAG, "列表组件数量：" + listViewCounts.size());
            if(listViewCounts.size() > 0){
                List<AccessibilityNodeInfo> textViewCounts = new ArrayList<AccessibilityNodeInfo>();
                AccessibilityWechatUtil.recycleFindView(textViewCounts, listViewCounts.get(0), "android.widget.TextView");
                Log.i(TAG, "未读消息数量：" + textViewCounts.size());
                if(textViewCounts.size() > 0){
                    //判断第一条消息序号
                    int index = 0;
                    String pattern = "\\d+\\+*";
                    for(AccessibilityNodeInfo info : textViewCounts){
                        if(info.getText() == null || Pattern.matches(pattern, info.getText().toString())){
                            //判断列表是否在可点击范围
                            Rect rect = new Rect();
                            textViewCounts.get(index).getParent().getBoundsInScreen(rect);
                            if(rect.height() <= 0 || rect.width() <= 0) {
                                return false;
                            }
                            //点击列表
                            GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(textViewCounts.get(index).getParent());
                            MyAccessibilityService.flag = 1;
                            isChat = true;
                            myAccessibilityService.opScreen(gesture);
//                            MyAccessibilityService.isRun =  1;
//                            textViewCounts.get(index).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                /*Thread.sleep(2000);
                                GestureDescription gesture = myAccessibilityService.clickBackBtn(event, info);
                                if(gesture != null){
                                    myAccessibilityService.flag = 1;
                                    myAccessibilityService.opScreen(gesture);
                                }else{
                                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                }*/
                            return true;
                        }
                        index++;
                    }
                }
            }
        }catch (Exception e){
            Log.i(TAG, "阅读消息失败", e);
        }
        return false;
    }

    public static boolean isBottom(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "-----------查看是否到底部----------");
            List<AccessibilityNodeInfo> listViewCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(listViewCounts, rootNodeInfo, "android.widget.ListView");
            Log.i(TAG, "ListView数量：" + listViewCounts.size());
            if(listViewCounts.size() > 0){
                List<AccessibilityNodeInfo> viewCounts = new ArrayList<AccessibilityNodeInfo>();
//                AccessibilityWechatUtil.recycleCheck(rootNodeInfo);
                Log.i(TAG,"------------------------------");
                AccessibilityWechatUtil.recycleFindView(viewCounts, rootNodeInfo, "android.view.View");
                List<AccessibilityNodeInfo> linearCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mm:id/b6e");
                if(viewCounts != null && viewCounts.size() > 0){
                    for(AccessibilityNodeInfo info : viewCounts){
                        Log.i(TAG, "text:" + info.getText());
                    }
                }
                if(linearCounts != null){
                    for(AccessibilityNodeInfo info : linearCounts){
                        Log.i(TAG, "======================");
                        AccessibilityWechatUtil.recycleCheck(info);
                    }
                }
                /*AccessibilityWechatUtil.recycleFindView(viewCounts, listViewCounts.get(3), "android.view.View");
                Log.i(TAG, "view数量：" + viewCounts.size());
                if(viewCounts.size() > 0) {
                    for (int i = 0; i < viewCounts.size(); i++) {
                        Log.i(TAG, "text:" + viewCounts.get(i).getText());
                    }
                }*/
            }
            /*List<AccessibilityNodeInfo> textCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("b6c");
            Log.i(TAG, "text数量：" + textCounts.size());
            if(textCounts != null){
                for(int i = 0;i < textCounts.size();i++){
                    Log.i(TAG, "text:" + textCounts.get(i).getText());
                }
            }*/
            /*List<AccessibilityNodeInfo> viewCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(viewCounts, rootNodeInfo, "android.view.View");
            Log.i(TAG, "view数量：" + viewCounts.size());
            for(AccessibilityNodeInfo info : viewCounts){
                Log.i(TAG, "text:" + info.getText());
            }*/
            /*if(listViewCounts.size() > 0){
                AccessibilityWechatUtil.clickCoordinate(listViewCounts.get(0));
                List<AccessibilityNodeInfo> linearCounts = new ArrayList<AccessibilityNodeInfo>();
                Log.i(TAG, "ListView子级数量：" + listViewCounts.get(0).getChildCount());
//                AccessibilityWechatUtil.recycleCheck(listViewCounts.get(0));
                for(int i= 0;i < listViewCounts.get(0).getChildCount();i++){
                    linearCounts.add(listViewCounts.get(0).getChild(i));
                }
                *//*if(linearCounts.size() > 0){
                    for(AccessibilityNodeInfo info : linearCounts){
                        AccessibilityWechatUtil.clickCoordinate(info);
                        AccessibilityWechatUtil.recycleCheck(info);
                        Log.i(TAG, "====================");
                        List<AccessibilityNodeInfo> viewCounts = new ArrayList<AccessibilityNodeInfo>();
                        AccessibilityWechatUtil.recycleFindView(viewCounts, info, "android.view.View");
                        if(viewCounts.size() > 0){
                            Log.i(TAG, "text:" + viewCounts.get(0).getText());
                        }
                    }
                }*//*
            }*/
            /*for(AccessibilityNodeInfo info : listViewCounts){
                Log.i(TAG, "id:" + info.getViewIdResourceName());
            }*/
        }catch (Exception e){
            Log.e(TAG, "判断是否为底部失败", e);
        }
        return false;
    }

    /**
     * 判断是否有后退按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static GestureDescription isBackBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "判断是否有后退按钮");
            Thread.sleep(1000);
            return myAccessibilityService.clickBackBtn(event, rootNodeInfo);
        }catch (Exception e){
            Log.e(TAG, "判断是否有后退按钮失败", e);
        }
        return null;
    }

    /**
     * 判断是否结束阅读
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isFinish(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, "com.tencent.mm.ui.mogic.WxViewPager");
            Log.i(TAG, "栏目数量：" + pageCounts.size());
            if(pageCounts.size() > 0){
                AccessibilityNodeInfo relativeCounts = pageCounts.get(0).getParent();
                if(relativeCounts != null){
                    List<AccessibilityNodeInfo> textViewCounts = new ArrayList<AccessibilityNodeInfo>();
                    AccessibilityWechatUtil.recycleFindView(textViewCounts, relativeCounts.getChild(1).getChild(0), "android.widget.TextView");
                    if(textViewCounts.size() == 0){
                        return true;
                    }else{
                        String pattern = "\\d+\\+*";
                        if(textViewCounts.get(0).getText() != null && !Pattern.matches(pattern, textViewCounts.get(0).getText().toString())){
                            return true;
                        }
                    }
//                relativeCounts.get(0).getChild(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
//                    MyAccessibilityService.isTopFlag = 2;
                }
            }
        }catch (Exception e){
            Log.i(TAG, "判读是否阅读结束失败", e);
        }
        return false;
    }

    /**
     * 点击拒绝按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickNoBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> notBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("拒绝");
            Log.i(TAG, "拒绝按钮数量：" + notBtnCounts.size());
            if(notBtnCounts.size() > 0){
                notBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击拒绝按钮失败");
        }
        return false;
    }

    /**
     * 结束任务
     */
    public static void finish(){
        try{
            Log.i(TAG, "阅读消息结束");
            isChat = false;
            viewName = "";
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "阅读消息结束";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "返回我的APP");
            Thread.sleep(2000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "结束阅读消息失败", e);
        }

    }

}
