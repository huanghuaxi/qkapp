package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Rect;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 阅读朋友圈
 *
 * 操作顺序
 * 1.点击发现按钮跳转到发现页面
 * 2.点击朋友圈按钮跳转到朋友圈页面
 * 3.下拉列表，根据概率点赞
 *
 */
public class ReadMoments implements TaskIntfService {

    private static final String TAG = ReadMoments.class.getName();

    //点赞机率
    public static Integer goodRatio = 0;
    //阅读时间
    public static Integer minTime = 0;

    public static Integer maxTime = 0;

    private static String viewName = "";

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName);
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面被点击");
                    /*if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.FIND_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页发现标签被点击，点击朋友圈搜索按钮
                                    info = rootNodeInfo;
                                    clickMomentsBtn(info, event, myAccessibilityService);
                                    break;
                                }
                            }
                        }
                    }*/
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    //阅读消息
                    if (info == null) {
                        info = rootNodeInfo;
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(viewName)){
                        //微信主页，点击朋友圈按钮
                        boolean isFindPage = clickMomentsBtn(info, event, myAccessibilityService);
                        //如果没有找到朋友圈按钮，则点击发现
                        if(!isFindPage){
                            clickFindBtn(info, event, myAccessibilityService);
                        }
                    }

                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if(className.indexOf("com.tencent.mm") > -1 ){
                        viewName = className;
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(className)) {
                        //主页，点击发现按钮
                        boolean isClick = clickFindBtn(info, event, myAccessibilityService);
                        if(isClick){
                            //点击朋友圈按钮
                            Thread.sleep(1000);
                            clickMomentsBtn(info, event, myAccessibilityService);
                        }
                    }else if("com.tencent.mm.plugin.sns.ui.SnsTimeLineUI".equals(className)) {
                        //阅读朋友圈
                        boolean isFinish = readMoments(info, event, myAccessibilityService);
                        if(isFinish){
                            Thread.sleep(1000);
                            //结束任务
                            finish();
                        }
                    }else if("com.tencent.mm.plugin.sns.ui.SnsMsgUI".equals(className)){
                        //好友点赞消息页面，后退
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }else if("com.tencent.mm.plugin.sns.ui.SnsOnlineVideoActivity".equals(className) || "com.tencent.mm.plugin.sns.ui.SnsBrowseUI".equals(className)){
                        //跳转到了内容详情页面，点击返回
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }else {
                        //其他页面，点击后退
                        Thread.sleep(1000);
                        myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                    }

                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "阅读朋友圈失败", e);
        }
    }

    /**
     * 点击发现标签按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickFindBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            Log.i(TAG, "点击发现标签按钮");
            GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 3);
            if(gesture != null){
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击发现标签按钮失败", e);
        }
        return false;
    }

    /**
     * 点击朋友圈按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickMomentsBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try {
            Thread.sleep(2000);
            List<AccessibilityNodeInfo> momentsBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MOMENTS_BTN_NAME);
            Log.i(TAG, "朋友圈按钮数量：" + momentsBtnCounts.size());
            if(momentsBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(momentsBtnCounts.get(0).getParent());
                if(gesture != null){
                    Thread.sleep(1000);
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击朋友圈按钮失败", e);
        }
        return false;
    }

    /**
     * 阅读朋友圈
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean readMoments(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            //判断是否有新消息按钮
            List<AccessibilityNodeInfo> newMsgBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("新消息");
            Log.i(TAG, "新消息按钮数量：" + newMsgBtnCounts.size());
            if(newMsgBtnCounts.size() > 0){
                //点击新消息按钮
                newMsgBtnCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return false;
            }
            //随机阅读时长，开始下拉
            Random random = new Random();
            Integer time = random.nextInt(maxTime - minTime + 1) + minTime;
            Log.i(TAG, "阅读时间：" + time + "秒");
            Date date = new Date();
            while ((new Date().getTime() - date.getTime()) < (time * 1000)){
                //是否点赞
                if(goodRatio != 0 && goodRatio > random.nextInt(100)){
                    //点击评论按钮，弹出点赞选项
                    List<AccessibilityNodeInfo> discussBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.DISCUSS_BTN_NAME);
                    Log.i(TAG, "评论按钮数量：" + discussBtnCounts.size());
                    if(discussBtnCounts.size() > 0){
                        Thread.sleep(1000);
                        //从最后一个评论按钮开始查找
                        Rect rect = new Rect();
                        for(int i = discussBtnCounts.size() - 1;i >= 0;i--){
                            discussBtnCounts.get(i).getBoundsInScreen(rect);
                            //判断按钮高度，如果是负数则无法点击，查询上一个评论按钮
                            if(rect.height() > 0){
                                discussBtnCounts.get(i).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                //点击点赞按钮
                                Thread.sleep(1000);
                                List<AccessibilityNodeInfo> goodBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.GOOD_BTN_NAME);
                                Log.i(TAG, "点赞按钮数量：" + goodBtnCounts.size());
                                if(goodBtnCounts.size() > 0){
                                    goodBtnCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                                }
                                break;
                            }
                        }

                    }
                }

                //向下滑动
                Thread.sleep(1000);
                GestureDescription gesture = AccessibilityWechatUtil.slitherCoordinate(300, 800, 300, 200);
                myAccessibilityService.opScreen(gesture);
                Thread.sleep(3000);
            }
            return true;
        }catch (Exception e){
            Log.e(TAG, "阅读朋友圈操作失败", e);
        }
        return false;
    }

    /**
     * 结束任务
     */
    public static void finish(){
        try{
            goodRatio = 0;
            minTime = 0;
            maxTime = 0;
            viewName = "";
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "阅读朋友圈结束";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "返回我的APP");
            Thread.sleep(1000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.e(TAG, "阅读朋友圈结束失败", e);
        }

    }

}
