package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;
import android.view.accessibility.AccessibilityWindowInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/**
 * 发送消息操作
 *
 * 操作顺序
 * 1.点击微信标签按钮跳转到微信页面
 * 2.点击搜索按钮跳转到搜索页面
 * 3.输入好友账号
 * 4.如果搜索到好友则点击列表第一个好友，没有搜索到好友更换下一个好友
 * 5.进入聊天页面输入消息点击发送按钮
 *
 */
public class SendMessage implements TaskIntfService {

    private static final String TAG = SendMessage.class.getName();
    //好友列表
    public static List<String> friendList;
    //消息列表
    public static List<String> msgList;
    //最小消息数
    public static Integer minMsgNum = 1;
    //最大消息数
    public static Integer maxMsgNum = 3;
    //最小间隔时间
    public static Integer minTime = 0;
    //最大间隔时间
    public static Integer maxTime = 0;

    private static Integer index = 0;

    public static Integer msgNum = 0;

    public static Integer msgIndex = 0;

    private static String viewName = "";
    //是否是在聊天页面
    private static boolean isChat = false;
    //是否点击好友列表
    private static boolean isClick = true;
    //是否进入搜索页面
    private static boolean isSearch = false;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {

        try {
            Log.i(TAG, "isChat:" + isChat);
            Log.i(TAG, "isClick:" + isClick);
            Log.i(TAG, "isSearch:" + isSearch);

            //判断是否结束
            if(isFinish()){
                return ;
            }
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.WECHAT_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页微信标签被点击，点击搜索按钮
                                    info = rootNodeInfo;
                                    clickSearchBtn(info, event, myAccessibilityService);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }

                    if(("com.tencent.mm.ui.chatting.ChattingUI".equals(viewName) || ("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName))
                            || ("com.tencent.mm.ui.LauncherUI".equals(viewName) || "com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(viewName)))
                            && ("android.widget.LinearLayout".equals(className) || "android.widget.FrameLayout".equals(className)
                            || "android.widget.Button".equals(className) || "android.widget.ListView".equals(className))){
                        //聊天页面操作
                        if(!isSearch){
                            //还没有点击搜索按钮
                            return ;
                        }
                        if(isChatPage(info, event)){
                            //聊天页面
                            info = rootNodeInfo;
                            sendMessage(info, event);
                            if(!isChat){
                                //下一个好友
                                index++;
                                isClick = false;
                                //后退
                                GestureDescription gesture = MyAccessibilityService.clickBackBtn(event, info);
                                if(gesture == null){
                                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                }else{
                                    MyAccessibilityService.flag = 1;
                                    myAccessibilityService.opScreen(gesture);
                                }
                            }
                            break;
                        }
                    }

                    if(("android.widget.FrameLayout".equals(className) || "android.widget.ListView".equals(className) || "android.widget.EditText".equals(className))
                            && ("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName) || "com.tencent.mm.ui.LauncherUI".equals(viewName))){
                        //搜索好友页面操作
                        if(!isSearch){
                            //还没有点击搜索按钮
                            return ;
                        }
                        //点击好友列表
                        Thread.sleep(1000);
                        info =  rootNodeInfo;
                        if(info == null){
                            //后退
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }
                        if(!isInputName(info, event)){
                            //搜索好友页面，输入好友账号
                            inputFriendName(info, event, friendList.get(index), myAccessibilityService);
                            isClick = true;
                            isChat = false;
                        }else if(!isChatPage(info, event)){
                            //搜索好友页面，点击好友列表
                            if(!isInputName(info, event)){
                                //输入框中没有账号输入
                                Log.i(TAG, "输入框中没有输入账号");
                                return ;
                            }
                            int flag = clickFriendView(info, event, myAccessibilityService);
                            if(flag == 2){
                                //没有点击到好友列表
                                index++;
                                if(isFinish()) {
                                    return;
                                }
                                Thread.sleep(1000);
                                inputFriendName(info, event, friendList.get(index), myAccessibilityService);
                                isClick = true;
                                isChat = false;
                            }
                        }

                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mm") > -1) {
                        viewName = className;
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(className)){
                        //点击微信
                        clickSearchBtn(info, event, myAccessibilityService);
                        if(!isSearch){
                            clickWechatBtn(info, event, myAccessibilityService);
                        }

                    }
                    if(className.indexOf("com.tencent.mm.ui.widget") > -1){
                        //没有搜索到好友弹窗
                        boolean isConfirm = clickConfirmBtn(info, event);
                        if(isConfirm){
                            index++;
                            if(isFinish()) {
                                return;
                            }
                            Thread.sleep(1000);
                            inputFriendName(info, event, friendList.get(index), myAccessibilityService);
                            isClick = true;
                            isChat = false;
                        }
                    }

                    if("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(className)){
                        //好友查询页面
                        if(index >= 0){
                            //从聊天页面返回时调用
                            inputFriendName(info, event, friendList.get(index), myAccessibilityService);
                        }

                    }

                    if("com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(className)){
                        //好友信息页面，点击发消息按钮
                        boolean isSend = clickSendMsgBtn(info, event, myAccessibilityService);
                        if(!isSend){
                            //没有添加该好友，跳过该好友
                            index++;
                            isClick = false;
                            isClick = true;
                            //后退
                            GestureDescription gesture = MyAccessibilityService.clickBackBtn(event, info);
                            if(gesture == null){
                                Thread.sleep(1000);
                                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }else{
                                MyAccessibilityService.flag = 1;
                                myAccessibilityService.opScreen(gesture);
                            }
                        }
                    }

                    /*if("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName) && "android.widget.LinearLayout".equals(className)){
                        if(isChat){
                            //聊天页面
                            info = rootNodeInfo;
                            sendMessage(info, event);
                            if(!isChat){
                                //下一个好友
                                index++;
                                isClick = false;
                                //后退
                                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }
                        }
                    }*/
                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "发送消息失败", e);
        }
    }

    /**
     * 点击微信标签按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickWechatBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击微信标签按钮");
            GestureDescription gesture1 = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 2);

            GestureDescription gesture2 = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 1);
            if(gesture2 != null){
                Thread.sleep(1000);
                myAccessibilityService.opScreen(gesture2);
                /*Thread.sleep(1000);
                myAccessibilityService.opScreen(gesture2);*/
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击微信标签按钮失败", e);
        }
        return false;
    }

    /**
     * 点击搜索按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSearchBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            if(rootNodeInfo == null){
                return false;
            }
            //获取更多按钮
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> moreBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MORE_BTN_NAME);
            Log.i(TAG, "更多按钮数量：" + moreBtnCounts.size());
            if(moreBtnCounts.size() > 0){
                Rect rect = new Rect();
                moreBtnCounts.get(moreBtnCounts.size() - 1).getBoundsInScreen(rect);
                //计算搜索按钮位置
                Point position = new Point(rect.left - (int)(rect.width() / 2), rect.top + (int)(rect.height() / 2));
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                GestureDescription gesture = builder.build();
                //点击搜索按钮
                Thread.sleep(1000);
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                isSearch = true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击搜索按钮失败", e);
        }
        return false;
    }

    /**
     * 输入好友账号
     * @param rootNodeInfo
     * @param event
     * @param name
     * @return
     */
    public static boolean inputFriendName(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String name, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                Log.i(TAG, "输入好友账号:" + name);
                //点击搜索框
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, name);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(1000);
                /*//点击第一个列表
                Rect rect = new Rect();
                searchCounts.get(0).getBoundsInScreen(rect);
                Point position = new Point(rect.left + (int)(rect.width() / 2), rect.top + rect.height() + 110);
                if(position.y > 500){
                    return false;
                }
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                GestureDescription gesture = builder.build();
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);*/
                return true;
            }
        }catch (Exception e){
            Log.i(TAG, "输入账号查询好友失败", e);
        }
        return false;
    }

    /**
     * 是否输入账号
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isInputName(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                String text1 = searchCounts.get(0).getText() == null ? "" : searchCounts.get(0).getText().toString();
                if(!"".equals(text1) && !"搜索".equals(text1)){
                    return true;
                }
            }
        }catch (Exception e){
            Log.i(TAG, "输入账号查询好友失败", e);
        }
        return false;
    }

    /**
     * 点击好友列表
     * @param rooNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static int clickFriendView(AccessibilityNodeInfo rooNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> linkmanCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.LINKMAN_TEXT_NAME);
            Log.i(TAG, "联系人文本框数量：" + linkmanCounts.size());
            if(linkmanCounts.size() > 0){
                //点击第一个好友
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(linkmanCounts.get(0).getParent().getParent().getChild(1));
                isChat = true;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            List<AccessibilityNodeInfo> dailyCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.DAILY_USE_TEXT_NAME);
            Log.i(TAG, "最常使用文本框数量：" + dailyCounts.size());
            if(dailyCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(dailyCounts.get(0).getParent().getParent().getChild(1));
                isChat = true;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            List<AccessibilityNodeInfo> searchBtnCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.SEARCH_MOBILE_QQ_BTN_NAME);
            Log.i(TAG, "查找手机/QQ号按钮数量：" + searchBtnCounts.size());
            if(searchBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchBtnCounts.get(0).getParent());
                isChat = true;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            return 2;
        }catch (Exception e){
            Log.e(TAG, "点击查询好友失败", e);
        }
        return 0;
    }

    /**
     * 发送消息
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean sendMessage(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(2000);
            Log.i(TAG, "发送消息");
            //判断当前是文字输入框还是语音
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());

            List<AccessibilityNodeInfo> infoBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("聊天信息");
            Log.i(TAG, "聊天信息按钮数量：" + infoBtnCounts.size());
            if(infoBtnCounts.size() <= 0){
                isChat = false;
                return false;
            }
            if(editCounts.size() > 0){
                //输入消息
                Bundle arguments  = new Bundle();

                //随机消息数
                Random random = new Random();
                int msgNum = random.nextInt(maxMsgNum - minMsgNum) + minMsgNum;
                if(msgNum == 0){
                    msgNum = 1;
                }
//                msgNum = 1;
                Log.i(TAG, "发送消息数：" + msgNum);
                for(int i = 0;i < msgNum;i++){
                    int msgIndex = random.nextInt(msgList.size());
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, msgList.get(msgIndex));
                    editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    Thread.sleep(1000);
                    //点击发送，原先的更多按钮位置
                    List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SEND_BTN_NAME);
                    Log.i(TAG, "发送按钮数量：" + sendBtnCounts.size());
                    if(sendBtnCounts.size() > 0){
                        sendBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    }
                    int time = (random.nextInt(maxTime - minTime + 1) + minTime);
                    Log.i(TAG,  "发送消息间隔时间：" + time + "秒");
                    Thread.sleep(time  * 1000);
                }
                isChat = false;
                return true;
            }else{
                Log.i(TAG, "点击切换文本框");
                //切换到文本输入框
                List<AccessibilityNodeInfo> changeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CHANGE_BTN_NAME);
                Log.i(TAG, "文本切换按钮数量：" + changeBtnCounts.size());
                if(changeBtnCounts.size() > 0){
                    Thread.sleep(1000);
                    changeBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }
        }catch (Exception e){
            Log.i(TAG, "发送消息失败", e);
        }
        return false;
    }

    /**
     * 清空好友账号
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean inputNull(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                Log.i(TAG, "清空好友账号:");
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, "");
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(1000);
            }
        }catch (Exception e){
            Log.i(TAG, "清空好友账号失败", e);
        }
        return false;
    }

    /**
     * 判断是否为聊天界面
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isChatPage(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "判断是否为聊天界面");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> infoBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("聊天信息");
            Log.i(TAG, "聊天信息按钮数量：" + infoBtnCounts.size());
            if(infoBtnCounts.size() > 0){
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否为聊天界面失败", e);
        }
        return false;
    }

    /**
     * 判断是否填写账号
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isInputAccount(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            boolean ischatPage = isChatPage(rootNodeInfo, event);
            if(ischatPage){
                return true;
            }
            Log.i(TAG, "判断是否填写账号");
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                List<AccessibilityNodeInfo> closeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("清除");
                if(closeBtnCounts.size() > 0){
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否输入搜索账号失败", e);
        }
        return false;
    }

    /**
     * 点击确定按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickConfirmBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> confirmBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CONFIRM_BTN_NAME);
            Log.i(TAG, "确定按钮数量：" + confirmBtnCounts.size());
            if(confirmBtnCounts.size() > 0){
                Log.i(TAG, "点击确定按钮");
                confirmBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击确定按钮失败", e);
        }
        return false;
    }

    /**
     * 点击发消息按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSendMsgBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> sendMsgBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SEND_MSG_BTN_NAME);
            Log.i(TAG, "发消息按钮数量：" + sendMsgBtnCounts.size());
            if(sendMsgBtnCounts.size() > 0){
                Log.i(TAG, "点击发消息按钮");
                MyAccessibilityService.flag = 1;
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(sendMsgBtnCounts.get(0));
                myAccessibilityService.opScreen(gesture);
            }

        }catch (Exception e){
            Log.e(TAG, "点击发送消息按钮失败", e);
        }
        return false;
    }



    /**
     * 判断是否结束
     * @return
     */
    public static boolean isFinish(){
        try{

            if(msgList == null || friendList == null || msgNum <= msgIndex){
                //初始化数据
                index = 0;
                friendList = null;
                msgList = null;
                msgNum = 0;
                msgIndex = 0;
                isSearch = false;
                isChat = false;
                isClick = true;
                MyAccessibilityService.resetParam();
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "发送消息结束";
                MainActivity.mHandler.sendMessage(message);
                Log.i(TAG, "返回我的APP");
                MainActivity.openMyApp();
                return true;
            }else if(msgNum > msgIndex && friendList.size() <= index){
                index = 0;
                msgIndex++;
                isSearch = false;
                isChat = false;
                isClick = true;
                MyAccessibilityService.isRun = 1;
            }
        }catch (Exception e){
            Log.e(TAG, "判断发送消息是否结束失败", e);
        }
        return false;
    }
}
