package com.qk.manager.impl;

import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 发送视频通话操作
 */
public class SendVideoCall implements TaskIntfService {


    private static final String TAG = SendVideoCall.class.getName();

    //好友
    public static String friend = null;
    //通话时长
    public static Integer callTime = 0;

    private static String viewName = "";

    private static int isChat = 0;

    private static boolean isSearch = false;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {

        try{

            if(friend == null || "".equals(friend)){
                finish(2);
            }

            Log.i(TAG, "isChat:" + isChat);
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.i(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.WECHAT_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页微信标签被点击，点击搜索按钮
                                    info = rootNodeInfo;
                                    clickSearchBtn(info, event, myAccessibilityService);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if(("android.widget.FrameLayout".equals(className) || "android.widget.ListView".equals(className) || "android.widget.EditText".equals(className))
                            && ("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName) || "com.tencent.mm.ui.LauncherUI".equals(viewName))){
                        //点击好友列表
                        if(!isSearch){
                            return ;
                        }
                        if(isChat == 0){
                            info =  rootNodeInfo;
                            int flag = clickFriendView(info, event, myAccessibilityService);
                            if(flag == 2) {
                                //没有找到好友
                                finish(2);
                            }
                            break;
                        }
                    }
                    if(("com.tencent.mm.ui.chatting.ChattingUI".equals(viewName) || ("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName))
                            || ("com.tencent.mm.ui.LauncherUI".equals(viewName) || "com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(viewName)))
                            && ("android.widget.LinearLayout".equals(className) || "android.widget.FrameLayout".equals(className)
                            || "android.widget.Button".equals(className) || "android.widget.EditText".equals(className))){
                        if(!isSearch){
                            return ;
                        }
                        if(isChat == 1){
                            //聊天页面，点击更多按钮
                            info = rootNodeInfo;
                            clickMoreBtn(info, event);
                        }else if(isChat == 2){
                            //聊天页面，更多窗口
                            clickCallBtn(info, event, myAccessibilityService);
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mm") > -1) {
                        viewName = className;
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(className)){
                        boolean isSearch = clickSearchBtn(info, event, myAccessibilityService);
                        if(!isSearch){
                            clickWechatBtn(info, event, myAccessibilityService);
                        }
                    }else if("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(className)){
                        //好友查询页面
                        inputFriendName(info, event, friend);
                    }if(("android.support.design.widget.c".equals(className) || "android.support.design.widget.a".equals(className)) &&
                            ("com.tencent.mm.ui.chatting.ChattingUI".equals(viewName) || ("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName))
                                    || "com.tencent.mm.ui.LauncherUI".equals(viewName) || "com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(viewName))) {
                        if (isChat == 3) {
                            //选择视频/语音通话界面
                            clickVideoCallBtn(info, event);

                        }

                    }
                    if("com.tencent.mm.ui.widget.a.c".equals(className)){
                        //提示框
                        boolean isFriend = clickConfirmBtn(info, event);
                        //没有找到好友
                        if(!isFriend){
                            finish(2);
                        }
                    }

                    if("com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(className)){
                        //好友信息页面，点击发消息按钮
                        clickSendMsgBtn(info, event, myAccessibilityService);
                    }

                    break;
            }
        }catch (Exception e){
            Log.e(TAG, "发送视频通话失败", e);
        }

    }

    /**
     * 点击微信标签按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickWechatBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            Log.i(TAG, "点击微信标签按钮");
            GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 1);
            if(gesture != null){
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击微信标签按钮失败", e);
        }
        return false;
    }

    /**
     * 点击搜索按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSearchBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            //获取更多按钮
            List<AccessibilityNodeInfo> moreBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MORE_BTN_NAME);
            Log.i(TAG, "更多按钮数量：" + moreBtnCounts.size());
            if(moreBtnCounts.size() > 0){

                //计算位置点击搜索按钮
                Rect rect = new Rect();
                moreBtnCounts.get(moreBtnCounts.size() - 1).getBoundsInScreen(rect);
                //计算搜索按钮位置
                Point position = new Point(rect.left - (int)(rect.width() / 2), rect.top + (int)(rect.height() / 2));
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                GestureDescription gesture = builder.build();
                //点击搜索按钮
                Thread.sleep(1000);
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击搜索按钮失败", e);
        }
        return false;
    }


    /**
     * 输入好友账号
     * @param rootNodeInfo
     * @param event
     * @param name
     * @return
     */
    public static boolean inputFriendName(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String name){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                isSearch = true;
                Log.i(TAG, "输入好友账号:" + name);
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, name);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(1000);
            }
        }catch (Exception e){
            Log.i(TAG, "输入账号查询好友失败", e);
        }
        return false;
    }

    /**
     * 点击好友列表
     * @param rooNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static int clickFriendView(AccessibilityNodeInfo rooNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> linkmanCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.LINKMAN_TEXT_NAME);
            Log.i(TAG, "联系人文本框数量：" + linkmanCounts.size());
            if(linkmanCounts.size() > 0){
                //点击第一个好友
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(linkmanCounts.get(0).getParent().getParent().getChild(1));
                isChat = 1;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            List<AccessibilityNodeInfo> dailyCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.DAILY_USE_TEXT_NAME);
            Log.i(TAG, "最常使用文本框数量：" + dailyCounts.size());
            if(dailyCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(dailyCounts.get(0).getParent().getParent().getChild(1));
                isChat = 1;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            List<AccessibilityNodeInfo> searchBtnCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.SEARCH_MOBILE_QQ_BTN_NAME);
            Log.i(TAG, "查找手机/QQ号按钮数量：" + searchBtnCounts.size());
            if(searchBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchBtnCounts.get(0).getParent());
                isChat = 1;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            return 2;
        }catch (Exception e){
            Log.e(TAG, "点击查询好友失败", e);
        }
        return 0;
    }

    /**
     * 点击更多按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickMoreBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "点击更多按钮");
            //判断当前是文字输入框还是语音
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());
            if(editCounts.size() <= 0){
                //点击更多按钮
                List<AccessibilityNodeInfo> moreBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MORE_BTN_NAME);
                Log.i(TAG, "更多按钮数量：" + moreBtnCounts.size());
                if(moreBtnCounts.size() > 0){
                    moreBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
                isChat = 2;
                return true;
            }else{
                //切换到文本输入框
                List<AccessibilityNodeInfo> changeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CHANGE_BTN_NAME);
                Log.i(TAG, "文本切换按钮数量：" + changeBtnCounts.size());
                if(changeBtnCounts.size() > 0){
                    Log.i(TAG, "点击切换文本框");
                    Thread.sleep(1000);
                    changeBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }
        }catch (Exception e){
            Log.i(TAG, "点击更多按钮失败", e);
        }
        return false;
    }

    /**
     * 点击视频通话按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickCallBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> callBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.VIDEO_CALL_BTN_NAME);
            Log.i(TAG, "视频通话按钮数量：" + callBtnCounts.size());
            if(callBtnCounts.size() > 0){
                Log.i(TAG, "点击视频通话按钮");
                Thread.sleep(1000);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(callBtnCounts.get(0));
                if(gesture != null){
                    myAccessibilityService.opScreen(gesture);
                    isChat = 3;
                }
                return true;
            }else{
                isChat = 1;
            }
        }catch (Exception e){
            Log.e(TAG, "点击视频通话按钮失败", e);
        }
        return false;
    }

    /**
     * 点击视频通话按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickVideoCallBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> callBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.VIDEO_CALL_BTN_NAME);
            Log.i(TAG, "视频通话按钮数量：" + callBtnCounts.size());
            if(callBtnCounts.size() > 0){
                Log.i(TAG, "点击视频通话按钮");
                Thread.sleep(1000);
                callBtnCounts.get(0).getParent().performAction(AccessibilityNodeInfo.ACTION_CLICK);
                //等待时长
                MyAccessibilityService.timeOut = new Date().getTime() + (callTime * 1000);
                //结束任务
                Thread.sleep((callTime + 13) * 1000);
                finish(1);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击视频通话按钮失败", e);
        }
        return false;
    }

    /**
     * 点击确定按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickConfirmBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> confirmBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CONFIRM_BTN_NAME);
            Log.i(TAG, "确定按钮数量：" + confirmBtnCounts.size());
            if(confirmBtnCounts.size() > 0){
                //判断是好友提示框或者流量提示框
                List<AccessibilityNodeInfo> textCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.FLOW_TEXT_NAME);
                Log.i(TAG, "流量文本数据量：" + textCounts.size());
                Log.i(TAG, "点击确定按钮");
                confirmBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                if(textCounts.size() == 0){
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击确定按钮失败", e);
        }
        return false;
    }

    /**
     * 点击发消息按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSendMsgBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> sendMsgBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SEND_MSG_BTN_NAME);
            Log.i(TAG, "发消息按钮数量：" + sendMsgBtnCounts.size());
            if(sendMsgBtnCounts.size() > 0){
                Log.i(TAG, "点击发消息按钮");
                MyAccessibilityService.flag = 1;
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(sendMsgBtnCounts.get(0));
                myAccessibilityService.opScreen(gesture);
            }

        }catch (Exception e){
            Log.e(TAG, "点击发送消息按钮失败", e);
        }
        return false;
    }

    /**
     * 发送视频通话结束
     * @param isSuccess
     */
    public static void finish(int isSuccess){
        try{
            friend = null;
            callTime = 0;
            isChat = 0;
            isSearch = false;
            viewName = "";
            MyAccessibilityService.resetParam();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "发送视频通话结束";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "返回我的APP");
            //延迟3秒返回app，否则会出现未拨通情况
            Thread.sleep(3000);
            MainActivity.openMyApp();

        }catch (Exception e){
            Log.i(TAG, "发送视频通话结束失败", e);
        }
    }


}
