package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class WXFriendCheck implements TaskIntfService {

    private static final String TAG = QQFriendCheck.class.getName();

    private static String viewName = "";
    //好友集合
    public static JSONArray accountMap;
    //好友索引
    public static Integer friendIndex = 0;
    //消息内容
    public static List<String> msgList = new ArrayList<String>();
    //是否发送消息
    public static boolean isSendMsg = false;
    //关联添加好友任务ID
    public static int addFriendId = 0;

    private static boolean isSearch = false;

    private static int isChat = 0;

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{

            if(isFinish()){
                return ;
            }

            Log.d(TAG, "微信好友检查");
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr == null ? "" : classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType) {
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    Log.d(TAG, "页面被点击");
                    if (info != null) {
                        if (info.getChildCount() > 0) {
                            for (int i = 0; i < info.getChildCount(); i++) {
                                if (Constants.WECHAT_BTN_NAME.equals(info.getChild(i).getText())) {
                                    //首页微信标签被点击，点击搜索按钮
                                    info = rootNodeInfo;
                                    clickSearchBtn(info, event, myAccessibilityService);
                                    break;
                                }
                            }
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    Log.d(TAG, "页面内容被改变");
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if("".equals(viewName)){
                        viewName = "com.tencent.mm.ui.LauncherUI";
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(viewName) && !isSearch) {
                        boolean isSearch = clickSearchBtn(info, event, myAccessibilityService);
                        if (!isSearch) {
                            clickWechatBtn(info, event, myAccessibilityService);
                        }
                    }

                    if(("android.widget.FrameLayout".equals(className) || "android.widget.ListView".equals(className) || "android.widget.EditText".equals(className))
                            && ("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName) || "com.tencent.mm.ui.LauncherUI".equals(viewName))){
                        //点击好友列表
                        if(!isSearch){
                            return ;
                        }
                        if(isChat == 0){
                            JSONObject json = (JSONObject) accountMap.get(friendIndex);
                            int state = json.getInt("state");
                            String account = json.getString("account");
                            info =  rootNodeInfo;
                            if(!isInputName(info, event, account)) {
                                //搜索好友页面，输入好友账号
                                inputFriendName(info, event, account);
                            }else if(!isChatPage(info, event)){
                                int flag = clickFriendView(info, event, myAccessibilityService);
                                if(flag == 2) {
                                    //没有找到好友，下一个好友
                                    friendIndex++;
                                    isChat = 0;
                                }
                                if(flag == 1){
                                    //查询到好友
                                    if(state == 3){
                                        //向服务器发送好友添加成功消息
                                        try{
                                            //已经添加修改好友状态
                                            String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改微信好友状态",
                                                    DataUtil.setAddFriendInfo(account, 4, addFriendId, 0));
                                            MainActivity.myClient.send(msg);
                                        }catch (Exception e){
                                            Log.e(TAG, "向服务器发送消息失败", e);
                                        }
                                    }
                                }
                            }
                            break;
                        }
                    }

                    if(("com.tencent.mm.ui.chatting.ChattingUI".equals(viewName) || ("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(viewName))
                            || ("com.tencent.mm.ui.LauncherUI".equals(viewName) || "com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(viewName)))
                            && ("android.widget.LinearLayout".equals(className) || "android.widget.FrameLayout".equals(className)
                            || "android.widget.Button".equals(className) || "android.widget.ListView".equals(className))){
                        //聊天页面操作
                        if(!isSearch){
                            //还没有点击搜索按钮
                            return ;
                        }
                        if(isChatPage(info, event)){
                            //聊天页面
                            info = rootNodeInfo;
                            JSONObject json = (JSONObject) accountMap.get(friendIndex);
                            int state = json.getInt("state");
                            String account = json.getString("account");
                            //查看是否回复消息
                            boolean isReply2 = isReply2(info);
                            if(isReply2) {
                                //已回复，向服务器发送好友已回复消息
                                try {
                                    //好友已回复状态
                                    String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改微信好友状态",
                                            DataUtil.setAddFriendInfo(account, 6, addFriendId, 0));
                                    MainActivity.myClient.send(msg);
                                } catch (Exception e) {
                                    Log.e(TAG, "向服务器发送消息失败", e);
                                }
                                friendIndex++;
                                isChat = 0;
                                //后退
                                GestureDescription gesture = MyAccessibilityService.clickBackBtn(event, info);
                                if(gesture == null){
                                    Thread.sleep(1000);
                                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                    Thread.sleep(1000);
                                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                }else{
                                    MyAccessibilityService.flag = 1;
                                    myAccessibilityService.opScreen(gesture);
                                }
                                break;
                            }
                            //发送消息
                            boolean isSend = sendMessage(info, event);
                            if(isSend){
                                friendIndex++;
                                isChat = 0;
                                Thread.sleep(5 * 1000);
                                boolean isReply = isReply(info);
                                if(isReply) {
                                    //已回复，向服务器发送好友已回复消息
                                    try {
                                        //好友已回复状态
                                        String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改微信好友状态",
                                                DataUtil.setAddFriendInfo(account, 6, addFriendId, 0));
                                        MainActivity.myClient.send(msg);
                                    } catch (Exception e) {
                                        Log.e(TAG, "向服务器发送消息失败", e);
                                    }
                                }
                                //后退
                                GestureDescription gesture = MyAccessibilityService.clickBackBtn(event, info);
                                if(gesture == null){
                                    Thread.sleep(1000);
                                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                    Thread.sleep(1000);
                                    myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                }else{
                                    MyAccessibilityService.flag = 1;
                                    myAccessibilityService.opScreen(gesture);
                                }
                            }
                            break;
                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    if (className.indexOf("com.tencent.mm") > -1) {
                        viewName = className;
                    }

                    if("com.tencent.mm.ui.LauncherUI".equals(className)){
                        boolean isSearch = clickSearchBtn(info, event, myAccessibilityService);
                        if(!isSearch){
                            clickWechatBtn(info, event, myAccessibilityService);
                        }
                    }else if("com.tencent.mm.plugin.fts.ui.FTSMainUI".equals(className)) {
                        //好友查询页面
                        JSONObject json = (JSONObject) accountMap.get(friendIndex);
                        String account = json.getString("account");
                        inputFriendName(info, event, account);
                    }
                    if(className.indexOf("com.tencent.mm.ui.widget") > -1){
                        //提示框
                        boolean isFriend = clickConfirmBtn(info, event);
                        //没有找到好友
                        if(isFriend){
                            friendIndex++;
                            isChat = 0;
                            if(isFinish()) {
                                return;
                            }
                            Thread.sleep(1000);
                            JSONObject json = (JSONObject) accountMap.get(friendIndex);
                            String account = json.getString("account");
                            inputFriendName(info, event, account);
                        }
                    }

                    if("com.tencent.mm.plugin.profile.ui.ContactInfoUI".equals(className)){
                        //好友信息页面，点击发消息按钮
                        boolean isClick = clickSendMsgBtn(info, event, myAccessibilityService);
                        if(isClick){
                            JSONObject json = (JSONObject) accountMap.get(friendIndex);
                            int state = json.getInt("state");
                            String account = json.getString("account");
                            if(state == 3){
                                //向服务器发送好友添加成功消息
                                try{
                                    //已经添加修改好友状态
                                    String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "修改微信好友状态",
                                            DataUtil.setAddFriendInfo(account, 4, addFriendId, 0));
                                    MainActivity.myClient.send(msg);
                                }catch (Exception e){
                                    Log.e(TAG, "向服务器发送消息失败", e);
                                }
                            }
                        }else{
                            //没有通过，下一个好友
                            friendIndex++;
                            isChat = 0;
                            if(isFinish()) {
                                return;
                            }
                            Thread.sleep(1000);
                            myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                        }

                    }

                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "微信好友检查失败", e);
        }
    }

    /**
     * 点击微信标签按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickWechatBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            Log.i(TAG, "点击微信标签按钮");
            GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 1);
            if(gesture != null){
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击微信标签按钮失败", e);
        }
        return false;
    }

    /**
     * 点击搜索按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSearchBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            //获取更多按钮
            List<AccessibilityNodeInfo> moreBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.MORE_BTN_NAME);
            Log.i(TAG, "更多按钮数量：" + moreBtnCounts.size());
            if(moreBtnCounts.size() > 0){

                //计算位置点击搜索按钮
                Rect rect = new Rect();
                moreBtnCounts.get(moreBtnCounts.size() - 1).getBoundsInScreen(rect);
                //计算搜索按钮位置
                Point position = new Point(rect.left - (int)(rect.width() / 2), rect.top + (int)(rect.height() / 2));
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                GestureDescription gesture = builder.build();
                //点击搜索按钮
                Thread.sleep(1000);
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击搜索按钮失败", e);
        }
        return false;
    }

    /**
     * 输入好友账号
     * @param rootNodeInfo
     * @param event
     * @param name
     * @return
     */
    public static boolean inputFriendName(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String name){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                isSearch = true;
                Log.i(TAG, "输入好友账号:" + name);
                Thread.sleep(1000);
                Bundle arguments  = new Bundle();
                arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, name);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                searchCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                Thread.sleep(1000);
            }
        }catch (Exception e){
            Log.i(TAG, "输入账号查询好友失败", e);
        }
        return false;
    }

    /**
     * 点击好友列表
     * @param rooNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static int clickFriendView(AccessibilityNodeInfo rooNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> linkmanCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.LINKMAN_TEXT_NAME);
            Log.i(TAG, "联系人文本框数量：" + linkmanCounts.size());
            if(linkmanCounts.size() > 0){
                //点击第一个好友
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(linkmanCounts.get(0).getParent().getParent().getChild(1));
                isChat = 1;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            List<AccessibilityNodeInfo> dailyCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.DAILY_USE_TEXT_NAME);
            Log.i(TAG, "最常使用文本框数量：" + dailyCounts.size());
            if(dailyCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(dailyCounts.get(0).getParent().getParent().getChild(1));
                isChat = 1;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 1;
            }
            List<AccessibilityNodeInfo> searchBtnCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.SEARCH_MOBILE_QQ_BTN_NAME);
            Log.i(TAG, "查找手机/QQ号按钮数量：" + searchBtnCounts.size());
            if(searchBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchBtnCounts.get(0).getParent());
                isChat = 1;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 3;
            }

            List<AccessibilityNodeInfo> searchWxBtnCounts = rooNodeInfo.findAccessibilityNodeInfosByText(Constants.SEARCH_WX_ACCOUNT_BTN_NAME);
            Log.i(TAG, "查找微信号数量：" + searchWxBtnCounts.size());
            if(searchWxBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(searchWxBtnCounts.get(0).getParent());
                isChat = 1;
                if(gesture != null){
                    MyAccessibilityService.flag = 1;
                    myAccessibilityService.opScreen(gesture);
                }
                return 3;
            }
            return 2;
        }catch (Exception e){
            Log.e(TAG, "点击查询好友失败", e);
        }
        return 0;
    }

    /**
     * 点击确定按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickConfirmBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> confirmBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CONFIRM_BTN_NAME);
            Log.i(TAG, "确定按钮数量：" + confirmBtnCounts.size());
            if(confirmBtnCounts.size() > 0){
                //判断是好友提示框或者流量提示框
                List<AccessibilityNodeInfo> textCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.FLOW_TEXT_NAME);
                Log.i(TAG, "流量文本数据量：" + textCounts.size());
                Log.i(TAG, "点击确定按钮");
                confirmBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                if(textCounts.size() == 0){
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击确定按钮失败", e);
        }
        return false;
    }

    /**
     * 点击发消息按钮
     * @param rootNodeInfo
     * @param event
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickSendMsgBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, MyAccessibilityService myAccessibilityService){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> sendMsgBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SEND_MSG_BTN_NAME);
            Log.i(TAG, "发消息按钮数量：" + sendMsgBtnCounts.size());
            if(sendMsgBtnCounts.size() > 0){
                Log.i(TAG, "点击发消息按钮");
                MyAccessibilityService.flag = 1;
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(sendMsgBtnCounts.get(0));
                myAccessibilityService.opScreen(gesture);
            }

        }catch (Exception e){
            Log.e(TAG, "点击发送消息按钮失败", e);
        }
        return false;
    }

    /**
     * 判断是否为聊天界面
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isChatPage(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Log.i(TAG, "判断是否为聊天界面");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> infoBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("聊天信息");
            Log.i(TAG, "聊天信息按钮数量：" + infoBtnCounts.size());
            if(infoBtnCounts.size() > 0){
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否为聊天界面失败", e);
        }
        return false;
    }

    /**
     * 发送消息
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean sendMessage(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(2000);
            Log.i(TAG, "发送消息");
            //判断当前是文字输入框还是语音
            List<AccessibilityNodeInfo> editCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editCounts.size());

            List<AccessibilityNodeInfo> infoBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("聊天信息");
            Log.i(TAG, "聊天信息按钮数量：" + infoBtnCounts.size());
            if(infoBtnCounts.size() <= 0){
                isChat = 2;
                return false;
            }
            if(editCounts.size() > 0){
                //输入消息
                Bundle arguments  = new Bundle();

                //随机消息数
                Log.i(TAG, "发送消息数：" + msgList.size());
                for(int i = 0;i < msgList.size();i++){
                    arguments.putCharSequence(AccessibilityNodeInfo.ACTION_ARGUMENT_SET_TEXT_CHARSEQUENCE, msgList.get(i));
                    editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_FOCUS);
                    editCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_SET_TEXT, arguments);
                    Thread.sleep(1000);
                    //点击发送，原先的更多按钮位置
                    List<AccessibilityNodeInfo> sendBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.SEND_BTN_NAME);
                    Log.i(TAG, "发送按钮数量：" + sendBtnCounts.size());
                    if(sendBtnCounts.size() > 0){
                        sendBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    }
                    Thread.sleep(5  * 1000);
                }
                isChat = 2;
                return true;
            }else{
                Log.i(TAG, "点击切换文本框");
                //切换到文本输入框
                List<AccessibilityNodeInfo> changeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CHANGE_BTN_NAME);
                Log.i(TAG, "文本切换按钮数量：" + changeBtnCounts.size());
                if(changeBtnCounts.size() > 0){
                    Thread.sleep(1000);
                    changeBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }
        }catch (Exception e){
            Log.i(TAG, "发送消息失败", e);
        }
        return false;
    }

    /**
     * 查看是否回复
     * @param rootNodeInfo
     * @return
     */
    public static boolean isReply(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "查看是否回复");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> imgViewCounts = rootNodeInfo.findAccessibilityNodeInfosByText("头像");
            Log.i(TAG, "头像数量：" + imgViewCounts.size());
            if(imgViewCounts.size() > 0){
                AccessibilityNodeInfo linear = imgViewCounts.get(imgViewCounts.size() - 1).getParent();
                AccessibilityWechatUtil.clickCoordinate(linear);
                AccessibilityWechatUtil.recycleCheck(linear);
                String type = linear.getChild(linear.getChildCount() - 1).getClassName() + "";
                Log.i(TAG, "最后一个组件类型：" + type);
                if("android.view.View".equals(type)){
                    //已回复
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "查看是否回复失败", e);
        }
        return false;
    }

    /**
     * isReply2
     * @param rootNodeInfo
     * @return
     */
    public static boolean isReply2(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "查看是否回复2条以上");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> imgViewCounts = rootNodeInfo.findAccessibilityNodeInfosByText("头像");
            Log.i(TAG, "头像数量：" + imgViewCounts.size());
            if(imgViewCounts.size() > 1){
                int i = 1;
                while (i <= 2){
                    AccessibilityNodeInfo linear = imgViewCounts.get(imgViewCounts.size() - i).getParent();
                    AccessibilityWechatUtil.clickCoordinate(linear);
                    AccessibilityWechatUtil.recycleCheck(linear);
                    String type = linear.getChild(linear.getChildCount() - 1).getClassName() + "";
                    Log.i(TAG, "最后一个组件类型：" + type);
                    if(!"android.view.View".equals(type)){
                        return false;
                    }
                    i++;
                }
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "查看是否回复2条以上失败", e);
        }
        return false;
    }

    /**
     * 是否输入账号
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean isInputName(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event, String account){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> searchCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(searchCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "搜索框数量：" + searchCounts.size());
            if(searchCounts.size() > 0){
                String text1 = searchCounts.get(0).getText() == null ? "" : searchCounts.get(0).getText().toString();
                if(account.equals(text1)){
                    return true;
                }
            }
        }catch (Exception e){
            Log.i(TAG, "输入账号查询好友失败", e);
        }
        return false;
    }

    /**
     * 判断是否结束
     * @return
     */
    public static boolean isFinish(){
        try{
            if(accountMap == null || accountMap.length() <= friendIndex){
                accountMap = new JSONArray();
                friendIndex = 0;
                msgList = new ArrayList<String>();
                viewName = "";
                isSearch = false;
                isChat = 0;
                try{
                    String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "完成任务",
                            DataUtil.setfinishTask(MyAccessibilityService.taskId, MainActivity.MOBILE_IMEI));
                    MainActivity.myClient.send(msg);
                }catch (Exception e){
                    Log.e(TAG, "发送完成任务消息失败", e);
                }
                MyAccessibilityService.resetParam();
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "微信好友检查结束";
                MainActivity.mHandler.sendMessage(message);
                Log.i(TAG, "返回我的APP");
                MainActivity.openMyApp();
            }
        }catch (Exception e){
            Log.e(TAG, "判断结束失败", e);
        }
        return false;
    }

}
