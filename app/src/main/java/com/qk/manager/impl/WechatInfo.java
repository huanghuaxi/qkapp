package com.qk.manager.impl;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.ArrayList;
import java.util.List;

/**
 *  获取微信信息
 */
public class WechatInfo implements TaskIntfService {

    private static final String TAG = MyAccessibilityService.class.getName();

    //当前页面
    private static String viewName = "";

    @Override
    public void handler(Object obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService) {
        try{
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String className = classNameChr.toString();
            Log.d(TAG, "eventType:" + eventType );
            Log.d(TAG, "className:" + className );
            Log.d(TAG, "viewName:" + viewName );
            AccessibilityNodeInfo info = event.getSource();

            switch (eventType){
                case AccessibilityEvent.TYPE_VIEW_CLICKED:
                    if(info != null){
                        Thread.sleep(1000);
//                        AccessibilityWechatUtil.recycleCheck(info);
                        if(info.getChildCount() > 0){
                            for (int i = 0;i < info.getChildCount();i++){
                                Log.i(TAG, "text:" + info.getChild(i).getText());
                                if(Constants.MINE_BTN_NAME.equals(info.getChild(i).getText())){
                                    //首页我的标签被点击，点击设置按钮
                                    Log.i(TAG, "点击设置按钮");
                                    Thread.sleep(1000);
                                    info = rootNodeInfo;
                                    GestureDescription gesture = clickSettingBtn(info, event);
                                    if(gesture != null){
                                        MyAccessibilityService.flag = 1;
                                        myAccessibilityService.opScreen(gesture);
                                    }
                                }else if(Constants.ADDR_LIST_BTN_NAME.equals(info.getChild(i).getText()) || Constants.WECHAT_BTN_NAME.equals(info.getChild(i).getText())
                                        || Constants.FIND_BTN_NAME.equals(info.getChild(i).getText())){
                                    //页面没有加载完成点到其他按钮，重新点击我的按钮
                                    Thread.sleep(1000);
                                    GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 4);
                                    if(gesture != null){
                                        MyAccessibilityService.flag = 1;
                                        myAccessibilityService.opScreen(gesture);
                                    }
                                }
                            }

                        }
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                    if(info == null){
                        info = rootNodeInfo;
                    }
                    if("android.widget.FrameLayout".equals(className) && "com.tencent.mm.plugin.setting.ui.setting.SettingsAccountInfoUI".equals(viewName)){
                        Log.i(TAG, "获取微信信息");
                        getWechatInfo(info, event);
                        MyAccessibilityService.resetParam();
                        //返回主页面
                        Log.i(TAG, "返回我的APP");
                        Thread.sleep(1000);
                        MainActivity.openMyApp();
                        Thread.sleep(1000);
                        MainActivity.lineService();
                    }
                    break;
                case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                    Log.d(TAG, "页面变化");
                    Log.d(TAG, "event:" + event.toString());
                    if(className.indexOf("com.tencent.mm") > -1 ){
                        viewName = className;
                    }
                    if(MyAccessibilityService.flag == 0) {
                        MyAccessibilityService.flag = 1;
                        if (info == null) {
                            info = rootNodeInfo;
                        }
                        //获取微信信息
                        if("com.tencent.mm.ui.LauncherUI".equals(className)){
                            //首页
//                            clickMineBtn(info, event);
                            Thread.sleep(1000);
                            GestureDescription gesture = AccessibilityWechatUtil.clickBottomMenuBtn(rootNodeInfo, 4);
                            if(gesture != null){
                                MyAccessibilityService.flag = 1;
                                myAccessibilityService.opScreen(gesture);
                            }
                        }else if("com.tencent.mm.plugin.setting.ui.setting.SettingsUI".equals(className)){
                            //设置页面
                            Thread.sleep(1000);
                            GestureDescription gesture = clickAccountBtn(info, event);
                            myAccessibilityService.opScreen(gesture);
                        }else if(!"com.tencent.mm.plugin.setting.ui.setting.SettingsAccountInfoUI".equals(className)){
                            //后退
                            GestureDescription gesture = MyAccessibilityService.clickBackBtn(event, info);
                            if(gesture != null){
                                myAccessibilityService.opScreen(gesture);
                            }else{
                                myAccessibilityService.performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }
                        }
                    }
                    MyAccessibilityService.flag = 0;
                    break;
            }

        }catch (Exception e){
            Log.e(TAG, "获取微信信息失败", e);
        }
    }

    /**
     * 点击设置按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickSettingBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> installBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("设置");
            Log.i(TAG, "设置按钮数量：" + installBtnCounts.size());
            if(installBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(installBtnCounts.get(0));
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击设置按钮失败");
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 获取微信信息
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean getWechatInfo(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> listViewCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(listViewCounts, rootNodeInfo, "android.widget.ListView");
            Log.i(TAG, "listView数量：" + listViewCounts.size());
            if(listViewCounts.size() > 0){
                Log.i(TAG, "获取微信信息");
                String wechatAccount = listViewCounts.get(0).getChild(0).getChild(1).getText().toString();
                String mobile = listViewCounts.get(0).getChild(1).getChild(1).getText().toString();
                mobile = mobile.replace("+", "");
                MainActivity.wechatInfo.put("wechatAccount", wechatAccount);
                MainActivity.wechatInfo.put("mobile", mobile);
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "微信号：" + wechatAccount;
                MainActivity.mHandler.sendMessage(message);
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "手机号：" + mobile;
                MainActivity.mHandler.sendMessage(message);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "获取微信信息失败");
            e.printStackTrace();
        }
        Message message = MainActivity.mHandler.obtainMessage();
        message.what = 100;
        message.obj = "获取微信信息失败";
        MainActivity.mHandler.sendMessage(message);
        return false;
    }

    /**
     * 点击我的标签按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickMineBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, Constants.MAIN_PAGE_NAME);
            if(pageCounts.size() > 0) {
                AccessibilityNodeInfo relativeCounts = pageCounts.get(0).getParent();
                if(relativeCounts != null) {
                    //点击微信标签按钮
                    Log.i(TAG, "点击我的标签按钮");
                    Thread.sleep(1000);
                    relativeCounts.getChild(4).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }else{
                return false;
            }
        }catch (Exception e){
            Log.e(TAG, "点击我的按钮失败");
            e.printStackTrace();
            return false;
        }
        return true;
    }

    /**
     * 点击账号与安全按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static GestureDescription clickAccountBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            List<AccessibilityNodeInfo> accountBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("android:id/title");
            Log.i(TAG, "设置页面按钮数量：" + accountBtnCounts.size());
            if(accountBtnCounts.size() > 0){
                Thread.sleep(1000);
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(accountBtnCounts.get(0));
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击账号与安全按钮失败", e);
        }
        return null;
    }

}
