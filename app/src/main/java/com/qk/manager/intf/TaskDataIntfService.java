package com.qk.manager.intf;

import java.util.Map;

public interface TaskDataIntfService {

    public boolean handler(Object  obj, Map<String, Object> map);

}
