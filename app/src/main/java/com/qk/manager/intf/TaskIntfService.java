package com.qk.manager.intf;

import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.qkapp.MyAccessibilityService;

public interface TaskIntfService {

    public void handler(Object  obj, AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService);
}
