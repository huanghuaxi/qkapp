package com.qk.manager.taskdata.impl;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.qk.manager.impl.AcceptVideoCall;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.Map;

/**
 * 接受语音通话数据处理
 */
public class AcceptVideoCallData implements TaskDataIntfService {

    private static final String TAG = AcceptVoiceCallData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始接收视频通话任务";
            MainActivity.mHandler.sendMessage(message);
            Integer minTime =  Integer.parseInt(map.get("minTime").toString());
            Integer maxTime =  Integer.parseInt(map.get("maxTime").toString());
            Integer callTime =  Integer.parseInt(map.get("callTime").toString());
            AcceptVideoCall.minTime = minTime;
            AcceptVideoCall.maxTime = maxTime;
            AcceptVideoCall.callTime = callTime;
            AcceptVideoCall.opFlag = 0;
            finishTask(callTime);
        }catch (Exception e){
            Log.e(TAG, "接受语音通话数据处理失败", e);
        }
        return true;
    }

    /**
     * 太长时间没有接收到消息自动结束任务
     */
    public static Handler handler = new Handler();
    public static Runnable mRunnable = null;
    public static void finishTask(Integer time){
        try{
            if(mRunnable == null){
                mRunnable  = new Runnable(){
                    @Override
                    public void run() {
                        Log.i(TAG, "太长时间没有接收到消息自动结束任务");
                        if(AcceptVideoCall.opFlag == 0){
                            AcceptVideoCall.finish();
                        }else{
                            AcceptVideoCall.opFlag = 0;
                            handler.postDelayed(mRunnable,1000 * (AcceptVideoCall.maxTime + 60));
                        }
                    }
                };
            }
            handler.postDelayed(mRunnable,1000 * (time + 60));
        }catch (Exception e){
            Log.e(TAG, "自动结束任务失败", e);
        }
    }

}
