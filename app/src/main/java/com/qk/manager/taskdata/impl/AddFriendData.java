package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.impl.AddFriendServiceImpl;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;

import java.util.Map;

/**
 * 添加好友数据处理
 */
public class AddFriendData implements TaskDataIntfService {

    private static final String TAG = AddFriendData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            //添加好友
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始添加好友任务";
            MainActivity.mHandler.sendMessage(message);
            JSONArray json = new JSONArray( map.get("friendList").toString());
            AddFriendServiceImpl.friendList = DataUtil.jsonToList(json);
        }catch (Exception e){
            Log.e(TAG, "存入添加好友数据失败", e);
        }
        return true;
    }
}
