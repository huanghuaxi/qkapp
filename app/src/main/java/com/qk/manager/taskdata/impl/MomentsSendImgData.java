package com.qk.manager.taskdata.impl;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.MediaStore;
import android.util.Log;

import com.qk.manager.impl.MomentsSendImg;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * 朋友圈发送图片数据处理
 */
public class MomentsSendImgData implements TaskDataIntfService {

    private static final String TAG = MomentsSendImgData.class.getName();

    public static Handler handler;

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            String msg = map.get("msg") == null ? "" : map.get("msg").toString();
            String imgUrl = map.get("imgUrl") == null ? "" : map.get("imgUrl").toString();
            String suffix = ".jpg";
            if(imgUrl.indexOf(".") > -1){
                suffix = imgUrl.substring(imgUrl.lastIndexOf("."));
            }
            String fileName = new Date().getTime() + suffix;
            getImg(fileName, imgUrl);
            MomentsSendImg.msg = msg;
            MomentsSendImg.fileName = fileName;
        }catch (Exception e){
            Log.e(TAG, "朋友圈发送图片数据处理失败", e);
        }
        return true;
    }

    /**
     * 获取图片
     * @param fileName
     * @param imgUrl
     */
    public static void getImg(String fileName, String imgUrl){
        Map<String, String> map = new HashMap<String, String>();
        map.put("fileName", fileName);
        map.put("imgUrl", imgUrl);
        try{
            Message message = handler.obtainMessage();
            message.what = 1;
            message.obj = map;
            handler.sendMessage(message);

        }catch (Exception e){
            //保存图片必须在子线程中操作，是耗时操作
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        //建立消息循环的步骤
                        Looper.prepare();//1、初始化Looper
                        handler = new Handler(){//2、绑定handler到CustomThread实例的Looper对象
                            public void handleMessage (Message msg) {//3、定义处理消息的方法
                                switch(msg.what) {
                                    case 1:
                                        try{
                                            Map<String, String> map = (Map<String, String>) msg.obj;
                                            saveBitmap(map.get("imgUrl"), map.get("fileName"));
                                        }catch (Exception e){
                                            Log.e(TAG, "获取图片失败", e);
                                        }
                                }
                            }
                        };
                        Looper.loop();//4、启动消息循环
                    }catch (Exception e){
                        Log.e(TAG, "接收消息失败", e);
                    }
                }
            }).start();

            try {
                Thread.sleep(1000);
            }catch (Exception e2){
                Log.e(TAG, "延迟出错", e2);
            }

            Message message = handler.obtainMessage();
            message.what = 1;
            message.obj = map;
            handler.sendMessage(message);
        }
    }


    /**
     * 下载图片
     * @param imgUrl
     * @return
     */
    public static Bitmap downBitmap(String imgUrl){
        try{
            URL myFileUrl = new URL(imgUrl);
            HttpURLConnection conn;
            conn = (HttpURLConnection) myFileUrl.openConnection();
            conn.setDoInput(true);
            conn.connect();
            InputStream is = conn.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            return bitmap;
        }catch (Exception e){
            Log.e(TAG, "下载图片失败", e);
        }
        return null;
    }

    /**
     * 保存图片
     * @param imgUrl
     * @param fileName
     */
    public static void saveBitmap(String imgUrl, String fileName){
        try{
            Bitmap bitmap = downBitmap(imgUrl);
            if(bitmap == null){
                return;
            }
            //保存图片
            File appDir = new File(Environment.getExternalStorageDirectory(), Constants.IMAGE_URL);
            //判断路径是否存在，不存在新建文件夹
            if (!appDir.exists()) {
                appDir.mkdirs();
            }
            //写入图片
            File file = new File(appDir, fileName);
            FileOutputStream fos = new FileOutputStream(file);
            String suffix = "jpg";
            if(imgUrl.indexOf(".") > -1){
                suffix = imgUrl.substring(imgUrl.lastIndexOf(".") + 1);
            }
            //判断图片类型
            switch (suffix){
                case "png":
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, fos);
                    break;
                default:
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
                    break;
            }

            fos.flush();
            fos.close();
            //把文件插入到系统图库
            MediaStore.Images.Media.insertImage(MainActivity.mActivity.getContentResolver(),
                    file.getAbsolutePath(), fileName, null);
            // 通知图库更新
            Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            Uri uri = Uri.fromFile(file);
            intent.setData(uri);
            MainActivity.mActivity.sendBroadcast(intent);
        }catch (Exception e){
            Log.e(TAG, "保存图片失败", e);
        }
    }

    /**
     * 得到图片字节流数组大小
     * @param inStream
     * @return
     */
    public static byte[] readStream(InputStream inStream) throws Exception {
        ByteArrayOutputStream outStream = new ByteArrayOutputStream();
        byte[] buffer = new byte[1024];
        int len = 0;
        while ((len = inStream.read(buffer)) != -1) {
            outStream.write(buffer, 0, len);
        }
        outStream.close();
        inStream.close();
        return outStream.toByteArray();
    }

}
