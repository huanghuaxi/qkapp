package com.qk.manager.taskdata.impl;

import android.util.Log;

import com.qk.manager.impl.MomentsSendText;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.qkapp.MyAccessibilityService;

import java.util.Map;

/**
 * 朋友圈发送文字
 */
public class MomentsSendTextData implements TaskDataIntfService {

    private static final String TAG = MomentsSendTextData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            String msg = map.get("msg") == null ? "" : map.get("msg").toString();
            MomentsSendText.msg = msg;
        }catch (Exception e) {
            Log.e(TAG, "朋友圈发送文字数据处理失败", e);
        }
        return true;
    }
}
