package com.qk.manager.taskdata.impl;

import android.app.DownloadManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.qk.manager.impl.MomentsSendVideo;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.DOWNLOAD_SERVICE;

/**
 * 朋友圈发送视频
 */
public class MomentsSendVideoData implements TaskDataIntfService {

    private static final String TAG = MomentsSendVideoData.class.getName();

    public static Handler handler;

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            String msg = map.get("msg") == null ? "" : map.get("msg").toString();
            String videoUrl = map.get("videoUrl") == null ? "" : map.get("videoUrl").toString();
            String suffix = ".mp4";
            if(videoUrl.indexOf(".") > -1){
                suffix = videoUrl.substring(videoUrl.lastIndexOf("."));
            }
            String fileName = new Date().getTime() + suffix;
            getVideo(fileName, videoUrl);
            MomentsSendVideo.msg = msg;
            MomentsSendVideo.fileName = fileName;
            Thread.sleep(1000);
        }catch (Exception e){
            Log.e(TAG, "朋友圈发送视频数据处理失败", e);
        }
        return true;
    }

    /**
     * 获取视频
     * @param fileName
     * @param videoUrl
     */
    public static void getVideo(String fileName, String videoUrl){
        Map<String, String> map = new HashMap<String, String>();
        map.put("fileName", fileName);
        map.put("videoUrl", videoUrl);
        try{
            Message message = handler.obtainMessage();
            message.what = 1;
            message.obj = map;
            handler.sendMessage(message);

        }catch (Exception e){
            //保存图片必须在子线程中操作，是耗时操作
            new Thread(new Runnable() {
                @Override
                public void run() {
                    try{
                        //建立消息循环的步骤
                        Looper.prepare();//1、初始化Looper
                        handler = new Handler(){//2、绑定handler到CustomThread实例的Looper对象
                            public void handleMessage (Message msg) {//3、定义处理消息的方法
                                switch(msg.what) {
                                    case 1:
                                        try{
                                            Map<String, String> map = (Map<String, String>) msg.obj;
                                            downVide(map.get("videoUrl"), map.get("fileName"));
                                        }catch (Exception e){
                                            Log.e(TAG, "获取视频失败", e);
                                        }
                                }
                            }
                        };
                        Looper.loop();//4、启动消息循环
                    }catch (Exception e){
                        Log.e(TAG, "接收消息失败", e);
                    }
                }
            }).start();

            try {
                Thread.sleep(1000);
            }catch (Exception e2){
                Log.e(TAG, "延迟出错", e2);
            }

            Message message = handler.obtainMessage();
            message.what = 1;
            message.obj = map;
            handler.sendMessage(message);
        }
    }

    /**
     * 保存视频
     * @param videoUrl
     * @param fileName
     */
    public static void saveVideo(String videoUrl, String fileName){
        try{
            //下载视频
            Uri uri = Uri.parse(videoUrl);
            DownloadManager.Request request =new DownloadManager.Request(uri);
            // 通过setAllowedNetworkTypes方法可以设置允许在何种网络下下载
            request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI);
            request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);
            request.setVisibleInDownloadsUi(true) ;
            //下载到那个文件夹下，以及命名
            File saveFile = new File(Environment.getExternalStorageDirectory().getPath(), Constants.VIDEO_URL);
            //判断路径是否存在，不存在新建文件夹
            if (!saveFile.exists()) {
                saveFile.mkdirs();
            }
            File file = new File(saveFile, fileName);
            request.setDestinationUri(Uri.fromFile(file));
            DownloadManager downloadManager = (DownloadManager) MainActivity.mActivity.getSystemService(DOWNLOAD_SERVICE);
            downloadManager.enqueue(request);
        }catch (Exception e){
            Log.e(TAG, "保存视频失败", e);
        }
    }

    /**
     * 下载视频
     * @param videoUrl
     * @param fileName
     */
    public static void downVide(String videoUrl, String fileName){
        try{
            URL url = new URL(videoUrl);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setReadTimeout(10000);
            con.setConnectTimeout(10000);
            con.setRequestProperty("Charset", "UTF-8");
            con.setRequestMethod("GET");
            if (con.getResponseCode() == 200) {
                InputStream is = con.getInputStream();//获取输入流
                FileOutputStream fileOutputStream = null;//文件输出流
                File file = null;
                if (is != null) {
                    //下载到那个文件夹下，以及命名
                    File saveFile = new File(Environment.getExternalStorageDirectory().getPath(), Constants.VIDEO_URL);
                    //判断路径是否存在，不存在新建文件夹
                    if (!saveFile.exists()) {
                        saveFile.mkdirs();
                    }
                    file = new File(saveFile, fileName);
                    fileOutputStream = new FileOutputStream(file);//指定文件保存路径
                    byte[] buf = new byte[1024];
                    int ch;
                    while ((ch = is.read(buf)) != -1) {
                        fileOutputStream.write(buf, 0, ch);//将获取到的流写入文件中
                    }
                }
                if (fileOutputStream != null) {
                    fileOutputStream.flush();
                    fileOutputStream.close();
                }

                //通知系统库更新
                Intent intent = new Intent(Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
                Uri uri = Uri.fromFile(file);
                intent.setData(uri);
                MainActivity.mActivity.sendBroadcast(intent);

            }
        }catch (Exception e){
            Log.e(TAG, "下载视频失败", e);
        }
    }
}
