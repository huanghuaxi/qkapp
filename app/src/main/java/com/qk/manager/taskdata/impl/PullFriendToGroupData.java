package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.common.AppContext;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.impl.FriendList;
import com.qk.manager.impl.GroupList;
import com.qk.manager.impl.PullFriendToGroup;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 拉人进群数据处理
 */
public class PullFriendToGroupData implements TaskDataIntfService {

    private static final String TAG = PullFriendToGroupData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            //拉人进群
            Message message = MainActivity.mHandler.obtainMessage();
            //获取群列表
            if(GroupList.groupList != null){
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "获取群列表";
                MainActivity.mHandler.sendMessage(message);
                if(GroupList.groupList.size() <= 0){
                    Log.i(TAG, "获取群列表");
                    GroupList.groupList = new ArrayList<Map<String, String>>();
                    MyAccessibilityService.taskType = TaskTypeEnum.GROUP_LIST.getCode();
                    MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.GROUP_LIST.getCode());
                    MyAccessibilityService.isRun = 1;
                    return false;
                }
            }else{
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "没有群聊房间";
                MainActivity.mHandler.sendMessage(message);
                isFinish(map);
                return false;
            }
            /*Map<String, String> testMap = new HashMap<String, String>();
            testMap.put("account", "111");
            testMap.put("mobile", "16570902114");
            FriendList.friendList.add(testMap);
            testMap = new HashMap<String, String>();
            testMap.put("account", "222");
            testMap.put("mobile", "13860688304");
            FriendList.friendList.add(testMap);
            testMap = new HashMap<String, String>();
            testMap.put("account", "111");
            testMap.put("mobile", "16570902114");
            FriendList.friendList.add(testMap);*/

            //获取好友列表
            if(FriendList.friendList != null){
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "获取好友列表";
                MainActivity.mHandler.sendMessage(message);
                if(FriendList.friendList.size() <= 0){
                    Log.i(TAG, "获取好友列表");
                    FriendList.friendList = new ArrayList<Map<String, String>>();
                    MyAccessibilityService.taskType = TaskTypeEnum.FRIEND_LIST.getCode();
                    MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.FRIEND_LIST.getCode());
                    MyAccessibilityService.isRun = 1;
                    return false;
                }

            }else{
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "没有好友";
                MainActivity.mHandler.sendMessage(message);
                isFinish(map);
                return false;
            }

            //去重
            message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始拉人进群任务";
            MainActivity.mHandler.sendMessage(message);
            Log.i(TAG, "去重");
            String nextName = "";
            List<String> groupList = new ArrayList<String>();
            List<String> memberList = new ArrayList<String>();
            Map<String, String> memberMap = new HashMap<String, String>();

            for(Map<String, String> member : FriendList.friendList){
                if(!nextName.equals(member.get("account"))){
                    if("weixin".equals(member.get("account"))){
                        continue;
                    }
                    if(MainActivity.wechatInfo.get("wechatAccount").equals(member.get("account"))){
                        continue;
                    }
                    if(member.get("mobile") != null && !"".equals(member.get("mobile"))){
                        memberList.add(member.get("mobile"));
                        memberMap.put(member.get("mobile"), member.get("name"));
                    }else{
                        if(member.get("account") != null && member.get("account").length() > 5 && "wxid_".equals(member.get("account").substring(0, 5))){
                            memberList.add(member.get("name"));
                            memberMap.put(member.get("name"), member.get("name"));
                        }else{
                            memberList.add(member.get("account"));
                            memberMap.put(member.get("account"), member.get("name"));
                        }
                    }
                }
            }
            /*for(String memberName : memberList){
                outLog(memberName);
            }*/
            for(Map<String, String> group : GroupList.groupList){
                if(!nextName.equals(group.get("name"))){
//                    Log.i(TAG, "groupName:" + group.get("name"));
                    groupList.add(group.get("name"));
                }
            }
            //根据条件获取数据
            Log.i(TAG, "Map:" + map.toString());
            JSONObject json = new JSONObject( map.get("pullToGroupList").toString());
            String groupName = json.getString("groupName").trim();
            if(!"".equals(groupName)){
                String[] groupNames = groupName.split("\\\n");
                List<String> list = new ArrayList<String>();
                Log.i(TAG, "根据条件查询群");
                for(String name1 : groupNames){
                    Log.i(TAG, "group_name1:" + name1);
                    for(String name2 : groupList){
                        Log.i(TAG, "group_name2:" + name2);
                        if(name2.indexOf(name1) > -1){
                            list.add(name2);
                        }
                    }
                }
                groupList = list;
                Log.i(TAG, "群数量：" + groupList.size());
            }


            Integer type = json.get("type") == null ? 0 : json.getInt("type");
            Log.i(TAG, "好友类型：" + type);
            if(type == 1){
                String invite = json.get("invite") == null ? "" : json.getString("invite");
                String noInvite = json.get("noInvite") == null ? "" : json.getString("noInvite");
                Log.i(TAG, "包含好友：" + invite);
                Log.i(TAG, "不包含好友：" + noInvite);
                if(!"".equals(invite)){
                    Log.i(TAG, "根据包含条件查询好友");
                    String[] invites = invite.split("\\\n");
                    List<String> list = new ArrayList<String>();
                    for(String name1 : invites){
                        Log.i(TAG, "member_name1:" + name1);
                        for(String name2 : memberList){
                            Log.i(TAG, "member_name2:" + name2);
                            if(memberMap.get(name2) != null){
                                if(memberMap.get(name2).indexOf(name1) > -1){
                                    list.add(name2);
                                }
                            }
                        }
                    }
                    memberList = list;
                    Log.i(TAG, "好友数量：" + memberList.size());
                }
                if(!"".equals(noInvite)){
                    Log.i(TAG, "根据不包含条件查询好友");
                    String[] noInvites = noInvite.split("\\\n");
                    List<String> list = new ArrayList<String>();
                    for(String name1 : noInvites){
                        list = new ArrayList<String>();
                        Log.i(TAG, "member_name1:" + name1);
                        for(String name2 : memberList){
                            Log.i(TAG, "member_name2:" + name2);
                            if(memberMap.get(name2) != null){
                                if(memberMap.get(name2).indexOf(name1) == -1){
                                    list.add(name2);
                                }
                            }
                        }
                        memberList = list;
                    }
                    memberList = list;
                    Log.i(TAG, "好友数量：" + memberList.size());
                }
            }
            PullFriendToGroup.groupList = groupList;
            PullFriendToGroup.memberList = memberList;
            PullFriendToGroup.memNum = json.get("memberNum") == null ? (memberList.size() / groupList.size()) : json.getInt("memberNum");
        }catch (Exception e){
            Log.e(TAG, "存入拉人进群数据失败", e);
        }
        return true;
    }

    /**
     * 完成任务
     */
    private static void isFinish(Map<String, Object> map){
        try{
            String taskId = map.get("taskId").toString();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "拉人进群操作结束";
            MainActivity.mHandler.sendMessage(message);
            MyAccessibilityService.taskType = "";
            MyAccessibilityService.className = "";
            MyAccessibilityService.index = 0;
            MyAccessibilityService.taskList = null;
            GroupList.groupList = new ArrayList<Map<String, String>>();
            FriendList.friendList = new ArrayList<Map<String, String>>();
            String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "完成任务",
                    DataUtil.setfinishTask(Integer.parseInt(taskId), MainActivity.MOBILE_IMEI));
            MainActivity.myClient.send(msg);
            AppContext cnt = AppContext.getInstance();
            cnt.removeObj(0);
            MyAccessibilityService.taskId = 0;
            MyAccessibilityService.isRun = 0;
            Log.i(TAG, "后退到主页面");
            Thread.sleep(1000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.i(TAG, "拉人进群任务完成失败", e);
        }
    }
}
