package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.impl.AddFriendServiceImpl;
import com.qk.manager.impl.QQAddFriend;
import com.qk.manager.impl.QQLogin;
import com.qk.manager.impl.QQLoginCheck;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 添加QQ好友数据处理
 */
public class QQAddFriendData implements TaskDataIntfService {

    private static final String TAG = QQAddFriendData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 1;

            //验证QQ登录
            if(!QQLoginCheck.isCheckFinish){
                JSONObject accountPwd = QQFriendCheckData.getAccountInfo(map);
                QQLogin.accountMap = accountPwd;
                QQLoginCheck.accountMap = accountPwd;
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "开始检查QQ是否登录";
                MainActivity.mHandler.sendMessage(message);
                MyAccessibilityService.taskType = TaskTypeEnum.QQ_LOGIN_CHECK.getCode();
                MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_LOGIN_CHECK.getCode());
                MyAccessibilityService.isRun = 1;
                return false;
            }
            //添加好友
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始添加QQ好友任务";
            MainActivity.mHandler.sendMessage(message);
           /* List<Map<String, String>> list = (List<Map<String, String>>)map.get("friendList");
            QQAddFriend.friendList = new ArrayList<JSONObject>();
            for(Map<String, String> friend : list){
                QQAddFriend.friendList.add(new JSONObject(friend));
            }*/
            JSONArray json = new JSONArray( map.get("friendList").toString());
            QQAddFriend.friendList = DataUtil.jsonToList(json);
        }catch (Exception e){
            Log.e(TAG, "存入添加QQ好友数据失败", e);
        }
        return true;
    }
}
