package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.impl.QQFriendCheck;
import com.qk.manager.impl.QQLogin;
import com.qk.manager.impl.QQLoginCheck;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.AESUtils;
import com.qk.manager.util.Constants;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 * QQ好友检查数据处理
 */
public class QQFriendCheckData implements TaskDataIntfService {

    private static final String TAG = QQFriendCheckData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 1;

            //验证QQ登录
            if(!QQLoginCheck.isCheckFinish){
                JSONObject accountPwd = getAccountInfo(map);
                QQLogin.accountMap = accountPwd;
                QQLoginCheck.accountMap = accountPwd;
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "开始检查QQ是否登录";
                MainActivity.mHandler.sendMessage(message);
                MyAccessibilityService.taskType = TaskTypeEnum.QQ_LOGIN_CHECK.getCode();
                MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_LOGIN_CHECK.getCode());
                MyAccessibilityService.isRun = 1;
                return false;
            }

            JSONObject jsonObject = (JSONObject) map.get("account");
            Map<String, JSONArray> accountMap = new HashMap<String, JSONArray>();
            List<String> qqAccount = new ArrayList<String>();
            Iterator it = jsonObject.keys();
            // 遍历jsonObject数据，添加到Map对象
            while (it.hasNext()) {
                String key = String.valueOf(it.next());
                JSONArray value = (JSONArray) jsonObject.optJSONArray(key);
                accountMap.put(key, value);
                qqAccount.add(key);
            }
            String msg = map.get("msg").toString();
            String addFriendId = map.get("addFriendId").toString();
            List<String> msgList = Arrays.asList(msg.split("\\\n\\\n"));
            QQFriendCheck.msgList = msgList;
            QQFriendCheck.addFriendId = Integer.parseInt(addFriendId);
            QQFriendCheck.qqAccount = qqAccount;
            QQFriendCheck.qqIndex = 0;
            QQFriendCheck.friendIndex = 0;
            QQFriendCheck.accountMap = accountMap;
        }catch (Exception e){
            Log.e(TAG, "QQ好友检查数据处理失败", e);
        }
        return true;
    }

    /**
     * 获取QQ账号信息
     * @param map
     * @return
     * @throws Exception
     */
    public static JSONObject getAccountInfo(Map<String, Object> map) throws Exception{
        JSONObject accountPwd = (JSONObject) map.get("qqTargetData");
        JSONObject accountJSON = new JSONObject();
        Iterator it = accountPwd.keys();
        String key = new String();
        String password = new String();
        while(it.hasNext()){
            key = it.next().toString();
            password = accountPwd.getString(key);
//            password = AESUtils.decrypt(password, Constants.SECURITY_KEY);
            accountJSON.put(key, password);
        }
        return accountJSON;
    }

}
