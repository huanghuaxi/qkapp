package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.impl.QQFriendList;
import com.qk.manager.impl.QQGroupList;
import com.qk.manager.impl.QQLogin;
import com.qk.manager.impl.QQLoginCheck;
import com.qk.manager.impl.QQPullFriendToGroup;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * QQ拉人进群数据处理
 */
public class QQPullFriendToGroupData implements TaskDataIntfService {

    private static final String TAG = QQPullFriendToGroupData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 1;
            Message message = MainActivity.mHandler.obtainMessage();

            //验证QQ登录
            if(!QQLoginCheck.isCheckFinish){
                JSONObject accountPwd = QQFriendCheckData.getAccountInfo(map);
                QQLogin.accountMap = accountPwd;
                QQLoginCheck.accountMap = accountPwd;
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "开始检查QQ是否登录";
                MainActivity.mHandler.sendMessage(message);
                MyAccessibilityService.taskType = TaskTypeEnum.QQ_LOGIN_CHECK.getCode();
                MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_LOGIN_CHECK.getCode());
                MyAccessibilityService.isRun = 1;
                return false;
            }

            //获取群列表
            if(QQGroupList.groupList != null){
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "获取QQ群列表";
                MainActivity.mHandler.sendMessage(message);
                if(QQGroupList.groupList.size() <= 0){
                    Log.i(TAG, "获取QQ群列表");
                    QQGroupList.groupList = new ArrayList<String>();
                    MyAccessibilityService.taskType = TaskTypeEnum.QQ_GROUP_LIST.getCode();
                    MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_GROUP_LIST.getCode());
                    MyAccessibilityService.isRun = 1;
                    return false;
                }
            }else{
                Log.i(TAG, "重新获取QQ群列表");
                QQGroupList.groupList = new ArrayList<String>();
                MyAccessibilityService.taskType = TaskTypeEnum.QQ_GROUP_LIST.getCode();
                MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_GROUP_LIST.getCode());
                MyAccessibilityService.isRun = 1;
                return false;
            }

            //获取好友列表
            if(QQFriendList.friendList != null){
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "获取QQ好友列表";
                MainActivity.mHandler.sendMessage(message);
                if(QQFriendList.friendList.size() <= 0){
                    Log.i(TAG, "获取QQ好友列表");
                    QQFriendList.friendList = new ArrayList<Map<String, String>>();
                    MyAccessibilityService.taskType = TaskTypeEnum.QQ_FRIEND_LIST.getCode();
                    MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_FRIEND_LIST.getCode());
                    MyAccessibilityService.isRun = 1;
                    return false;
                }

            }else{
                Log.i(TAG, "重新获取QQ群列表");
                QQGroupList.groupList = new ArrayList<String>();
                MyAccessibilityService.taskType = TaskTypeEnum.QQ_GROUP_LIST.getCode();
                MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_GROUP_LIST.getCode());
                MyAccessibilityService.isRun = 1;
                return false;
            }

            /*QQGroupList.groupList.add("20190924");
            QQGroupList.groupList.add("20190808");
            Map<String, String> fMap = new HashMap<String, String>();
            fMap.put("name", "20190801");
            fMap.put("qq", "2648575348");
            QQFriendList.friendList.add(fMap);
            fMap = new HashMap<String, String>();
            fMap.put("name", "20190802");
            fMap.put("qq", "2386315850");
            QQFriendList.friendList.add(fMap);
            fMap = new HashMap<String, String>();
            fMap.put("name", "20190901");
            fMap.put("qq", "2468167887");
            QQFriendList.friendList.add(fMap);
            fMap = new HashMap<String, String>();
            fMap.put("name", "20190902");
            fMap.put("qq", "2474158696");
            QQFriendList.friendList.add(fMap);
            fMap = new HashMap<String, String>();
            fMap.put("name", "1111");
            fMap.put("qq", "3502582578");
            QQFriendList.friendList.add(fMap);
            fMap = new HashMap<String, String>();
            fMap.put("name", "222");
            fMap.put("qq", "66600000");
            QQFriendList.friendList.add(fMap);*/

            for(Map<String, String> friendMap : QQFriendList.friendList){
                Log.i(TAG, "QQ:" + friendMap.get("qq"));
                Log.i(TAG, "name:" + friendMap.get("name"));
            }

            //拉人进群
            message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始QQ拉人进群任务";
            MainActivity.mHandler.sendMessage(message);

            //去除未命名的群
            List<String> groupList = new ArrayList<String>();
            for(String name : QQGroupList.groupList){
                if(name.indexOf("、") < 0){
                    groupList.add(name);
                }
                Log.i(TAG, "groupName:" + name);
            }

            //根据条件获取群
            JSONObject json = new JSONObject( map.get("pullToGroupList").toString());
            String groupName = json.getString("groupName").trim();
            if(!"".equals(groupName)){
                String[] groupNames = groupName.split("\\\n");
                List<String> list = new ArrayList<String>();
                Log.i(TAG, "根据条件查询群");
                List<String> groups = new ArrayList<String>();
                for(String name : groupList){
                    for(String gName : groupNames){
                        if(name.indexOf(gName) > -1){
                            groups.add(name);
                            break;
                        }
                    }
                }
                groupList = groups;
            }

            //根据条件获取好友
            Integer type = json.get("type") == null ? 0 : json.getInt("type");
            Log.i(TAG, "好友类型：" + type);
            List<String> friendList = new ArrayList<String>();
            if(type == 1){
                String invite = json.get("invite") == null ? "" : json.getString("invite");
                String noInvite = json.get("noInvite") == null ? "" : json.getString("noInvite");
                Log.i(TAG, "包含好友：" + invite);
                Log.i(TAG, "不包含好友：" + noInvite);
                if(!"".equals(invite)){
                    //包含
                    Log.i(TAG, "根据包含条件查询好友");
                    String[] invites = invite.split("\\\n");
                    List<String> list = new ArrayList<String>();
                    for(Map<String, String> friendMap : QQFriendList.friendList){
                        for(String name : invites){
                            if(friendMap.get("name").indexOf(name) > -1){
                                list.add(friendMap.get("qq"));
                                break;
                            }
                        }
                    }
                    friendList = list;
                }else{
                    //全部好友
                    for(Map<String, String> friendMap : QQFriendList.friendList){
                        friendList.add(friendMap.get("qq"));
                    }
                }

                if(!"".equals(noInvite)){
                    //不包含
                    Log.i(TAG, "根据包含条件查询好友");
                    String[] noInvites = noInvite.split("\\\n");
                    List<String> list = new ArrayList<String>();
                    for(Map<String, String> friendMap : QQFriendList.friendList){
                        for(String name : noInvites){
                            if(friendMap.get("name").indexOf(name) > -1){
                                list.add(friendMap.get("qq"));
                                break;
                            }
                        }
                    }
                    if(list.size() > 0){
                        List<String> friends = new ArrayList<String>();
                        for(String qq : friendList){
                            boolean isAdd = true;
                            for(String qq2 : list){
                                if(qq.equals(qq2)){
                                    isAdd = false;
                                    break;
                                }
                            }
                            if(isAdd){
                                friends.add(qq);
                            }
                        }
                        friendList = friends;
                    }

                }
            }else{
                //全部好友
                for(Map<String, String> friendMap : QQFriendList.friendList){
                    friendList.add(friendMap.get("qq"));
                }
            }

            //去重
            List<String> nameList = new ArrayList<String>(groupList);
            for(int j = 0;j < groupList.size();j++){
                int count = 0;
                for(int i = 0;i < nameList.size();i++){
                    if(nameList.get(i).equals(groupList.get(j))){
                        count++;
                        if(count > 1){
                            nameList.remove(i);
                            j--;
                            break;
                        }
                    }

                }
            }
            nameList = new ArrayList<String>(friendList);
            for(int j = 0;j < friendList.size();j++){
                int count = 0;
                for(int i = 0;i < nameList.size();i++){
                    if(nameList.get(i).equals(friendList.get(j))){
                        count++;
                        if(count > 1){
                            nameList.remove(i);
                            j--;
                            break;
                        }
                    }

                }
            }

            Log.i(TAG, "-------结果-------");
            for(String name : groupList){
                Log.i(TAG, "goupName:" + name);
            }

            for(String name : friendList){
                Log.i(TAG, "friendName:" + name);
            }

            QQPullFriendToGroup.groupList = groupList;
            QQPullFriendToGroup.memberList = friendList;
            QQPullFriendToGroup.memNum = json.get("memberNum") == null ? (friendList.size() / groupList.size()) : json.getInt("memberNum");

        }catch (Exception e){
            Log.e(TAG, "QQ拉好友进群数据处理失败", e);
        }
        return true;
    }

    /**
     * 完成任务
     */
    private static void isFinish(Map<String, Object> map){
        try{
            QQFriendList.friendList = new ArrayList<Map<String, String>>();
            QQGroupList.groupList = new ArrayList<String>();
            //切换账号重新开始拉人进群
            MyAccessibilityService.isRun = 0;
            MyAccessibilityService.taskType = "";
            MyAccessibilityService.className = "";
            Log.i(TAG, "后退到主页面");
            Thread.sleep(1000);
            MainActivity.openMyApp();
        }catch (Exception e){
            Log.i(TAG, "拉人进群任务完成失败", e);
        }
    }
}
