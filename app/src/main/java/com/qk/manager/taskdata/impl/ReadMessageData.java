package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.intf.TaskDataIntfService;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.Map;

/**
 * 阅读消息数据处理
 */
public class ReadMessageData implements TaskDataIntfService {

    private static final String TAG = ReadMessageData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try {
            MyAccessibilityService.opType = 0;
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始阅读消息任务";
            MainActivity.mHandler.sendMessage(message);
        }catch (Exception e){
            Log.e(TAG, "存入阅读消息信息失败", e);
        }
        return true;
    }
}
