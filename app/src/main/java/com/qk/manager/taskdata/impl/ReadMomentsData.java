package com.qk.manager.taskdata.impl;

import android.util.Log;

import com.qk.manager.impl.ReadMoments;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.qkapp.MyAccessibilityService;

import java.util.Map;

/**
 * 阅读朋友圈数据处理类
 */
public class ReadMomentsData implements TaskDataIntfService {

    private static final String TAG = ReadMomentsData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try {
            MyAccessibilityService.opType = 0;
            //点赞机率
            Integer goodRatio = Integer.parseInt(map.get("goodRatio").toString());
            //阅读时间
            Integer minTime = Integer.parseInt(map.get("minTime").toString());
            Integer maxTime = Integer.parseInt(map.get("maxTime").toString());
            ReadMoments.goodRatio = goodRatio;
            ReadMoments.minTime = minTime;
            ReadMoments.maxTime = maxTime;

        }catch (Exception e){
            Log.e(TAG, "阅读朋友圈数据处理失败", e);
        }
        return true;
    }
}
