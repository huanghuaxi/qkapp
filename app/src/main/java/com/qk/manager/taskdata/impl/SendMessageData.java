package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.impl.SendMessage;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;

import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * 发送消息数据处理
 */
public class SendMessageData implements TaskDataIntfService {

    private static final String TAG = SendMessageData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始开始发送消息任务";
            MainActivity.mHandler.sendMessage(message);
            //好友账号
            List<String> friendList = DataUtil.jsonToList2((JSONArray) map.get("target"));
            //消息
            List<String> msgList = DataUtil.jsonToList2((JSONArray) map.get("sendMsg"));
            Integer maxMsgNum = Integer.parseInt(map.get("msgNum").toString());
            Integer minTime = Integer.parseInt(map.get("minTime").toString());
            Integer maxTime = Integer.parseInt(map.get("maxTime").toString());
            Random random = new Random();
            SendMessage.friendList = friendList;
            SendMessage.msgNum = random.nextInt(maxMsgNum) + 1;
            SendMessage.msgIndex = 0;
            SendMessage.minTime = minTime;
            SendMessage.maxTime = maxTime;
            SendMessage.msgList = msgList;
        }catch (Exception e) {
            Log.e(TAG, "发送消息数据处理失败", e);
        }
        return true;
    }
}
