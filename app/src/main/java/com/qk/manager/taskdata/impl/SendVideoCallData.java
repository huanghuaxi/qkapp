package com.qk.manager.taskdata.impl;

import android.os.Message;
import android.util.Log;

import com.qk.manager.impl.SendVideoCall;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import java.util.Map;

/**
 * 发送视频通话数据处理
 */
public class SendVideoCallData implements TaskDataIntfService {


    private static final String TAG = SendVideoCallData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开始发送视频通话任务";
            MainActivity.mHandler.sendMessage(message);
            String friend = map.get("targetAcc") == null ? "" : map.get("targetAcc").toString();
            Integer callTime =  Integer.parseInt(map.get("callTime").toString());
            SendVideoCall.callTime = callTime;
            SendVideoCall.friend = friend;
        }catch (Exception e){
            Log.e(TAG, "发送视频通话数据处理失败", e);
        }
        return true;
    }
}
