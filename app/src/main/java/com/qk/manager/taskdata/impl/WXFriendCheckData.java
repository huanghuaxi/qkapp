package com.qk.manager.taskdata.impl;

import android.util.Log;

import com.qk.manager.impl.WXFriendCheck;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * 微信好友检查数据处理
 */
public class WXFriendCheckData implements TaskDataIntfService {

    private static final String TAG = WXFriendCheckData.class.getName();

    @Override
    public boolean handler(Object obj, Map<String, Object> map) {
        try{
            MyAccessibilityService.opType = 0;

            JSONArray account = (JSONArray) map.get("account");

            String msg = map.get("msg").toString();
            String addFriendId = map.get("addFriendId").toString();
            List<String> msgList = Arrays.asList(msg.split("\\\n\\\n"));
            WXFriendCheck.msgList = msgList;
            WXFriendCheck.addFriendId = Integer.parseInt(addFriendId);
            WXFriendCheck.friendIndex = 0;
            WXFriendCheck.accountMap = account;
        }catch (Exception e){
            Log.e(TAG, "微信好友检查数据处理失败", e);
        }
        return true;
    }
}
