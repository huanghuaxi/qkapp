package com.qk.manager.util;

import android.accessibilityservice.GestureDescription;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.Rect;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import java.util.ArrayList;
import java.util.List;

/**
 *  微信无障碍操作
 */
public class AccessibilityWechatUtil {

    private static final String TAG = AccessibilityWechatUtil.class.getName();

    /**
     * 点击微信底部菜单栏
     * @param rootNodeInfo
     * @param index
     * @return
     */
    public static GestureDescription clickBottomMenuBtn(AccessibilityNodeInfo rootNodeInfo, Integer index){
        try{
            //1: 微信 2：通讯录 3：发现 4：我
            List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, Constants.MAIN_PAGE_NAME);
            if(pageCounts.size() > 0) {
                AccessibilityNodeInfo relativeCounts = pageCounts.get(0).getParent();
                if(relativeCounts != null){
                    int num = 0;
                    for(int i = 0;i < relativeCounts.getChildCount();i++){
                        if(relativeCounts.getChild(i).getChildCount() > 0){
                            if("android.widget.RelativeLayout".equals(relativeCounts.getChild(i).getClassName())){
                                num = i;
                                break;
                            }
                        }
                    }
                    AccessibilityNodeInfo btnCounts = relativeCounts.getChild(num).getChild(relativeCounts.getChild(num).getChildCount() - 1);
                    if(btnCounts.getText() != null && !"微信".equals(btnCounts.getText().toString())){
                        return null;
                    }
                    //点击微信标签按钮
                    Thread.sleep(1000);
                    Rect rect = new Rect();
                    Rect rect2 = new Rect();
                    relativeCounts.getChild(1).getBoundsInScreen(rect);
                    relativeCounts.getBoundsInScreen(rect2);
                    int width = rect2.width() / 4;
                    Point position = new Point((int)(width * index - width / 2), rect.top + (int)(rect.height() / 2));
                    GestureDescription.Builder builder = new GestureDescription.Builder();
                    Path p = new Path();
                    p.moveTo(position.x, position.y);
                    builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                    GestureDescription gesture = builder.build();
                    return gesture;
//                    relativeCounts.getChild(2).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击微信底部菜单" + index + "失败", e);
        }
        return null;
    }

    /**
     * 双击微信底部菜单栏
     * @param rootNodeInfo
     * @param index
     * @return
     */
    public static GestureDescription doubleClickBottomMenuBtn(AccessibilityNodeInfo rootNodeInfo, Integer index){
        try{
            //1: 微信 2：通讯录 3：发现 4：我
            List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(pageCounts, rootNodeInfo, Constants.MAIN_PAGE_NAME);
            if(pageCounts.size() > 0) {
                AccessibilityNodeInfo relativeCounts = pageCounts.get(0).getParent();
                if(relativeCounts != null){
                    int num = 0;
                    for(int i = 0;i < relativeCounts.getChildCount();i++){
                        if(relativeCounts.getChild(i).getChildCount() > 0){
                            if("android.widget.RelativeLayout".equals(relativeCounts.getChild(i).getClassName())){
                                num = i;
                                break;
                            }
                        }
                    }
                    AccessibilityNodeInfo btnCounts = relativeCounts.getChild(num).getChild(relativeCounts.getChild(num).getChildCount() - 1);
                    if(btnCounts.getText() != null && !"微信".equals(btnCounts.getText().toString())){
                        return null;
                    }
                    //点击微信标签按钮
                    Thread.sleep(1000);
                    Rect rect = new Rect();
                    relativeCounts.getChild(1).getBoundsInScreen(rect);
                    Point position = new Point(rect.left + (int)(rect.width() * index - rect.width() / 2), rect.top + (int)(rect.height() / 2));
                    GestureDescription.Builder builder = new GestureDescription.Builder();
                    Path p = new Path();
                    p.moveTo(position.x, position.y);
                    builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                    builder.addStroke(new GestureDescription.StrokeDescription(p, 300L, 100L));
                    GestureDescription gesture = builder.build();
                    return gesture;
//                    relativeCounts.getChild(2).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "点击微信底部菜单" + index + "失败", e);
        }
        return null;
    }

    /**
     * 点击QQ底部菜单栏
     * @param rootNodeInfo
     * @param index
     * @return
     */
    public static GestureDescription clickQQTab(AccessibilityNodeInfo rootNodeInfo, Integer index){
        try{
            //1: 消息 2：联系人 3：看点 4：动态
            List<AccessibilityNodeInfo> tabCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("android:id/tabs");
            Log.i(TAG, "tab数量：" + tabCounts.size());
            if(tabCounts.size() > 0){
                //点击QQ底部按钮
                Rect rect = new Rect();
                tabCounts.get(0).getBoundsInScreen(rect);
                if(rect.height() == 0 || rect.width() == 0){
                    return null;
                }
                int width = rect.width() / 4;
                Point position = new Point(rect.left + (int)(width * index - width / 2), rect.top + (int)(rect.height() / 2));
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                GestureDescription gesture = builder.build();
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击QQ底部菜单" + index + "失败", e);
        }
        return null;
    }

    /**
     * 双击QQ底部菜单栏
     * @param rootNodeInfo
     * @param index
     * @return
     */
    public static GestureDescription doubleClickQQTab(AccessibilityNodeInfo rootNodeInfo, Integer index){
        try{
            //1: 消息 2：联系人 3：看点 4：动态
            List<AccessibilityNodeInfo> tabCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("android:id/tabs");
            Log.i(TAG, "tab数量：" + tabCounts.size());
            if(tabCounts.size() > 0){
                //点击QQ底部按钮
                Rect rect = new Rect();
                tabCounts.get(0).getBoundsInScreen(rect);
                if(rect.height() == 0 || rect.width() == 0){
                    return null;
                }
                int width = rect.width() / 4;
                Point position = new Point(rect.left + (int)(width * index - width / 2), rect.top + (int)(rect.height() / 2));
                GestureDescription.Builder builder = new GestureDescription.Builder();
                Path p = new Path();
                p.moveTo(position.x, position.y);
                builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
                builder.addStroke(new GestureDescription.StrokeDescription(p, 300L, 100L));
                GestureDescription gesture = builder.build();
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击QQ底部菜单" + index + "失败", e);
        }
        return null;
    }

    /**
     * 点击屏幕
     * @param info
     */
    public static GestureDescription clickCoordinate(AccessibilityNodeInfo info){
        try{
            Rect rect = new Rect();
            info.getBoundsInScreen(rect);
            Log.d(TAG,"top:" + rect.top + "\tleft:" + rect.left + "\twidth:" + rect.width() + "\theight:" + rect.height());
            if(rect.height() <= 0 || rect.width() <= 0){
                return null;
            }
            Point position = new Point(rect.left + (int)(rect.width() / 2), rect.top + (int)(rect.height() / 2));
            GestureDescription.Builder builder = new GestureDescription.Builder();
            Path p = new Path();
            p.moveTo(position.x, position.y);
            builder.addStroke(new GestureDescription.StrokeDescription(p, 0L, 100L));
            GestureDescription gesture = builder.build();
            return gesture;
        }catch (Exception e){
            Log.e(TAG, "点击屏幕失败");
            e.printStackTrace();
        }
        return null;
    }
    /**
     * 滑动屏幕
     * @param x1
     * @param y1
     * @param x2
     * @param y2
     * @return
     */
    public static GestureDescription slitherCoordinate(int x1, int y1, int x2, int y2){
        try{
            GestureDescription.Builder builder = new GestureDescription.Builder();
            Path p = new Path();
            p.moveTo(x1, y1);
            p.lineTo(x2, y2);
            builder.addStroke(new GestureDescription.StrokeDescription(p, 1000L, 300L));
            GestureDescription gesture = builder.build();
            return gesture;
        }catch (Exception e){
            Log.e(TAG, "滑动屏幕失败");
            e.printStackTrace();
        }
        return null;
    }


    /**
     * 递归获取组件
     * @param infos
     * @param node
     * @param className
     */
    public static void recycleFindView(List<AccessibilityNodeInfo> infos, AccessibilityNodeInfo node, String className) {
        //edittext下面必定没有子元素，所以放在此时判断
        if(node == null){
            return;
        }
        if(node.getClassName() != null){
            if(className.equals(node.getClassName().toString())){
                Log.d(TAG, "text:" + node.getText());
                infos.add(node);
            }
        }
        if (node.getChildCount() > 0) {
            for (int i = 0; i < node.getChildCount(); i++) {
                recycleFindView(infos, node.getChild(i), className);
            }
        }
    }

    public static void recycleFind(AccessibilityNodeInfo node, AccessibilityNodeInfo info, String name) throws Exception{
        //edittext下面必定没有子元素，所以放在此时判断
        Log.d(TAG, "type:" + node.getClassName());
        Log.d(TAG, "text:" + node.getText());
        Log.d(TAG, "click:" + node.isClickable());
//        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);
        String text = node.getText() != null ? node.getText().toString() : "";
        text = new String(text.getBytes("ISO-8859-1"), "UTF-8");
        name = new String(name.getBytes("ISO-8859-1"), "UTF-8");
        if(name.equals(text)){
            info = node;
            return ;
        }

        if (node.getChildCount() > 0) {
            for (int i = 0; i < node.getChildCount(); i++) {
                recycleCheck(node.getChild(i));
            }
        }
    }


    /**
     * 递归查看组件
     * @param node
     */
    public static void recycleCheck(AccessibilityNodeInfo node) {
        //edittext下面必定没有子元素，所以放在此时判断
        Log.d(TAG, "type:" + node.getClassName());
        Log.d(TAG, "text:" + node.getText());
        Log.d(TAG, "click:" + node.isClickable());
        Log.d(TAG, "id:" + node.getViewIdResourceName());
//        node.performAction(AccessibilityNodeInfo.ACTION_CLICK);

        if (node.getChildCount() == 0) {

        } else {
            for (int i = 0; i < node.getChildCount(); i++) {
                recycleCheck(node.getChild(i));
            }
        }
    }

}
