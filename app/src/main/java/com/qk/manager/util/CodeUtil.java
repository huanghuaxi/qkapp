package com.qk.manager.util;

/**
 * 编码常量类
 */
public class CodeUtil {

    //成功
    public static final String SUCCESS_CODE = "000000";
    //失败
    public static final String FAIL_CODE = "999999";
    //websocket保持连接
    public static final String WEBSOCKET_ONLINE_CODE = "111111";

}
