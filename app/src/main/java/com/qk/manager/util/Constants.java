package com.qk.manager.util;

/**
 *  常量类
 */
public interface Constants {

    public static final String MORE_BTN_NAME = "更多";

    public static final String LOGIN_OTHER_BTN_NAME = "登录其他账号";

    public static final String MOBILE_TEXT_NAME = "手机号";

    public static final String NEXT_BTN_NAME = "下一步";

    public static final String PASSWORD_TEXT_NAME = "密码";

    public static final String LOGIN_BTN_NAME = "登录";

    public static final String MOBILE_INPUT_NAME = "请填写手机号";

    public static final String PASSWORD_INPUT_NAME = "请填写密码";

    public static final String WECHAT_BTN_NAME = "微信";

    public static final String ADD_FRIEND_NAME = "添加朋友";

    public static final String SHEARCH_FEIEND_NAME = "微信号/手机号";

    public static final String SHEARCH_ACCOUNT_NAME = "搜索:";

    public static final String BACK_BTN_NAME = "返回";

    public static final String MINE_BTN_NAME = "我";

    public static final String ADDR_LIST_BTN_NAME = "通讯录";

    public static final String GROUP_CHAT_BTN_NAME = "群聊";

    public static final String CHAT_INFO_BTN_NAME = "聊天信息";

    public static final String ADD_MEMBER_BTN_NAME = "添加成员";

    public static final String SEARCH_MEMBER_NAME = "搜索";

    public static final String LINKMAN_TEXT_NAME = "联系人";

    public static final String WECHAT_NUM_TEXT_NAME = "微信号:";

    public static final String NOT_FIND_FRIEND_TEXT_NAME = "该用户不存在";

    public static final String SEND_MSG_BTN_NAME = "发消息";

    public static final String DAILY_USE_TEXT_NAME = "最常使用";

    public static final String SEND_BTN_NAME = "发送";

    public static final String CHANGE_BTN_NAME = "切换";

    public static final String VIDEO_CALL_BTN_NAME = "视频通话";

    public static final String VOICE_CALL_BTN_NAME = "语音通话";

    public static final String ANSWER_BTN_NAME = "接听";

    public static final String HANG_UP_BTN_NAME = "挂断";

    public static final String FIND_BTN_NAME = "发现";

    public static final String MOMENTS_BTN_NAME = "朋友圈";

    public static final String DISCUSS_BTN_NAME = "评论";

    public static final String GOOD_BTN_NAME = "赞";

    public static final String PHOTO_SHARE_BTN_NAME = "拍照分享";

    public static final String I_SEE_BTN_NAME = "我知道了";

    public static final String PUBLISH_BTN_NAME = "发表";

    public static final String  QUIT_BTN_NAME = "退出";

    public static final String NOT_RETAIN_BTN_NAME = "不保留";

    public static final String CONFIRM_BTN_NAME = "确定";

    public static final String SEARCH_MOBILE_QQ_BTN_NAME = "查找手机/QQ号";

    public static final String SEARCH_WX_ACCOUNT_BTN_NAME = "查找微信号";

    public static final String FLOW_TEXT_NAME = "流量";

    public static final String ADD_BTN_NAME = "添加";

    public static final String SETTING_BTN_NAME = "设置";

    public static final String REMOVE_BTN_NAME = "删除";

    public static final String CANCEL_BTN_NAME = "取消";

    public static final String MAIN_VIEW_CLASS_NAME = "com.tencent.mm.ui.LauncherUI";

    public static final String MAIN_PAGE_NAME = "com.tencent.mm.ui.mogic.WxViewPager";

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1000;
    //图片保存地址
    public static final String IMAGE_URL = "/qukong/img/";
    //视频保存地址
    public static final String VIDEO_URL = "/qukong/video/";

    //安全密匙
    public static final String SECURITY_KEY = "123QWEQ";

}
