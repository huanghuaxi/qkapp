package com.qk.manager.util;

import android.os.Message;
import android.util.Log;

import com.qk.manager.common.AppContext;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

/**
 *  数据处理类
 */
public class DataUtil {

    private static final String TAG = MyAccessibilityService.class.getName();

    /**
     * 封装数据并转成字符串
     * @param code
     * @param msg
     * @param obj
     * @return
     */
    public static String setData(String code, String msg, Object obj){
        JSONObject jsonObject = new JSONObject();
        try{
            jsonObject.put("code", code);
            jsonObject.put("msg", msg);
            if(obj != null){
                JSONObject paramsObj = new JSONObject((Map<String, Object>)obj);
                jsonObject.put("data", paramsObj);
            }
        }catch (Exception e){
            Log.e(TAG, "数据封装失败", e);
        }
        return jsonObject.toString();
    }

    /**
     * 解析数据
     * @param str
     */
    public static void getData(String str){
        try{
            if("".equals(str)){
                return;
            }
            JSONObject jsonObject = new JSONObject(str);
            String code = jsonObject.getString("code");
            String msg  = jsonObject.getString("msg");
            //是否为任务，0：否 1：是
            Integer isTask = jsonObject.getInt("isTask");
            //是否允许重复存入任务 0：否 1：是
            Integer isAllowRepeat = jsonObject.getInt("isAllowRepeat");
            if(jsonObject.isNull("data")){
                return ;
            }
            JSONObject data = jsonObject.getJSONObject("data");
            if(CodeUtil.SUCCESS_CODE.equals(code)){
                if(isTask == 1){
                    AppContext cnt = AppContext.getInstance();
                    //数据获取成功,把data数据转成map
                    Log.i(TAG, "json:" + data.toString());
                    Map<String, Object> valueMap = jsonToMap(data);
                    valueMap.put("isAllowRepeat", isAllowRepeat);
                    Log.i(TAG, "map:" + valueMap.toString());
                    //判断当前养号任务是否允许重复
                    if(isAllowRepeat == 0){
                        //判断任务列表中是否存不允许重复的未执行任务任务
                        List<Map<String, Object>> taskList = cnt.getObj();
                        if(taskList != null && taskList.size() > 1){
                            for(int i = 1;i < taskList.size();i++){
                                if("0".equals(taskList.get(i).get("isAllowRepeat").toString())){
                                    //不存入该任务
                                    Message message = MainActivity.mHandler.obtainMessage();
                                    message.what = 100;
                                    message.obj = "因还有养号任务未执行，不接收该条任务";
                                    message.obj += ",任务ID：" + valueMap.get("taskId");
                                    message.obj += "，任务名称：" + TaskTypeEnum.getTaskNameByCode(valueMap.get("taskType").toString());
                                    MainActivity.mHandler.sendMessage(message);

                                    //开始任务
                                    MainActivity.startTask();
                                    return;
                                }
                            }
                        }
                    }

                    //判断添加好友任务是否重复
                    if(valueMap.get("taskType").toString().equals(TaskTypeEnum.ADD_FRIEND.getCode())){
                        List<Map<String, Object>> taskList = cnt.getObj();
                        if(taskList != null && taskList.size() > 0){
                            for(Map<String, Object> task : taskList){
                                if(task.get("taskType").toString().equals(TaskTypeEnum.ADD_FRIEND.getCode())){
                                    //已有添加好友任务存在，不存入该条任务
                                    Message message = MainActivity.mHandler.obtainMessage();
                                    message.what = 100;
                                    message.obj = "因还有添加好友任务未执行，不接收该条任务";
                                    message.obj += ",任务ID：" + valueMap.get("taskId");
                                    message.obj += "，任务名称：" + TaskTypeEnum.getTaskNameByCode(valueMap.get("taskType").toString());
                                    MainActivity.mHandler.sendMessage(message);

                                    //开始任务
                                    MainActivity.startTask();
                                    return;
                                }
                            }
                        }
                    }
                    Log.i(TAG, "存入任务");
                    //存入任务
                    cnt.addObj(valueMap);
                    //开始任务
                    MainActivity.startTask();
                }else{
                    String opType = data.getString("opType");
                    String opClass = data.getString("opClass");
                    JSONObject obj = data.getJSONObject("obj");
                    if("setData".equals(opType)){
                        if("editDeviceName".equals(opClass)){
                            editDeviceName(obj.getString("deviceName"));
                        }
                    }
                }

            }else{
                //数据获取失败
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = msg;
                MainActivity.mHandler.sendMessage(message);
            }
        }catch (Exception e){
            Log.e(TAG, "解析数据失败", e);
        }
    }

    /**
     * 封装设备信息
     * @param deviceMap
     * @return
     */
    public static Object setDeviceInfo(Map<String, String> deviceMap){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("opType", "setData");
            map.put("opClass", "editDevice");
            JSONObject paramsObj = new JSONObject((deviceMap));
            map.put("obj", paramsObj);
            return map;
        }catch (Exception e){
            Log.e(TAG, "封装设备数据失败", e);
        }
        return null;
    }

    /**
     * 封装微信信息
     * @param wechatMap
     * @return
     */
    public static Object setWechatInfo(Map<String, String> wechatMap){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("opType", "setData");
            map.put("opClass", "editAccount");
            JSONObject paramsObj = new JSONObject((wechatMap));
            map.put("obj", paramsObj);
            return map;
        }catch (Exception e){
            Log.e(TAG, "封装微信数据失败", e);
        }
        return null;
    }

    /**
     * 封装添加好友信息
     * @param account
     * @param state
     * @return
     */
    public static Object setAddFriendInfo(String account, Integer state, Integer taskId, Integer type){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("opType", "setData");
            map.put("opClass", "editFriendState");
            Map<String, Object> objMap = new HashMap<String, Object>();
            objMap.put("account", account);
            objMap.put("targetAccount", MainActivity.wechatInfo.get("wechatAccount") + "&" + taskId);
            objMap.put("state", state);
            objMap.put("deviceNum", MainActivity.MOBILE_IMEI);
            objMap.put("type", type);
            map.put("obj", objMap);
            return map;
        }catch (Exception e){
            Log.e(TAG, "封装添加好友信息失败", e);
        }
        return null;
    }

    /**
     * 封装QQ添加好友信息
     * @param account
     * @param state
     * @param targetAccount
     * @param taskId
     * @return
     */
    public static Object setQQAddFriendInfo(String account, Integer state, String targetAccount, Integer taskId, Integer type){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("opType", "setData");
            map.put("opClass", "editQQFriendState");
            Map<String, Object> objMap = new HashMap<String, Object>();
            objMap.put("account", account);
            objMap.put("targetAccount", targetAccount);
            objMap.put("targetDevice", MainActivity.MOBILE_IMEI + "&" + taskId);
            objMap.put("taskId", MyAccessibilityService.taskId);
            objMap.put("state", state);
            objMap.put("deviceNum", MainActivity.MOBILE_IMEI);
            objMap.put("type", type);
            map.put("obj", objMap);
            return map;
        }catch (Exception e){
            Log.e(TAG, "封装QQ添加好友信息失败", e);
        }
        return null;
    }

    /**
     * 封装QQ账号状态信息
     * @param account
     * @param state
     * @return
     */
    public static Object setQQTargetStateInfo(String account, Integer state){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("opType", "setData");
            map.put("opClass", "editQQTargetState");
            Map<String, Object> objMap = new HashMap<String, Object>();
            objMap.put("account", account);
            objMap.put("state", state);
            map.put("obj", objMap);
            return map;
        }catch (Exception e){
            Log.e(TAG, "封装QQ账号状态信息失败", e);
        }
        return null;
    }

    /**
     * 封装QQ账号备注信息
     * @param account
     * @param remark
     * @return
     */
    public static Object setQQTargetRemarkInfo(String account, String remark){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("opType", "setData");
            map.put("opClass", "editQQTargetRemark");
            Map<String, Object> objMap = new HashMap<String, Object>();
            objMap.put("account", account);
            objMap.put("remark", remark);
            map.put("obj", objMap);
            return map;
        }catch (Exception e){
            Log.e(TAG, "封装QQ账号备注信息失败", e);
        }
        return null;
    }

    /**
     * 封装完成任务信息
     * @param taskId
     * @param deviceNum
     * @return
     */
    public static Object setfinishTask(Integer taskId, String deviceNum){
        try{
            Map<String, Object> map = new HashMap<String, Object>();
            map.put("opType", "setData");
            map.put("opClass", "finishTask");
            Map<String, Object> objMap = new HashMap<String, Object>();
            objMap.put("taskId", taskId);
            objMap.put("deviceNum", deviceNum);
            objMap.put("state", 1);
            map.put("obj", objMap);
            return map;
        }catch (Exception e){
            Log.e(TAG, "封装完成任务信息失败", e);
        }
        return null;
    }

    /**
     * 修改设备名称
     * @param deviceName
     */
    public static void editDeviceName(String deviceName){
        try{
            Log.i(TAG, "修改设备名称");
            Message msgMessage = Message.obtain();
            msgMessage.obj = deviceName;
            msgMessage.what = 200;
            MainActivity.mHandler.sendMessage(msgMessage);
            //开始任务
            MainActivity.startTask();
        }catch (Exception e){
            Log.e(TAG, "修改设备名称失败", e);
        }
    }

    /**
     * json转map
     * @param data
     * @return
     */
    public static Map<String, Object> jsonToMap(JSONObject data){
        Map<String, Object> valueMap = new HashMap<String, Object>();
        try{
            Iterator<String> keyIter = data.keys();
            String key;
            Object value;
            while (keyIter.hasNext()) {
                key = (String) keyIter.next();
                value = data.get(key);
                valueMap.put(key, value);
            }
        }catch (Exception e){
            Log.e(TAG, "json转map失败", e);
        }
        return valueMap;
    }

    /**
     * json转list
     * @param json
     * @return
     */
    public static List<JSONObject> jsonToList(JSONArray json){
        List<JSONObject> list = new ArrayList<JSONObject>();
        try{
            for(int i = 0;i < json.length();i++){
                list.add((JSONObject) json.get(i));
            }
        }catch (Exception e){
            Log.e(TAG, "json转list失败", e);
        }
        return list;
    }

    /**
     * json转list
     * @param json
     * @return
     */
    public static List<String> jsonToList2(JSONArray json){
        List<String> list = new ArrayList<String>();
        try{
            for(int i = 0;i < json.length();i++){
                list.add(json.getString(i));
            }
        }catch (Exception e){
            Log.e(TAG, "json转list失败", e);
        }
        return list;
    }

}
