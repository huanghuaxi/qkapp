package com.qk.manager.websocket.client;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.DataUtil;
import com.qk.qkapp.MainActivity;
import com.qk.qkapp.MyAccessibilityService;

import org.java_websocket.client.WebSocketClient;
import org.java_websocket.handshake.ServerHandshake;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Iterator;

/**
 * websocket 客户端
 */
public class AppWebSocketClient extends WebSocketClient {

	private static final String TAG = MyAccessibilityService.class.getName();
	//是否连接状态
	private static boolean isLink = false;

	private static Handler handler = new Handler();

	private static Handler handler2 = null;

	/**
	 * @description: 获取Client 对象
 	 * @param url
	 * @throws URISyntaxException
	 */
	public AppWebSocketClient(String url) throws URISyntaxException {
		super(new URI(url));
	}

	/**
	 * @description: 打开链接
	 * @param shake
	 */
	@Override
	public void onOpen(ServerHandshake shake) {
		Log.i("websocket onOpen","握手...");
		for(Iterator<String> it=shake.iterateHttpFields();it.hasNext();) {
			String key = it.next();
			Log.i("websocket onOpen",key + ":" + shake.getFieldValue(key));
		}
		Message message = MainActivity.mHandler.obtainMessage();
		message.what = 100;
		message.obj = "与服务器连接成功";
		MainActivity.mHandler.sendMessage(message);
		isLink = true;
	}

	/**
	 * @desription:接收消息
	 * @param paramString
	 */
	@Override
	public void onMessage(String paramString) {
		Log.i("websocket onMessage",paramString);
		try{
			//发送消息
			Message message = handler2.obtainMessage();
			message.what = 1;
			message.obj = paramString;
			handler2.sendMessage(message);
		}catch (Exception e){
			//创建子线程
			receiveMessage();
			//发送消息
			Message message = handler2.obtainMessage();
			message.what = 1;
			message.obj = paramString;
			handler2.sendMessage(message);
		}
	}

	/**
	 * @desription:关闭链接
	 * @param paramInt
	 * @param paramString
	 * @param paramBoolean
	 */
	@Override
	public void onClose(int paramInt, String paramString, boolean paramBoolean) {
		Log.i("websocket onClose","paramString:"+paramString+",paramInt:"+paramInt+",paramBoolean:"+paramBoolean);
		isLink = false;
		Log.i(TAG, "重新连接WebSocket");
		Message message = MainActivity.mHandler.obtainMessage();
		message.what = 100;
		message.obj = "与服务器连接断开";
		MainActivity.mHandler.sendMessage(message);
		if(relinkRunnable == null){
			relink();
		}
		handler.postDelayed(relinkRunnable,1000 * 60 * 3);
	}

	/**
	 * @desription:异常处理
	 * @param e
	 */
	@Override
	public void onError(Exception e) {
		Log.e("websocket Exception", "WebSocket连接发生异常",e);

	}

	/**
	 * 保持连接
	 */
	private static Runnable keepAliveRunnable = null;
	public static void keepAlive(){
		try{
			if(keepAliveRunnable == null){
				keepAliveRunnable  = new Runnable(){
					@Override
					public void run() {
						try{
							if(isLink){
								Log.i(TAG, "保持连接");
								String msg = DataUtil.setData(CodeUtil.WEBSOCKET_ONLINE_CODE, "保持连接", null);
								MainActivity.myClient.send(msg);
								handler.postDelayed(this,1000 * 60);
							}
						}catch (Exception e){
							Log.e(TAG, "保持连接失败", e);
						}

					}
				};
			}
			handler.postDelayed(keepAliveRunnable,1000 * 60);
		}catch (Exception e){
			Log.e(TAG, "保持WebSocket连接失败", e);
		}
	}

	/**
	 * 重新连接
	 */
	private static Runnable relinkRunnable = null;
	public static void relink(){
		try {
			relinkRunnable  = new Runnable(){
				@Override
				public void run() {
					try{
						//重连
						if(!isLink){
							Message message = MainActivity.mHandler.obtainMessage();
							message = MainActivity.mHandler.obtainMessage();
							message.what = 100;
							message.obj = "重连中...";
							MainActivity.mHandler.sendMessage(message);
							MainActivity.lineService();
						}
					}catch (Exception e){
						Log.e(TAG, "重新连接WebSocket失败", e);
					}
				}
			};

		}catch (Exception e){
			Log.e(TAG, "创建重连线程失败", e);
		}

	}

	/**
	 * 接收消息
	 */
	public void receiveMessage(){
		Log.i(TAG, "创建接收消息子线程");
		Thread thread = new Thread(){
			@Override
			public void run(){
				try{
					//建立消息循环的步骤
					Looper.prepare();//1、初始化Looper
					handler2 = new Handler(){//2、绑定handler到CustomThread实例的Looper对象
						public void handleMessage (Message msg) {//3、定义处理消息的方法
							switch(msg.what) {
								case 1:
									Message message = MainActivity.mHandler.obtainMessage();
									message.what = 100;
									message.obj = "获取服务端数据";
									MainActivity.mHandler.sendMessage(message);
									DataUtil.getData(msg.obj.toString());
							}
						}
					};
					Looper.loop();//4、启动消息循环
				}catch (Exception e){
					Log.e(TAG, "接收消息失败", e);
				}
			}
		};
		thread.start();
		try {
			Thread.sleep(1000);
		}catch (Exception e){
			Log.e(TAG, "延迟出错", e);
		}

	}


 }
