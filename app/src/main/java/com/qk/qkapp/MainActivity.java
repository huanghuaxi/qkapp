package com.qk.qkapp;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.os.Bundle;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.qk.manager.common.AppContext;
import com.qk.manager.common.GenerateClsUtil;
import com.qk.manager.common.ProperUtil;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.intf.TaskDataIntfService;
import com.qk.manager.util.CodeUtil;
import com.qk.manager.util.Constants;
import com.qk.manager.util.DataUtil;
import com.qk.manager.util.SystemUtil;
import com.qk.manager.websocket.client.AppWebSocketClient;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.os.Handler;
import android.os.Message;
import android.provider.Settings;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;


/**
 * 主页
 */
public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getName();

    public static MainActivity mActivity;

    private static TextView textView;

    private static TextView titleView;

    private static Button qrcodeBtn;

    private static Button wechatBtn;

    public static String MOBILE_IMEI = "";
    //设备名称
    public static String deviceName = "";

    public static AppWebSocketClient myClient = null;

    public static Map<String, String> wechatInfo = new HashMap<String, String>();

    public static boolean isState = false;
    //申请权限列表
    private String[] permissions = new String[]{
            Manifest.permission.READ_PHONE_STATE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.CAMERA};
    //未通过权限列表
    List<String>  mPermissionList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isState = true;
        //防止打开新的页面
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            Log.i(TAG, "已开启关闭页面");
            finish();
            return;
        }

        //不息屏
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON,WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        setContentView(R.layout.activity_main);

        // 以下用来捕获程序崩溃异常
//        Thread.setDefaultUncaughtExceptionHandler(restartHandler); // 程序崩溃时触发线程

        mActivity = this;

        textView = findViewById(resId("id", "text"));
        titleView = findViewById(resId("id", "title"));


        if(!"".equals(deviceName)){
            titleView.setText(deviceName);
        }

        //初始化扫一扫按钮
        initQrcordBtn();
        //初始化获取微信按钮
        initWechatBtn();

        /*//获取配置文件
        Properties properties = getProperties(getApplicationContext());
        //获取上下文实例，单例模式
        AppContext appContext = AppContext.getInstance();
        appContext.setProperties(properties);*/

    }

    public static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            switch (msg.what){
                case 100:
                    //输出日志
                    SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    if(textView.getLineCount() >= 40){
                        textView.setText("\n" + format.format(new Date()) + "\t" + msg.obj.toString());
                    }else{
                        textView.append("\n" + format.format(new Date()) + "\t" + msg.obj.toString());
                    }
                    break;
                case 200:
                    //更改标题
                    deviceName = msg.obj.toString();
                    titleView.setText(msg.obj.toString());
                    break;
                case 300:
                    //弹出二维码扫描对话框
                    new AlertDialog.Builder(mActivity).setTitle("提示")//设置对话框标题
                        .setMessage("没有获取到服务器地址，请扫描二维码")//设置显示的内容
                        .setPositiveButton("扫一扫", new DialogInterface.OnClickListener()  {//添加确定按钮
                            @Override
                            public void onClick(DialogInterface dialog, int which) {//确定按钮的响应事件
                                /*以下是启动我们自定义的扫描活动*/
                                IntentIntegrator intentIntegrator = new IntentIntegrator(mActivity);
                                intentIntegrator.setBeepEnabled(true);
                                /*设置启动我们自定义的扫描活动，若不设置，将启动默认活动*/
                                intentIntegrator.setCaptureActivity(ScanActivity.class);
                                intentIntegrator.initiateScan();
                            }
                        })
                        .setNegativeButton("取消", new DialogInterface.OnClickListener() {//添加取消按钮
                            @Override
                            public void onClick(DialogInterface dialog, int which) {//取消按钮的响应事件
                                //输出日志
                                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                if(textView.getLineCount() >= 40){
                                    textView.setText("\n" + format.format(new Date()) + "\t没有连上服务器");
                                }else{
                                    textView.append("\n" + format.format(new Date()) + "\t没有连上服务器");
                                }
                            }
                        }).show();
                    break;
                case 400:
                    //修改按钮为可用
                    qrcodeBtn.setEnabled(true);
                    wechatBtn.setEnabled(true);
                    break;
            }
        }
    };

    /**
     * 初始化扫一扫按钮
     */
    private void initQrcordBtn(){
        qrcodeBtn = findViewById(resId("id", "qrcordBtn"));
        //添加按钮点击监听
        qrcodeBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.i(TAG, "扫一扫按钮点击监听");
                /*以下是启动我们自定义的扫描活动*/
                IntentIntegrator intentIntegrator = new IntentIntegrator(mActivity);
                intentIntegrator.setBeepEnabled(true);
                /*设置启动我们自定义的扫描活动，若不设置，将启动默认活动*/
                intentIntegrator.setCaptureActivity(ScanActivity.class);
                intentIntegrator.initiateScan();
            }
        });
        //禁用以防用户不小心点到
        if("".equals(MOBILE_IMEI)){
            qrcodeBtn.setEnabled(false);
        }
    }

    /**
     * 初始化微信按钮
     */
    private void initWechatBtn(){
        wechatBtn = findViewById(resId("id", "wechatBtn"));
        //添加按钮点击监听
        wechatBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                Log.i(TAG, "微信按钮点击监听");
                wechatInfo.put("wechatAccount", "");
                getWechatInfo();
            }
        });
        //禁用以防用户不小心点到
        if("".equals(MOBILE_IMEI)){
            wechatBtn.setEnabled(false);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * 获取配置文件信息
     * @param cnt
     * @return
     */
    public Properties getProperties(Context cnt){
        Properties properties = ProperUtil.getProperties(cnt, "appConfig.properties");
        return properties;
    }


    @Override
    protected void onStart() {
        super.onStart();
        Log.d("onStart","onStart");
        isState = true;
        //判断任务页面是否存在，存在则销毁
        if(MyAccessibilityService.isRun != 0 && TaskActivity.taskActivity != null){
            TaskActivity.isBack = false;
            TaskActivity.taskActivity.finish();
        }

        Log.d(TAG, "isRun:" + MyAccessibilityService.isRun);
        if(MyAccessibilityService.isRun != 0){
            openTaskPage(MyAccessibilityService.taskId);
        }
        if("".equals(MOBILE_IMEI)){
            initPermission();
        }else{
            getWechatInfo();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if("".equals(MOBILE_IMEI)){
            requestPermission();
        }else{
            getWechatInfo();
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d("onPause","onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d("onStop","onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d("onDestroy","onDestroy");
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        Log.i(TAG, "打开页面回调");
    }

    /**
     *  app启动调用
     */
    public void appStart(){
        try{
            boolean b = MyAccessibilityService.isStart();
            if(!b){
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "开启无障碍设置页面";
                mHandler.sendMessage(message);
                Log.i(TAG, "开启无障碍设置页面");
                Thread.sleep(1000);
                //跳转到无障碍服务设置页
                try {
                    mActivity.startActivity(new Intent(Settings.ACTION_ACCESSIBILITY_SETTINGS));
                } catch (Exception e) {
                    mActivity.startActivity(new Intent(Settings.ACTION_SETTINGS));
                    e.printStackTrace();
                }
                isState = false;
            }else{
                getWechatInfo();
            }
        }catch (Exception e){
            Log.e(TAG, "开启无障碍失败");
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "开启无障碍失败";
            mHandler.sendMessage(message);
        }
    }

    /**
     *  获取微信信息
     */
    public void getWechatInfo(){
        try{
            boolean b = MyAccessibilityService.isStart();
            if(b) {
                String account = wechatInfo.get("wechatAccount");
                Log.i(TAG, "account:" + account);
                if(account == null || "".equals(account)){
                    Log.i(TAG, "获取微信信息");
                    MyAccessibilityService.opType = 0;
                    MyAccessibilityService.taskType = TaskTypeEnum.WECHAT_INFO.getCode();
                    MyAccessibilityService.className = TaskTypeEnum.getClsName(TaskTypeEnum.WECHAT_INFO.getCode());
                    MyAccessibilityService.isRun = 1;
                    Thread.sleep(1000);
                    //打开微信
                    openTaskPage(MyAccessibilityService.taskId);
                    Thread.sleep(1000);
                }else{
                    //查看是否有任务要执行
                    Log.i(TAG, "查看是否有任务要执行");
                    startTask();
                }
                /*getTask();
                Thread.sleep(1000);*/
            }else{
                Log.e(TAG, "无障碍未启动");
                appStart();
            }
        }catch (Exception e){
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "获取微信信息失败";
            mHandler.sendMessage(message);
            Log.e(TAG, "获取微信信息失败", e);
        }
    }

    /**
     * 连接服务器
     */
    public static void lineService() {
        try {
            //启用按钮
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 400;
            mHandler.sendMessage(message);
            //WebSocket连接
            Log.i(TAG, "WebSocket连接");
            /*AppContext cnt = AppContext.getInstance();
            String url = cnt.getProperties().getProperty("websocket_service_url");*/
            String url = getServiceUrl();
            if(url == null || "".equals(url)){
                //服务器地址不存在，弹出提示框要求扫描二维码
                message = MainActivity.mHandler.obtainMessage();
                message.what = 300;
                mHandler.sendMessage(message);
                return ;
            }

            message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "服务器地址：" + url;
            mHandler.sendMessage(message);

            Log.i(TAG, "WebSocketUrl:" + url + "&" + MOBILE_IMEI);
            myClient = new AppWebSocketClient(url + "&" + MOBILE_IMEI);
            myClient.connectBlocking();
            //发送设备数据
            String userId = url.substring(url.lastIndexOf("/") + 1);
            wechatInfo.put("userId", userId);
            String deviceMsg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "设备信息", DataUtil.setDeviceInfo(wechatInfo));
            MainActivity.myClient.send(deviceMsg);
            //发送微信数据
            if(wechatInfo.get("wechatAccount") != null && !"".equals(wechatInfo.get("wechatAccount")) && !"&".equals(wechatInfo.get("wechatAccount"))){
                String msg = DataUtil.setData(CodeUtil.SUCCESS_CODE, "微信信息", DataUtil.setWechatInfo(wechatInfo));
                MainActivity.myClient.send(msg);
            }
            //获取任务
            getTask();
            AppWebSocketClient.keepAlive();
        } catch (Exception e) {
            Log.e(TAG, "WebSocket连接失败", e);
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "与服务器连接失败";
            mHandler.sendMessage(message);
        }

    }

    /**
     * 修改服务器地址
     * @param url
     * @return
     */
    public static void eidtServiceUrl(String url){
        Log.i(TAG, "修改服务器地址:" + url);
        //存储服务器地址
        SharedPreferences sharePreferences = mActivity.getSharedPreferences("service_info", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharePreferences.edit();
        editor.putString("url", url);
        editor.commit();
    }

    /**
     * 读取服务器地址
     * @return
     */
    public static String getServiceUrl(){
        Log.i(TAG, "获取服务器地址");
        SharedPreferences sharePreferences = mActivity.getSharedPreferences("service_info", Context.MODE_PRIVATE);
        String url = sharePreferences.getString("url", "");
        return url;
    }

    /**
     *  获取任务
     */
    public static void getTask(){
        try{
            boolean b = MyAccessibilityService.isStart();
            if(b){
                AppContext cnt = AppContext.getInstance();
                //获取任务
                /*Map<String, Object> testData = testData();
                cnt.addObj(testData);*/
                List<Object> list = cnt.getObj();
                if(list != null && list.size() > 0){
                    //开始任务
                    startTask();
                }else{
                    Message message = MainActivity.mHandler.obtainMessage();
                    message.what = 100;
                    message.obj = "没有要执行的任务";
                    mHandler.sendMessage(message);
                }
            }
        }catch (Exception e) {
            Log.e(TAG, "获取任务失败");
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "获取任务失败";
            mHandler.sendMessage(message);
        }
    }

    /**
     * 开始任务
     */
    public static void startTask(){
        try{
            Log.i(TAG, "isState:" + isState);
            /*if(isState == false){
                boolean isMyApp = isMyApp();
                Log.i(TAG, "是否在我的app页面：" + isMyApp);
                if(isMyApp){
                    openTaskPage(MyAccessibilityService.taskId);
                }
                return;
            }*/
            Log.i(TAG, "isRun:" + MyAccessibilityService.isRun);
            if(MyAccessibilityService.isRun != 0){
//                boolean isMyApp = isMyApp();
                Log.i(TAG, "是否在我的app页面：" + MainActivity.isState);
                if(MainActivity.isState){
                    //跳转到任务页面再跳转到微信
                    openTaskPage(MyAccessibilityService.taskId);
                }else if((new Date().getTime() - MyAccessibilityService.timeOut) > (1000 * 60)){
                    Log.i(TAG, "执行任务超时，返回我的APP");
                    openMyApp();
                }
                return;
            }else{
                if(!MainActivity.isState){
                    openMyApp();
                    return ;
                }
            }
            if("".equals(MyAccessibilityService.taskType)){
                AppContext cnt = AppContext.getInstance();

                List<Object> list = cnt.getObj();
                if(list != null && list.size() > 0){
                    Map<String, Object> map = (Map<String, Object>)list.get(0);
                    Log.i(TAG, "数据：" + map.toString());
                    setData(map);
                    Thread.sleep(1000);
                    openTaskPage(MyAccessibilityService.taskId);
                }else{
                    Message message = MainActivity.mHandler.obtainMessage();
                    message.what = 100;
                    message.obj = "没有要执行的任务";
                    mHandler.sendMessage(message);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "开始执行任务失败", e);
        }
    }

    /**
     *  打开微信
     */
    public static void openWechat(){
        try{
            if(MyAccessibilityService.isRun != 0){
                boolean b = MyAccessibilityService.isStart();
                if(b){
                    Thread thread = new Thread(new Runnable(){
                        public void run(){
                            // run方法具体重写
                            try{
                                Thread.sleep(3000);
                                Log.i(TAG, "打开微信");
                                ComponentName cn = new ComponentName("com.tencent.mm", "com.tencent.mm.ui.LauncherUI");
                                Intent intent = new Intent();
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setComponent(cn);
                                if(MyAccessibilityService.isRun != 0){
                                    mActivity.startActivity(intent);
                                }
                            }catch (Exception e){
                                Log.e(TAG, "打开微信失败", e);
                                Message message = MainActivity.mHandler.obtainMessage();
                                message.what = 100;
                                message.obj = "打开微信失败，请确认微信已安装";
                                mHandler.sendMessage(message);
                                //重置参数
                                MyAccessibilityService.resetParam();
                                wechatInfo.put("wechatAccount", "&");
                                TaskActivity.toMain();
                                lineService();
                            }
                        }
                    });
                    thread.start();
                }else{
                    Message message = MainActivity.mHandler.obtainMessage();
                    message.what = 100;
                    message.obj = "请开启无障碍服务";
                    mHandler.sendMessage(message);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "打开微信失败", e);
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "打开微信失败，请确认微信已安装";
            mHandler.sendMessage(message);
        }
    }

    /**
     * 打开QQ
     */
    public static void openQQ(){
        try{
            if(MyAccessibilityService.isRun != 0){
                boolean b = MyAccessibilityService.isStart();
                if(b){
                    Thread thread = new Thread(new Runnable(){
                        public void run(){
                            // run方法具体重写
                            try{
                                Thread.sleep(3000);
                                Log.i(TAG, "打开QQ");
                                ComponentName cn = new ComponentName("com.tencent.mobileqq", "com.tencent.mobileqq.activity.SplashActivity");
                                Intent intent = new Intent();
                                intent.addCategory(Intent.CATEGORY_LAUNCHER);
                                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                intent.setComponent(cn);
                                mActivity.startActivity(intent);
                            }catch (Exception e){
                                Log.e(TAG, "打开QQ失败", e);
                                Message message = MainActivity.mHandler.obtainMessage();
                                message.what = 100;
                                message.obj = "打开QQ失败，请确认QQ已安装";
                                mHandler.sendMessage(message);
                            }
                        }
                    });
                    thread.start();
                }else{
                    Message message = MainActivity.mHandler.obtainMessage();
                    message.what = 100;
                    message.obj = "请开启无障碍服务";
                    mHandler.sendMessage(message);
                }
            }

        }catch (Exception e){
            Log.e(TAG, "打开QQ失败", e);
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "打开QQ失败，请确认QQ已安装";
            mHandler.sendMessage(message);
        }
    }

    /**
     * 存入操作数据
     * @param map
     */
    public static void setData(Map<String, Object> map) throws Exception{
        //输出任务列表
        Message message = MainActivity.mHandler.obtainMessage();
        message.what = 100;
        message.obj = "当前任务列表\n";
        AppContext cnt = AppContext.getInstance();
        List<Map<String, Object>> taskList = cnt.getObj();
        if(taskList == null || taskList.size() == 0){
            message.obj += "无任务\n";
        }else {
            for(Map<String, Object> taskMap : taskList){
                message.obj += "任务ID：" + taskMap.get("taskId");
                message.obj += "，任务名称：" + TaskTypeEnum.getTaskNameByCode(taskMap.get("taskType").toString());
                message.obj += "\n";
            }
        }
        mHandler.sendMessage(message);

        Log.i(TAG, "存入操作数据");
        String taskType = map.get("taskType") != null ? map.get("taskType").toString() : "";
        String taskId = map.get("taskId") != null ? map.get("taskId").toString() : "0";
        String className = TaskTypeEnum.getClsName(taskType);
        String dataName = TaskTypeEnum.getDataName(taskType);

        MyAccessibilityService.taskId = Integer.parseInt(taskId);

        Log.i(TAG, "跳转到数据处理类");
        TaskDataIntfService taskDataIntfService = (TaskDataIntfService) GenerateClsUtil.getHanlderCls(dataName);
        boolean isReturn = taskDataIntfService.handler(new Object(), map);
        if(!isReturn){
            return ;
        }
        MyAccessibilityService.taskType = taskType;
        MyAccessibilityService.className = className;
        MyAccessibilityService.isRun = 1;

        message = MainActivity.mHandler.obtainMessage();
        message.what = 100;
        message.obj = "任务ID:" + taskId;
        mHandler.sendMessage(message);
    }

    /**
     * 跳转到任务页面
     * @param taskId
     */
    public static void openTaskPage(Integer taskId){
        try{
            //跳转到任务页面
            MainActivity.isState = false;
            Log.i(TAG, "跳转到任务页面");
            Intent intent = new Intent(mActivity, TaskActivity.class);
            intent.putExtra("taskId", taskId);
            intent.putExtra("deviceName", deviceName);
            mActivity.startActivity(intent);
        }catch (Exception e){
            Log.e(TAG, "跳转到任务页面失败", e);
        }
    }

    /**
     * 打开我的app
     */
    public static void openMyApp(){
        try{
            Thread.sleep(1000);
            /*Log.i(TAG, "返回手机主页");
            Intent intent1 = new Intent(Intent.ACTION_MAIN);
            intent1.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            intent1.addCategory(Intent.CATEGORY_HOME);
            mActivity.startActivity(intent1);*/
            /*Log.i(TAG, "打开我的APP");
//            ComponentName cn = new ComponentName("com.qk.qkapp", "com.qk.qkapp.MainActivity");
//            Intent intent = new Intent();
            Intent intent = new Intent(mActivity, MainActivity.class);
            intent.setAction(Intent.ACTION_MAIN);
//            intent.setAction("android.intent.action.MAINACT");
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
//            intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED|Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP );
//            intent.setClassName("com.qk.qkapp", "com.qk.qkapp.MainActivity");
//            intent.setComponent(cn);
//            mActivity.startActivity(intent);
            mActivity.onResume();*/
            Log.i(TAG, "打开我的APP");
            /**获取ActivityManager*/
            ActivityManager activityManager = (ActivityManager) mActivity.getSystemService(ACTIVITY_SERVICE);

            /**获得当前运行的task(任务)*/
            List<ActivityManager.AppTask> taskList = activityManager.getAppTasks();
            Log.i(TAG, "packageName:" + mActivity.getPackageName());
            for (ActivityManager.AppTask task : taskList) {
                /**找到本应用的 task，并将它切换到前台*/
                ActivityManager.RecentTaskInfo taskInfo = task.getTaskInfo();
                if(taskInfo.topActivity == null || taskInfo.topActivity.getPackageName() == null){
                    continue;
                }
                Log.i(TAG, "packageName1:" + taskInfo.topActivity.getPackageName());
                if (taskInfo.topActivity.getPackageName().equals(mActivity.getPackageName())) {
                    Log.i(TAG, "taskInfo.id:" + taskInfo.id);
                    activityManager.moveTaskToFront(taskInfo.id, 0);
                    if(taskInfo.topActivity.getClassName().indexOf("TaskActivity") > 0){
                        isState = false;
                        Thread.sleep(1000);
                        TaskActivity.toMain();
                    }else{
                        isState = true;
                    }
                    return;
                }
            }

            //获得当前正在运行的activity
            /*List<ActivityManager.RunningTaskInfo> runningTaskInfos = activityManager.getRunningTasks(Integer.MAX_VALUE);
            if(runningTaskInfos != null && runningTaskInfos.size() > 0){
                for(ActivityManager.RunningTaskInfo run : runningTaskInfos){
                    if(run.topActivity == null || run.topActivity.getPackageName() == null){
                        continue;
                    }
                    if(run.topActivity.getPackageName().equals(mActivity.getPackageName())){
                        activityManager.moveTaskToFront(run.id, 0);
                        return ;
                    }
                }
            }*/

            /*Log.i(TAG, "打开我的APP");
//            ComponentName cn = new ComponentName("com.qk.qkapp", "com.qk.qkapp.MainActivity");
//            Intent intent = new Intent();
            Intent intent = new Intent(mActivity, MainActivity.class);
            intent.setAction(Intent.ACTION_MAIN);
//            intent.setAction("android.intent.action.MAINACT");
            intent.addCategory(Intent.CATEGORY_LAUNCHER);
//            intent.addFlags(Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED|Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP );
//            intent.setClassName("com.qk.qkapp", "com.qk.qkapp.MainActivity");
//            intent.setComponent(cn);
            mActivity.startActivity(intent);*/



        }catch (Exception e){
            Log.e(TAG, "打开我的APP失败", e);
        }
    }



    /**
     * 测试数据
     * @return
     */
    public static Map<String, Object> testData(){
        Map<String, Object> dataMap = new HashMap<String, Object>();
        dataMap.put("taskId", 1);
        dataMap.put("taskType", "QQFRIENDCHECK");
        dataMap.put("type", 0);
        //朋友圈发送视频
        /*dataMap.put("videoUrl", "http://192.168.1.10:8080/uploads/momentsImg/1567042550383.mp4");
        dataMap.put("msg", "");*/
        //朋友圈发送图片
        /*dataMap.put("imgUrl", "http://192.168.1.10:8080/uploads/activityImg/1555321150390.jpg");
        dataMap.put("msg", "6666666");*/
        //朋友圈发送文字
        /*dataMap.put("msg", "666666");*/
        //阅读朋友圈
        /*dataMap.put("goodRatio", 0);
        dataMap.put("minTime", 20);
        dataMap.put("maxTime", 30);*/
        //接收语音
        /*dataMap.put("minTime", 10);
        dataMap.put("maxTime", 20);
        dataMap.put("callTime", 5);*/
        //发送语音
        /*dataMap.put("targetAcc", "000");
        dataMap.put("callTime", 10);*/
        //发送消息
        /*JSONArray friends = new JSONArray();
        friends.put("639205240596");
        friends.put("16570902114");
        friends.put("0");
        friends.put("18050795597");
        dataMap.put("target", friends);
        JSONArray msgList = new JSONArray();
        msgList.put("[微笑]");
        msgList.put("[撇嘴]");
        msgList.put("[发呆]");
        msgList.put("[得意]");
        msgList.put("[流泪]");
        msgList.put("[害羞]");
        dataMap.put("sendMsg", msgList);
        dataMap.put("msgNum", 3);
        dataMap.put("minTime", "5");
        dataMap.put("maxTime", "10");*/
        //拉人进群
        /*Map<String, Object> groupMap = new HashMap<String, Object>();
        groupMap.put("groupName", "201908\n201909");
        groupMap.put("invite", "201908\n201909");
        groupMap.put("noInvite", "");
        groupMap.put("memberNum", 3);
        groupMap.put("type", 1);
        JSONObject groupJson = new JSONObject(groupMap);
        dataMap.put("pullToGroupList", groupJson);*/
        /*List<Map<String, String>> memberList = new ArrayList<Map<String, String>>();
        memberList.add("www");
        memberList.add("13860688304");
        memberList.add("aaa");
        memberList.add("17359115385");
        memberList.add("www");
        memberList.add("hhx1503");
        memberList.add("eee");
        memberList.add("小米");
        memberList.add("ewew");
        memberList.add("weixin");
        FriendList.friendList = memberList;
        List<String> groupList = new ArrayList<String>();
        groupList.add("1122");
        groupList.add("测试1");
        groupList.add("测试2");
        PullFriendToGroup.groupList = groupList;*/
        //添加好友
        /*JSONArray friendList = new JSONArray();
        JSONObject friendMap = new JSONObject();
        try {
            friendMap.put("account", "2050171151");
            friendMap.put("remarkName", "");
            friendMap.put("applyText", "请加我好友");
            friendMap.put("sleepTime", "0");
            friendList.put(friendMap);
            friendMap = new JSONObject();
            friendMap.put("account", "2386315850");
            friendMap.put("remarkName", "");
            friendMap.put("applyText", "请加我好友");
            friendMap.put("sleepTime", "0");
            friendList.put(friendMap);
            friendMap = new JSONObject();
            friendMap.put("account", "222");
            friendMap.put("remarkName", "无2");
            friendMap.put("applyText", "请加我好友");
            friendMap.put("sleepTime", "0");
            friendList.put(friendMap);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        dataMap.put("friendList", friendList.toString());*/
        //QQ好友检查
        String msg = new String();
        msg = "你好！\n\n有需要群控系统的吗？\n\n价格可以优惠哦！！！！！\n\n一个月价格，两个月的价格都是不一样的哦！！！";
        dataMap.put("msg", msg);
        JSONObject account = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try{
            JSONObject json = new JSONObject();
            json.put("account", "2040705361");
            json.put("state", 3);
            jsonArray.put(json);
            account.put("1968468565", jsonArray);
            jsonArray = new JSONArray();
            json = new JSONObject();
            json.put("account", "2111890642");
            json.put("state", 3);
            jsonArray.put(json);
            account.put("2386315850", jsonArray);
            jsonArray = new JSONArray();
            json = new JSONObject();
            json.put("account", "2111890642");
            json.put("state", 3);
            jsonArray.put(json);
            json = new JSONObject();
            json.put("account", "2040705361");
            json.put("state", 4);
            jsonArray.put(json);
            account.put("3117649963", jsonArray);
        }catch (Exception e){
            e.printStackTrace();
        }
        dataMap.put("account", account);
        //微信好友检查
        /*String msg = new String();
        msg = "你好！\n\n有需要群控系统的吗？\n\n价格可以优惠哦！！！！！\n\n一个月价格，两个月的价格都是不一样的哦！！！";
        dataMap.put("msg", msg);
        dataMap.put("addFriendId", 1);
        JSONObject account = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try{
            account = new JSONObject();
            account.put("account", "13860688304");
            account.put("state", 3);
            jsonArray.put(account);
            account = new JSONObject();
            account.put("account", "18050795597");
            account.put("state", 4);
            jsonArray.put(account);
            account = new JSONObject();
            account.put("account", "17359115385");
            account.put("state", 3);
            jsonArray.put(account);
        }catch (Exception e){
            e.printStackTrace();
        }
        dataMap.put("account", jsonArray);*/
        //QQ账号密码
        try{
            JSONObject accountPwd = new JSONObject();
            accountPwd.put("3200323015", "1EDAE6DCAD5D8E36870A9BBD6BEE491F");
            accountPwd.put("1968468565", "1EDAE6DCAD5D8E36870A9BBD6BEE491F");
            accountPwd.put("1945925742", "1EDAE6DCAD5D8E36870A9BBD6BEE491F");
            accountPwd.put("3117649963", "1EDAE6DCAD5D8E36870A9BBD6BEE491F");
            accountPwd.put("3117565155", "1EDAE6DCAD5D8E36870A9BBD6BEE491F");
            dataMap.put("qqTargetData", accountPwd);
        }catch (Exception e){
            e.printStackTrace();
        }
        return dataMap;
    }

    public static int resId(Context context, String type, String name) {
        Resources resources = context.getResources();
        return resources.getIdentifier(name, type, context.getApplicationInfo().packageName);
    }

    public int resId(String type, String name) {
        return resId(this, type, name);
    }

    /**
     * 获取权限
     */
    private void initPermission(){
        mPermissionList.clear();//清空已经允许的没有通过的权限
        //逐个判断是否还有未通过的权限
        for (int i = 0;i<permissions.length;i++){
            if (ContextCompat.checkSelfPermission(this,permissions[i])!=
                    PackageManager.PERMISSION_GRANTED){
                mPermissionList.add(permissions[i]);//添加还未授予的权限到mPermissionList中
            }
        }
        //申请权限
        if (mPermissionList.size()>0){//有权限没有通过，需要申请
            ActivityCompat.requestPermissions(this,permissions, Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS);
        }else {
            Log.i(TAG, "取得权限");
            Log.i(TAG, "获取设备信息");
            //获取设备信息
            String imei = SystemUtil.getIMEI(this);
            String deviceType = SystemUtil.getSystemModel();
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "设备号：" + imei;
            mHandler.sendMessage(message);
            message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "设备型号：" + deviceType;
            mHandler.sendMessage(message);
            MOBILE_IMEI = imei;
            wechatInfo.put("deviceNum", imei);
            wechatInfo.put("deviceType", deviceType);
            appStart();
        }
    }


    /**
     * 获取权限回调
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        boolean hasPermissionDismiss = false;//有权限没有通过
        if (Constants.MY_PERMISSIONS_REQUEST_READ_CONTACTS == requestCode){
            for (int i=0;i<grantResults.length;i++){
                if (grantResults[i]==-1){
                    hasPermissionDismiss=true;
                    break;
                }
            }
        }
        if (!hasPermissionDismiss) {
            if("".equals(MOBILE_IMEI)){
                Log.i(TAG, "取得权限");
                Log.i(TAG, "获取设备信息");
                //获取设备信息
                String imei = SystemUtil.getIMEI(this);
                String deviceType = SystemUtil.getSystemModel();
                Message message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "设备号：" + imei;
                mHandler.sendMessage(message);
                message = MainActivity.mHandler.obtainMessage();
                message.what = 100;
                message.obj = "设备型号：" + deviceType;
                mHandler.sendMessage(message);
                MOBILE_IMEI = imei;
                wechatInfo.put("deviceNum", imei);
                wechatInfo.put("deviceType", deviceType);
                appStart();
            }
        } else {
            Log.i(TAG, "未取得权限");
            Message message = MainActivity.mHandler.obtainMessage();
            message.what = 100;
            message.obj = "请给予获取手机信息和存储权限";
            mHandler.sendMessage(message);
        }
    }

    /**
     * 二维码扫描回调
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if(result != null) {
            Log.i(TAG, "二维码信息：" + result.getContents());
            if(result.getContents() == null){
                //取消扫描
                return ;
            }
            //存入服务器地址
            eidtServiceUrl(result.getContents());

            if(myClient != null){
                //与服务器断开
                myClient.close();
            }
            //连接服务器
            lineService();
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

}
