package com.qk.qkapp;

import android.accessibilityservice.AccessibilityService;
import android.accessibilityservice.GestureDescription;
import android.content.Intent;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.view.accessibility.AccessibilityEvent;
import android.view.accessibility.AccessibilityNodeInfo;

import com.qk.manager.common.AppContext;
import com.qk.manager.common.GenerateClsUtil;
import com.qk.manager.enums.TaskTypeEnum;
import com.qk.manager.impl.QQLogin;
import com.qk.manager.impl.QQLoginCheck;
import com.qk.manager.intf.TaskIntfService;
import com.qk.manager.util.AccessibilityWechatUtil;
import com.qk.manager.util.Constants;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;


/**
 *  无障碍服务
 */
public class MyAccessibilityService extends AccessibilityService {

    private static final String TAG = MyAccessibilityService.class.getName();

    public static MyAccessibilityService mService;

    //任务类型
    public static String taskType = "";
    //类名
    public static String className = "";
    //操作数据
    public static List<Map<String, Object>> taskList = null;
    //标识
    public static int flag = 0;
    //序号
    public static int index = 0;
    //是否运行
    public static int isRun = 0;
    //任务ID
    public static int taskId = 0;
    //执行时间
    public static long timeOut = 0;
    //操作应用，0：微信 1：QQ
    public static int opType = 1;

    @Override
    protected void onServiceConnected() {
        super.onServiceConnected();
        mService = this;
        //setServiceInfo();这个方法同样可以实现xml中的配置信息
        //可以做一些开启后的操作比如点两下返回
        try{
            Log.i(TAG, "退回APP");
            Thread.sleep(1000L);
            Thread.sleep(1000L);
            performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
            Thread.sleep(1000L);
            performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
            //打开微信
//            MainActivity.openMyApp();
//            MainActivity.openWechat();

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.i(TAG,"无障碍服务关闭");
        return super.onUnbind(intent);
    }

    @Override
    public void onAccessibilityEvent(AccessibilityEvent event){
        timeOut = new Date().getTime();
        if(flag != 0){
            return ;
        }
        Log.d(TAG, "无障碍服务监听");
        if(opType == 0){
            //微信操作
            wechatOp(event);
        }
        if(opType == 1){
            //QQ操作
            qqOp(event);
        }
    }

    @Override
    public void onInterrupt() {
        //当服务要被中断时调用.会被调用多次
        Log.i(TAG, "无障碍服务中断");
    }

    /**
     * 微信操作
     * @param event
     */
    public void wechatOp(AccessibilityEvent event){
        try {

            if(isRun == 1){
                Log.d(TAG, "跳转到主页面");
                //查看是否在主页面
                int eventType = event.getEventType();
                CharSequence classNameChr = event.getClassName();
                String className = classNameChr.toString();
                switch (eventType){
                    case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:

                        //判断是否为主页，如果微信按钮存在存在则表示为主页
                        if(AccessibilityWechatUtil.clickBottomMenuBtn(getRootInActiveWindow(), 1) != null){
                            isRun = 2;
                        }
                        break;
                    case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                        //判断是否为登录页
                        if("com.tencent.mm.plugin.account.ui.LoginPasswordUI".equals(className)){
                            //重置参数
                            MainActivity.wechatInfo.put("wechatAccount", "&");
                            resetParam();
                            Log.i(TAG, "返回我的APP");
                            Thread.sleep(1000);
                            MainActivity.openMyApp();
                            Thread.sleep(1000);
                            MainActivity.lineService();
                        }else if("com.tencent.mm.ui.widget.b.c".equals(className) || "com.tencent.mm.ui.widget.a.c".equals(className)){
                            //选择弹窗，点击确定
                            Log.i(TAG, "点击确定按钮");
                            clickConfirmBtn(getRootInActiveWindow(), event);
                        }else if(!"com.tencent.mm.ui.LauncherUI".equals(className)){
                            Log.i(TAG, "后退");
                            Thread.sleep(1000);
                            GestureDescription gesture = clickBackBtn(event, getRootInActiveWindow());
                            if(gesture != null){
                                flag = 1;
                                opScreen(gesture);
                            }else{
                                Thread.sleep(1000);
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }
                        }else{
                            List<AccessibilityNodeInfo> pageCounts = new ArrayList<AccessibilityNodeInfo>();
                            AccessibilityWechatUtil.recycleFindView(pageCounts, getRootInActiveWindow(), "com.tencent.mm.ui.mogic.WxViewPager");
                            Log.i(TAG, "page数量：" + pageCounts.size());
                            if(pageCounts.size() <= 0){

                                Log.i(TAG, "后退");
                                GestureDescription gesture = clickBackBtn(event, getRootInActiveWindow());
                                if(gesture != null){
                                    flag = 1;
                                    opScreen(gesture);
                                }else{
                                    Thread.sleep(1000);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                }
                            }
                            Log.i(TAG, "开始执行");
                            isRun = 2;
                        }
                        break;
                }

            }
            if(isRun == 2){
                //运行任务
                Log.d(TAG, "运行任务");
                if(!"".equals(className)){
                    TaskIntfService taskIntfService = (TaskIntfService) GenerateClsUtil.getHanlderCls(className);
                    taskIntfService.handler( new Object(), event, getRootInActiveWindow(), this);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "微信操作失败", e);
        }
    }

    /**
     * QQ操作
     * @param event
     */
    public void qqOp(AccessibilityEvent event){
        try {
            int eventType = event.getEventType();
            CharSequence classNameChr = event.getClassName();
            String clsName = classNameChr == null ? "" : classNameChr.toString();

            if("com.tencent.mobileqq.activity.LoginActivity".equals(clsName) && isRun != 2){
                //判断是否有后退按钮
                List<AccessibilityNodeInfo> backBtnCounts = getRootInActiveWindow().findAccessibilityNodeInfosByText("返回");
                Log.i(TAG, "后退按钮数量：" + backBtnCounts.size());
                if(backBtnCounts.size() > 0){
                    //点击后退按钮
                    GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(backBtnCounts.get(0));
                    flag = 1;
                    opScreen(gesture);
                    return ;
                }else{
                    //执行登录操作
                    className = TaskTypeEnum.getClsName(TaskTypeEnum.QQ_LOGIN.getCode());
                    isRun = 2;
                }
            }

            if(isRun == 1){
                Log.d(TAG, "跳转到主页面");
                //查看是否在主页面
                switch (eventType){
                    case AccessibilityEvent.TYPE_WINDOW_CONTENT_CHANGED:
                        //判断是否为主页，如果微信按钮存在存在则表示为主页
                        if(AccessibilityWechatUtil.clickQQTab(getRootInActiveWindow(), 1) != null){
                            isRun = 2;
                        }
                        break;
                    case AccessibilityEvent.TYPE_WINDOW_STATE_CHANGED:
                        if("com.tencent.mobileqq.activity.SplashActivity".equals(clsName)) {
                            if(AccessibilityWechatUtil.clickQQTab(getRootInActiveWindow(), 1) == null){
                                //判断是否为登录界面
                                Thread.sleep(3000);
                                if(isQQLoginPage(getRootInActiveWindow())){
                                    return ;
                                }
                                Log.i(TAG, "后退");
                                GestureDescription gesture = clickBackBtn(event, getRootInActiveWindow());
                                if(gesture != null){
                                    flag = 1;
                                    opScreen(gesture);
                                }else{
                                    //取消焦点
                                    if(clearFocus(getRootInActiveWindow())){
                                        Thread.sleep(1000);
                                        performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                    }
                                    //后退
                                    Thread.sleep(1000);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);

                                }
                            }
                            Log.i(TAG, "开始执行");
                            isRun = 2;

                        }else if("com.tencent.mobileqq.activity.NotificationActivity".equals(clsName)){
                            //QQ号冻结页面，点击取消
                            clickCancelBtn(getRootInActiveWindow(), this);
                        }else{
                            Log.i(TAG, "后退");
                            Thread.sleep(1000);
                            GestureDescription gesture = clickBackBtn(event, getRootInActiveWindow());
                            if (gesture != null) {
                                flag = 1;
                                opScreen(gesture);
                            } else {
                                //取消焦点
                                if (clearFocus(getRootInActiveWindow())) {
                                    Thread.sleep(1000);
                                    performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                                }
                                //后退
                                Thread.sleep(1000);
                                performGlobalAction(AccessibilityService.GLOBAL_ACTION_BACK);
                            }
                        }
                        break;
                }

            }
            if(isRun == 2){
                //运行任务
                Log.d(TAG, "运行任务");
                if(!"".equals(className)){
                    TaskIntfService taskIntfService = (TaskIntfService) GenerateClsUtil.getHanlderCls(className);
                    taskIntfService.handler( new Object(), event, getRootInActiveWindow(), this);
                }
            }
        }catch (Exception e){
            Log.e(TAG, "QQ操作失败", e);
        }
    }

    /**
     * 点击返回按钮
     * @param event
     * @param rootNodeInfo
     * @return
     */
    public static GestureDescription clickBackBtn(AccessibilityEvent event, AccessibilityNodeInfo rootNodeInfo){
        try{
            if(rootNodeInfo == null){
                return null;
            }
            List<AccessibilityNodeInfo> backBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText("返回");
            Log.i(TAG, "返回按钮数量：" + backBtnCounts.size());
            if(backBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(backBtnCounts.get(0));
                backBtnCounts.get(0).recycle();
                return gesture;
            }
        }catch (Exception e){
            Log.e(TAG, "点击返回按钮失败", e);
        }
        return null;
    }

    /**
     * 点击确定按钮
     * @param rootNodeInfo
     * @param event
     * @return
     */
    public static boolean clickConfirmBtn(AccessibilityNodeInfo rootNodeInfo, AccessibilityEvent event){
        try{
            Thread.sleep(1000);
            //是否有确定按钮
            List<AccessibilityNodeInfo> confirmBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CONFIRM_BTN_NAME);
            Log.i(TAG, "确认按钮数量：" + confirmBtnCounts.size());
            if(confirmBtnCounts.size() > 0){
                //点击确定按钮
                confirmBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                confirmBtnCounts.get(0).recycle();
                return true;
            }
            //是否有不保存按钮
            List<AccessibilityNodeInfo> notRetionBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.NOT_RETAIN_BTN_NAME);
            Log.i(TAG, "不保留按钮数量：" + notRetionBtnCounts.size());
            if(notRetionBtnCounts.size() > 0){
                //点击不保存按钮
                notRetionBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                notRetionBtnCounts.get(0).recycle();
                return true;
            }
            //是否有不保存按钮
            List<AccessibilityNodeInfo> iSeeBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.I_SEE_BTN_NAME);
            Log.i(TAG, "我知道了按钮数量：" + iSeeBtnCounts.size());
            if(notRetionBtnCounts.size() > 0){
                //点击我知道了按钮
                iSeeBtnCounts.get(0).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                iSeeBtnCounts.get(0).recycle();
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击确定按钮失败");
        }
        return false;
    }

    /**
     * 点击取消按钮
     * @param rootNodeInfo
     * @param myAccessibilityService
     * @return
     */
    public static boolean clickCancelBtn(AccessibilityNodeInfo rootNodeInfo, MyAccessibilityService myAccessibilityService){
        try{
            Log.i(TAG, "点击取消按钮");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> cancelBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByText(Constants.CANCEL_BTN_NAME);
            Log.i(TAG, "取消按钮数量：" + cancelBtnCounts.size());
            if(cancelBtnCounts.size() > 0){
                GestureDescription gesture = AccessibilityWechatUtil.clickCoordinate(cancelBtnCounts.get(0));
                MyAccessibilityService.flag = 1;
                myAccessibilityService.opScreen(gesture);
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "点击取消按钮失败", e);
        }
        return false;
    }

    /**
     * 取消焦点
     * @param rootNodeInfo
     * @return
     */
    public static boolean clearFocus(AccessibilityNodeInfo rootNodeInfo){
        try{
            List<AccessibilityNodeInfo> editTextCounts = new ArrayList<AccessibilityNodeInfo>();
            AccessibilityWechatUtil.recycleFindView(editTextCounts, rootNodeInfo, "android.widget.EditText");
            Log.i(TAG, "输入框数量：" + editTextCounts.size());
            for(int i = 0;i < editTextCounts.size();i++){
                //取消焦点
                if(editTextCounts.get(i).isFocused()){
                    Thread.sleep(1000);
                    editTextCounts.get(i).performAction(AccessibilityNodeInfo.ACTION_CLICK);
                    return true;
                }
            }
        }catch (Exception e){
            Log.e(TAG, "移除焦点失败", e);
        }
        return false;
    }

    /**
     * 判断是否为登录界面
     * @param rootNodeInfo
     * @return
     */
    public static boolean isQQLoginPage(AccessibilityNodeInfo rootNodeInfo){
        try{
            Log.i(TAG, "判断是否为登录界面");
            Thread.sleep(1000);
            List<AccessibilityNodeInfo> loginBtnCounts = rootNodeInfo.findAccessibilityNodeInfosByViewId("com.tencent.mobileqq:id/login");
            Log.i(TAG, "登录按钮数量：" + loginBtnCounts.size());
            if(loginBtnCounts.size() > 0){
                return true;
            }
        }catch (Exception e){
            Log.e(TAG, "判断是否为登录页面", e);
        }
        return false;
    }


    /**
     * 辅助功能是否启动
     */
    public static boolean isStart() {
        return mService != null;
    }

    /**
     * 操作屏幕
     * @param gesture
     * @return
     */
    public static Handler sHandler;
    public void opScreen(GestureDescription gesture){

        try{
            Message message = sHandler.obtainMessage();
            message.what = 1;
            message.obj = gesture;
            sHandler.sendMessage(message);
        }catch (Exception e){
            Thread thread = new Thread(){
                @Override
                public void run(){
                    try{
                        //建立消息循环的步骤
                        Looper.prepare();//1、初始化Looper
                        sHandler = new Handler(){//2、绑定handler到CustomThread实例的Looper对象
                            public void handleMessage (Message msg) {//3、定义处理消息的方法
                                switch(msg.what) {
                                    case 1:
                                        try{
                                            Thread.sleep(2000);
                                            GestureDescription gesture = (GestureDescription)msg.obj;
                                            if(gesture == null){
                                                flag = 0;
                                                return ;
                                            }
                                            boolean isDispatched = dispatchGesture(gesture, new AccessibilityService.GestureResultCallback() {
                                                @Override
                                                public void onCompleted(GestureDescription gestureDescription) {
                                                    super.onCompleted(gestureDescription);
                                                    Log.d(TAG, "onCompleted: 完成..........");
                                                    flag = 0;
                                                }

                                                @Override
                                                public void onCancelled(GestureDescription gestureDescription) {
                                                    super.onCancelled(gestureDescription);
                                                    Log.d(TAG, "onCompleted: 取消..........");
                                                    flag = 0;
                                                }
                                            }, null);
                                        }catch (Exception e){
                                            Log.e(TAG, "页面点击/滑动失败", e);
                                        }
                                }
                            }
                        };
                        Looper.loop();//4、启动消息循环
                    }catch (Exception e){
                        Log.e(TAG, "接收消息失败", e);
                    }
                }
            };
            thread.start();

            try {
                Thread.sleep(1000);
            }catch (Exception e2){
                Log.e(TAG, "延迟出错", e2);
            }
            Message message = sHandler.obtainMessage();
            message.what = 1;
            message.obj = gesture;
            sHandler.sendMessage(message);
        }
    }

    /**
     * 重置参数
     */
    public static void resetParam(){
        Log.i(TAG, "重置参数");
        MyAccessibilityService.taskType = "";
        MyAccessibilityService.className = "";
        MyAccessibilityService.isRun = 0;
        QQLoginCheck.isCheckFinish = false;
        QQLogin.accountMap = new JSONObject();
        QQLoginCheck.accountMap = new JSONObject();
        AppContext cnt = AppContext.getInstance();
        if(cnt != null){
            cnt.removeObj(0);
        }
        MyAccessibilityService.taskId = 0;
    }

    /**
     * 回收
     * @param rootNodeInfo
     */
    public static void recycleAll(List<AccessibilityNodeInfo> rootNodeInfo){
        try{
            if(rootNodeInfo != null){
                for(int i = 0;i < rootNodeInfo.size();i++){
                    rootNodeInfo.get(i).recycle();
                }
            }
        }catch (Exception e){
            Log.e(TAG, "回收失败", e);
        }
    }

}



