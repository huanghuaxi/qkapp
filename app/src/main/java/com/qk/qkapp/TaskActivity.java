package com.qk.qkapp;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.qk.manager.enums.TaskTypeEnum;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * 任务界面
 */
public class TaskActivity extends AppCompatActivity {

    private static final String TAG = TaskActivity.class.getName();

    public static TaskActivity taskActivity;

    private static TextView textView;

    private static TextView titleView;
    //设备名称
    private static String deviceName = "";
    //任务ID
    private static Integer taskId = 0;

    public static boolean isBack = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //防止打开新的页面
        if ((getIntent().getFlags() & Intent.FLAG_ACTIVITY_BROUGHT_TO_FRONT) != 0) {
            Log.i(TAG, "已开启关闭页面");
            finish();
            return;
        }

        setContentView(R.layout.activity_task);

        taskActivity = this;

        textView = findViewById(resId("id", "text"));
        titleView = findViewById(resId("id", "title"));

        textView.setText("开始任务\n");
        textView.append("任务ID：" + MyAccessibilityService.taskId + "，任务名称：" + TaskTypeEnum.getTaskNameByCode(MyAccessibilityService.taskType) + "\n");

        //获取数据
        Intent intent = getIntent();
        taskId = intent.getIntExtra("taskId", 0);
        deviceName = intent.getStringExtra("deviceName");

        if(deviceName != null && !"".equals(deviceName)){
            Message message = new Message();
            message.what = 200;
            message.obj = deviceName;
            mHandler.sendMessage(message);
        }


    }

    public static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (msg.what == 100) {
                SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                if(textView.getLineCount() >= 40){
                    textView.setText("\n" + format.format(new Date()) + "\t" + msg.obj.toString());
                }else{
                    textView.append("\n" + format.format(new Date()) + "\t" + msg.obj.toString());
                }
            }else if(msg.what == 200){
                deviceName = msg.obj.toString();
                titleView.setText(msg.obj.toString());
            }
        }
    };

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TAG,"onStart");
        Log.d(TAG, "isBack:" + isBack);
        if(MyAccessibilityService.isRun == 0){
            Log.i(TAG, "跳回主页面");
            toMain();
        }else{
            isBack = true;
            //打开微信/QQ
            if(MyAccessibilityService.opType == 0){
                MainActivity.openWechat();
            }
            if(MyAccessibilityService.opType == 1){
                MainActivity.openQQ();
                /*try{
                    Thread.sleep(6000);
                }catch (Exception e){
                    e.printStackTrace();
                }*/
            }

        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        /*if("".equals(MOBILE_IMEI)){
            requestPermission();
        }else{
            getWechatInfo();
        }*/
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TAG,"onPause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TAG,"onStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG,"onDestroy");
    }

    public static void toMain(){
        isBack = false;
        Intent intent = new Intent(taskActivity, MainActivity.class);
        //只是把栈中的activity调换位置，不开启新的页面
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        taskActivity.startActivity(intent);
    }


    public static int resId(Context context, String type, String name) {
        Resources resources = context.getResources();
        return resources.getIdentifier(name, type, context.getApplicationInfo().packageName);
    }

    public int resId(String type, String name) {
        return resId(this, type, name);
    }

}
